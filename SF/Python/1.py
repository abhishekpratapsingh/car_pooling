from math import radians, cos, sin, asin, sqrt
import pickle
import heapq
import datetime 
import os

maxDistance = 10000000

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees) 
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r

nodes = file("LocationData.txt")
edgesFile = file("SegmentData.txt")
cab = file("./cabspottingdata/new_abcoij.txt")
path = "./cabspottingdata/"

nodesMap = {}
nodesMap2 = {}
listCoords = []
nodesList = []
while(1):
    line = nodes.readline()
    if( line == "" ):
        break
    lineData = line.split(",")
    nodesMap[ int( lineData[0] ) ] = ( float(lineData[1]), float(lineData[2]) )
    nodesMap2[ ( float(lineData[1]), float(lineData[2]) ) ] = int( lineData[0] )
    listCoords.append( ( float(lineData[1]), float(lineData[2]) )  ) 
    nodesList.append( int( lineData[0] ) )

listCoords.sort(key=lambda tup: (tup[0], tup[1]) )


edgeWeight = {}
edges = {}
tedgeWeight = {}
tedges = {}
counter_bad = 0
while(1):
    line = edgesFile.readline()
    if( line == "" ):
        break
    lineData = line.split(";")
    edgeWeight[ ( int( lineData[0] ) , int( lineData[1] ) ) ] = float( lineData[2] )/1000
    try: 
        edges[ int(lineData[0]) ].append( int(lineData[1]) )
    except: 
        edges[ int(lineData[0]) ] = [ int(lineData[1]) ]

def load_obj(name ):
    with open( name + '.p', 'rb') as f:
        return pickle.load(f)


timeWeights = load_obj("timeWeights")
avg = 0
totalN = 0
# for x in timeWeights:
#     for i in xrange(0, len(timeWeights[x])):
#         if( timeWeights[x][i] < 0):
#             timeWeights[x][i] = 0;

# for x in timeWeights:
#     avg += sum( timeWeights[x] )
#     totalN += len(timeWeights[x])
# avg = float(avg)/totalN

# for x in edgeWeight:
#     if(x in timeWeights):
#         avg_time = ( sum(timeWeights[x])/len(timeWeights[x]) )
#         edgeWeight[x]  = float(edgeWeight[x])/ avg_time
#     elif( (x[1], x[0]) in timeWeights):
#         y = (x[1], x[0])
#         avg_time = ( sum(timeWeights[y])/len(timeWeights[y]) )
#         edgeWeight[x]  = float(edgeWeight[x])/ avg_time
#     else:
#         edgeWeight[x]  = float(edgeWeight[x])/avg
# del timeWeights

print "Updated Edges"

def missingNodes( source, Dest):
    global nodesList, edges, edgeWeight
    heap = []
    tempDis = {}
    path = {}
    for i in xrange(0, len(nodesList) ):
        tempDis[ nodesList[i] ] = maxDistance

    tempDis[ source ] = 0
    heapq.heappush( heap, (0, (source) ) )
    path[ source ] = source
    try:
        while( len(heap) ):
            top = heapq.heappop( heap )
            # print top,heap   
            if( abs(top[0]) > tempDis[ top[1] ] ):
                continue
            
            if(top == Dest):
                break
            
            if top[1] in edges:
                for key in edges[ top[1] ]:
                    weightNode = tempDis[ top[1] ]
                    weightKey = tempDis[ key ]
                    newWeightKey = weightNode + edgeWeight[ (top[1], key) ]
                    if( newWeightKey < weightKey):
                        tempDis[ key ] = newWeightKey
                        heapq.heappush( heap, ( newWeightKey, (key) ) )
                        path[ key ] = top[1]
        
        sp = []
        x = Dest

        while(1):
            if(x == source):
                break
            sp.append(x)
            x = path[x]

        sp.reverse()
        return( sp, tempDis[Dest] )
    except:
        raise

traj = []
currentTraj = []
node = prevnode = ( 0, 0)

listing = os.listdir( path )
skipTraj = False
for infile in listing:
    if(infile[0:4] == "new_"):
        cab = open(path+infile, 'r')
    else:
	continue
    print infile
    cabLines = []
    while( 1 ):
        line = cab.readline()
        if( line == "" ):
            break
        cabLines.append( line )
    Llat = 0; Llon = 0; Ltime = 0;
    for indexFile in xrange( len(cabLines)-1, 0, -1):
        line = cabLines[ indexFile ]
        lineData = line.split(" ")
        lineData[-1] = lineData[-1][0:-2]
        lat = float( lineData[0] ); lon = float( lineData[1] )
        occupied = int(lineData[2]) 
        time = int(lineData[-1])
        howfar = haversine(lon, lat, Llon, Llat)
        Llat = lat; Llon = lon; Ltime = time;
        timeStamp = datetime.datetime.utcfromtimestamp( time )
        ar = ( [ haversine(lonList, latList, lon, lat)  for (lonList, latList) in listCoords ] )
        nodeIndex = ar.index( min( ar ) )
        
        if( min(ar) > 0.5):
            skipTraj = True
            continue
        
        node = nodesMap2[ listCoords[nodeIndex] ]
        
        if( prevnode == node):
            continue

        missingNodesList = []
        if( not( occupied ) ):
            if( len( currentTraj ) ):
                if( not(skipTraj) ):
                    traj.append( currentTraj )
                currentTraj = []
                skipTraj = False
        else:
            if( not( len(currentTraj) ) ):
                currentTraj.append( node )
            else:
                # try:
                #     # (missingNodesList, dist) = missingNodes(prevnode, node)
                #     missingNodesList = [node]
                #     # print len(missingNodesList), dist*1000, dist*1000/(time-prevT), time-prevT, "\n"
                # except: 
                #     skipTraj = True
                currentTraj.append( node ) 
        prevnode = node              
        prevT = time

    if( len( currentTraj ) ):
        if( not(skipTraj) ):
                traj.append( currentTraj )
        traj.append( currentTraj )        

    fp1 = open('traj.p', 'w')
    pickle.dump( traj, fp1)
    fp1.close()

