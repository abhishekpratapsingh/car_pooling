#include <bits/stdc++.h>
#include <ctime>
#include "ioD.h"

using namespace std;

long long n;
int queryCnt;
char * txtName, * degName, * outName, *inName;
edgeL * deg;
edgeS * labelout, *labelin;
edgeS * labelx, * labely;
bool fc = 0;

double query(int x, int y)
{
	if (x == y) return 0;
	int xx = x, yy = y;

	x = ((deg[xx].x<<32)>>32);
	y = ((deg[yy].x<<32)>>32);
		
	if (x > y)
	{
		labelx = labelout + deg[xx].w;
		labely = labelin + deg[yy].y;
	}
	else
	{
		int xy = x; x = y; y = xy;
		labelx = labelin + deg[yy].y;
		labely = labelout + deg[xx].w;
	}

	int ans = 1000000, i = 0, j = 0;

	if (labelx[i].x != -1 && labely[j].x != -1)
	while (labelx[i].x < y)
	{
		if (labelx[i].x == labely[j].x) 
		{
			ans = ans>(labelx[i].w + labely[j].w)?(labelx[i].w + labely[j].w):ans;
			if (labelx[++i].x == -1) break;
			if (labely[++j].x == -1) break;
		}
		else if (labelx[i].x < labely[j].x)
		{
			if (labelx[++i].x == -1) break;
		}
		else if (labely[++j].x == -1) break;
	}
	
	while (labelx[i].x != -1 && labelx[i].x < y) i++;
	if (labelx[i].x == y) ans = ans>labelx[i].w?labelx[i].w:ans;

	return float(ans);
}


void loadIndex()
{
	inBufL degBuf(degName);
	inBufS inLabel(inName), outLabel(outName);
	
	n = checkB(degName)/sizeof(edgeL);

	deg = (edgeL *)malloc(sizeof(edgeL)*n);
	labelin = (edgeS*)malloc(checkB(inName));
	labelout = (edgeS*)malloc(checkB(outName));

	printf("%lld vertices\n", n);

	degBuf.start();
	for (int i = 0; i < n; i++)
		degBuf.nextEdge(deg[i]);

	inLabel.start();
	for (int i = 0; !inLabel.isEnd; i++)
		inLabel.nextEdge(labelin[i]);
	
	outLabel.start();
	for (int i = 0; !outLabel.isEnd; i++)
		outLabel.nextEdge(labelout[i]);			
}

int DEBUG = 1;
float alpha  = 1.3;
double maxDepth = 0.2;
long long int beta = 0;
long long int maxDistance = 100000;
/* nodes global structure */
vector< pair<double, double> > nodes;
vector<long long int> nodeID;
map<long long int, long long int> idToNode;
map< long long int, pair<double, double> > nodeToLatLon;

/* Edges global structure */
map< long long int, vector<long long int> > edges;
map< long long int, vector<double> > edgeWeight;
map< long long int, vector<long long int> > edgesReverse;
map< long long int, vector<double> > edgeWeightReverse;

map< long long int, map<long long int, vector<long long int> > > sourceTimeDestination; 

double minPathScore[1000];

double dagScore[1000];
double dagTimeTaken[1000];

double dagExScore[1000];
double dagExTime[1000];

long long int tripNumber = 0;
clock_t startTimeTrip;
clock_t endTimeTrip;
int countWin = 0;

double distFromSourceToDestination; 

void getTimeSourceDestination() {
	ifstream file;
	file.open( "outpu" );
	string s;
	int trips = 0;
	while( getline(file, s) ) {
		stringstream ss( s );
		long long int source, timeSlot, dest;
		ss>>source>>timeSlot;
		while( ss>>dest ) {
			trips++;
			sourceTimeDestination[ source ][ timeSlot ].push_back( dest );
		}
	}
	cout<<trips<<endl;
	exit(1);
}

void takeGraphInput() {
	ifstream Location;
	Location.open("LocationData.txt");
	string s;
	int index = 0;
	while( getline(Location, s) ) {
		stringstream ss(s);
		long long int id; double lon; double lat; char ch;
		ss>>id>>ch>>lon>>ch>> lat;
		nodes.push_back( make_pair( lon, lat) );
		nodeID.push_back( id );
		idToNode[ id ] = index;
		nodeToLatLon[ id ] = make_pair( lat, lon);
		index++;
	}

	Location.close();

	ifstream edgesFile;
	edgesFile.open("SegmentData.txt");
	
	while( getline(edgesFile, s) ) {
		stringstream ss(s);
		long long int node1; long long int node2; double weight; char ch;
		ss>>node1>>ch>>node2>>ch>>weight;
		edges[ node1 ].push_back( node2 );
		edgeWeight[ node1 ].push_back( weight );

		edgesReverse[ node2 ].push_back( node1 );
		edgeWeightReverse[ node2 ].push_back( weight );
		
	}
	edgesFile.close();
	return ;
}

vector<long long int>  dijkstra_lengths(long long int S, long long int D, map<long long int, double > &distanceFromSource,
 map<long long int, vector<long long int> > &edges, map<long long int, vector<double> > &edgeWeight ) { 
	
   	map<long long int, long long int> path;
	for(int i=0; i< nodeID.size(); i++)
	{ 	distanceFromSource[ nodeID[i] ] = 100000;
		path[ nodeID[i] ] = -1;
	}

	distanceFromSource[ S ] = 0;
	priority_queue< pair<float, long long int> > dj;
	dj.push( make_pair(0, S) );

	while(dj.size()) { 
		pair<float, long long int> x = dj.top();
		dj.pop();

		long long int u = x.second;
		for(int i=0; i< edges[ u ].size(); i++) { 	
			long long int v = edges[ u ][ i ];
			float alt = distanceFromSource[ u ] + edgeWeight[ u ][ i ];
			if(alt < distanceFromSource[ v ])
			{ 	distanceFromSource[ v ] = alt;
				dj.push( make_pair(-alt, v) );
				path[v] = u;
			}
		}
	}

	vector<long long int> path1;
	long long int x = D;
	while(1) {
		path1.push_back(x);
		if( ( x == S ) || ( x == -1 ) || ( path.find(x) == path.end() ) )
		  	break;
		x = path[ x ];
	}
	
	path1.push_back(S);

	return path1;
}

double getDistanceFromTo(long long int S, long long int D) { 
	map<long long int, float> dist;
 
	for(int i=0; i< nodeID.size() ; i++)
	{ 	dist[ nodeID[i] ] = 100000;
	}

	dist[S] = 0;
	priority_queue< pair<float, long long int> > dj;
	dj.push( make_pair(0, S) );

	while(dj.size())
	{ 
		pair<float, long long int> x = dj.top();
		dj.pop();
		
		long long int u = x.second;
		
		if( u == D)
			break;
		
		for(int i=0; i< edges[u].size(); i++)
		{ 	long long int v = edges[u][i];
			float alt = dist[u] + edgeWeight[u][i];
			if(alt < dist[v])
			{ 	dist[v] = alt; 
				dj.push(make_pair(-alt, v));
			}
		}
	}

	return dist[ D ];
}

int HD(long long int S, long long int D, map<long long int, long long int > &weights) { 
	map<long long int, float> dist;
 
	for(int i=0; i< nodeID.size() ; i++)
	{ 	dist[ nodeID[i] ] = 100000;
	}

	dist[S] = 0;

	// ( (score, node), (distance, path) )
	priority_queue< pair< pair<float, long long int>, pair< float, vector<int> > > > dj;
	
	vector<int> path;
	path.push_back(S);
	dj.push( make_pair( make_pair(0, S), make_pair(0, path) ) );

	while(dj.size())
	{ 
		pair< pair<float, long long int>, pair< float, vector<int> >  > x = dj.top();
		dj.pop();
		
		long long int u = x.first.second;
		
		if( x.second.first > distFromSourceToDestination )
			continue;

		if( u == D ) {
			int score = 0;
			for(int i=1;i<x.second.second.size(); i++) {
				score += weights[ x.second.second[i] ];
			}
			return score;
		}
		
		for(int i=0; i< edges[u].size(); i++)
		{ 	long long int v = edges[u][i];
			for(int j=0; j<x.second.second.size(); j++) 
				if( x.second.second[j] == v)
					continue;

			float alt = -x.first.first + (edgeWeight[u][i]/(weights[v]+1) );
			if( (x.second.first + edgeWeight[u][i]) < dist[v] )
			{ 	dist[v] = (x.second.first + edgeWeight[u][i]); 
			    path = x.second.second;
			    path.push_back(v);
				dj.push( make_pair( make_pair(-alt, v), make_pair(dist[v], path) ) );
			}
		}
	}

	// not reachable
	return -1; 
}


void get_all_trips(long long int source,long long int start_time, vector<long long int> &tripList) {

	if( sourceTimeDestination.find(source) != sourceTimeDestination.end() ) {
		if( sourceTimeDestination[source].find(start_time) 
					!= sourceTimeDestination[source].end() ) {

			for(int i = 0; i < sourceTimeDestination[ source][start_time].size(); i++) {
				tripList.push_back( sourceTimeDestination[source][ start_time ][ i ] );
			}

		}
	}
}

long long int get_weight(long long int source, long long int destination, long long int midStop, long long int start_time,
 	map<long long int, double> &distanceToDestination, map<long long int, double> &distanceFromDestination)
{  	
	vector<long long int> tripList;
	
	get_all_trips(midStop, start_time/15, tripList);
  
  	long long int score = 0;
  	int counter = 0;

  	long long int s = idToNode[source];
  	long long int d = idToNode[ destination ];
  	long long int v = idToNode[ midStop ];

  	for(int i = 0; i < tripList.size(); i++) {

  		long long int w = idToNode[ tripList[ i ] ]; 
		double tsv = query(s, v);
 		double tvw = query(v, w);
        double tvd = query(v, d);
        double twd = query(w, d);
        double tdw = query(d, w);

      	if( tvd + tdw < (1+alpha)*tvw ) {
      		counter += 1;
      	}
      	else {
      		double distanceA = ( tsv + tvw + twd);
      		double distanceB = (1+alpha)*distFromSourceToDestination;
      		if( distanceA <= distanceB) {
      			counter += 1;
      		}
      	}
  	}
  	
  	return counter;
}

long long int dagScore1(long long int source, long long int destination, map<long long int, double > &distanceFromSource,
	map<long long int, double > &distanceToDestination, map<long long int, long long int > &weights) {
	vector< pair< double, long long int> > dag;
	long long int sourceIndex = idToNode[ source ];
	long long int destinationIndex = idToNode[ destination ];
	cout<<distanceFromSource[ destination ]<<endl;
	int startTripTime = clock();
	for(int i=0; i <  nodes.size() ; i++) {	
		if( ( query(idToNode[ source ], i) + query(i, idToNode[ destination ] ) )
			 < alpha*distFromSourceToDestination )
			  {
			dag.push_back( make_pair(-distanceToDestination[ nodeID[i] ], nodeID[ i ]) );
		}
	} 
	dagTimeTaken[ tripNumber ] += float( clock()-startTripTime )/CLOCKS_PER_SEC;
	sort( dag.begin(), dag.end() );
	vector< map<int, double> > scores( dag.size() );
  
  	map<long long int, long long int> nodeToDagIndex;
	// cout<<dag.size()<<endl;
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
		
	long long int startIndex = nodeToDagIndex[source];
  	long long int endIndex = nodeToDagIndex[destination];

  	scores[ startIndex ][ 0 ] = 0;
  	for(int i = startIndex; i < dag.size(); i++) {
    	long long int u = dag[i].second;
    	// cout<<weights[u]<<endl;
	    for(int j = 0; j < edges[u].size(); j++) {
	    	long long int v = edges[u][j];
	      	long long int vIndex = nodeToDagIndex[ v ];
	      	if( nodeToDagIndex.find( v ) == nodeToDagIndex.end()  )
	        	continue;
	      	//some nodes are ajacent to themselves in the graph
	      	if(u == v)
	        	continue;

	      	if( vIndex >= i ) {
	      		for(map<int, double>::iterator it=scores[ i ].begin(); it != scores[ i ].end(); it++)
	        	{ 
	        		// cout<<it->first<<endl;
	        		if( it->first > 10000)
	        			continue;

	          		if( scores[ nodeToDagIndex[v] ].find( it->first + weights[v] ) != scores[ nodeToDagIndex[v] ].end() ) {
			          	if( scores[ nodeToDagIndex[v] ][it->first + weights[v] ] >= it->second + edgeWeight[u][j] ) {
			            	scores[ nodeToDagIndex[v] ][it->first + weights[v] ] = it->second + edgeWeight[u][j];
			            }
			        }
			        else
			        {
			        	scores[ nodeToDagIndex[v] ][it->first + weights[v] ] = it->second + edgeWeight[u][j];	
			        }
				}
			}
		}
	}
	long long int bestScore = 0;

	for( map<int, double>::iterator it=scores[ endIndex ].begin(); it != scores[ endIndex ].end(); it++) {
		
		if( it->second < distFromSourceToDestination*alpha ) {
			bestScore = it->first;
		}
	}
	cout<<"BEST SCORE:   "<<bestScore<<endl;
	return bestScore;

}


void extendEdges(long long int source, long long int node, vector<long long int> &path, vector< pair<double, vector<long long int> > >  &paths,
	double pathLen) {
	
	for( int i = 0; i < path.size(); i++) {
		if( node == path[i] )
			return;
	}
	path.push_back( node );

	if( path.size() > 1 ) {
		paths.push_back( make_pair(pathLen, path) ) ;
	}

	if( pathLen >= maxDepth ) {
		path.pop_back();
		return ;
	}
	
	for(long long int j=0; j < edges[ node ].size(); j++) {
		long long int newNode = edges[ node ][j];
		extendEdges( source, newNode, path, paths, pathLen + edgeWeight[ node ][j] ) ;
	}

	path.pop_back();
}

long long int dagExtendedEdges(long long int source, long long int destination, map<long long int, double > &distanceFromSource, map<long long int, double > &distanceToDestination,
  map<long long int, vector< pair<double, vector<long long int> > > > &extendEdge, map<long long int, vector< long long int > > &extendEdgeWeights, map<long long int, long long int > &weights) {

	vector< pair< double, long long int> > dag;
	long long int sourceIndex = idToNode[ source ];
	long long int destinationIndex = idToNode[ destination ];
	for(int i=0; i <  nodes.size() ; i++) {	
		if( ( query(idToNode[ source ], i) + query(i, idToNode[ destination ] ) )
			 < alpha*distFromSourceToDestination )
			  {
			dag.push_back( make_pair(-distanceToDestination[ nodeID[i] ], nodeID[ i ]) );
		}
	} 

	sort( dag.begin(), dag.end() );

  	vector< map<int, double>  > scores(dag.size());
  	
  	map<long long int, long long int> nodeToDagIndex;
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
	
	long long int startIndex = nodeToDagIndex[ source ];
  	long long int endIndex = nodeToDagIndex[ destination ];

  	scores[ startIndex ][ 0 ] = 0;
  	map<long long int, map<long long int, long long int> > reach;
  	map<long long int, map<long long int, long long int> > reachFrom;

  	int counter = 0;
  	for(int i = startIndex; i < dag.size(); i++) {
    	long long int u = dag[i].second;
	    
	    for(int j = 0; j < extendEdge[u].size(); j++) {
	    	
	    	pair<double, vector<long long int> > v = extendEdge[u][j];
	    	bool continueLoop = false;
	    	bool reachAbleError = false;
	    	long long int pathSize = v.second.size();

	    	if( pathSize < 1)
	    		continue;

	    	for(int k1 = 0; k1 < pathSize && !reachAbleError ; k1++ ) {	
    			for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
    				if( reach[ v.second[ k2 ] ][ v.second[ k1 ] ]  == 1) {
    					reachAbleError = true;
    					break;
    				}
    			}
        	} 

	    	for(int k = 1; k < pathSize ; k++ ) {
	    		long long int pathNode = v.second[ k]; 
	    		
	    		if( ( nodeToDagIndex[ pathNode ] >= i ) ^  ( k == ( pathSize -1 ) ) ) {
	    			continueLoop = true;
	    			break;
	    		}
	    		if( reach[ pathNode ][u] || ( nodeToDagIndex.find( pathNode ) == nodeToDagIndex.end() ) ) {
	    			reachAbleError = true;
	    			break;
	    		}
	    	}

	    	long long int lastNode =  v.second[ pathSize - 1 ];

	      	if( continueLoop || reachAbleError || ( lastNode == u ) )
	    		continue;

	    	counter++;
	       	long long int vIndex = nodeToDagIndex[ lastNode ];
	      	long long int vWeight = extendEdgeWeights[u][j];

	      	long long int updated = false;

      		for(map<int, double>::iterator it=scores[ i ].begin(); it != scores[ i ].end(); it++) { 

	          	if( it->first > 10000)
	        			continue;

          		if( scores[ vIndex ].find( it->first + vWeight ) != scores[ vIndex ].end() ) {
		          	if( scores[ vIndex][it->first + vWeight ] >= it->second + edgeWeight[u][j] ) {
		            	scores[ vIndex ][it->first + vWeight ] = it->second + edgeWeight[u][j];
		            }
		        }
		        else
		        {
		        	scores[ vIndex ][it->first + vWeight ] = it->second + edgeWeight[u][j];	
		        }
				
            	if( updated )
            		continue;
            	else
            		updated = 1;

            	for(int k1 = 0; k1 < pathSize ; k1++ ) {	
        			for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
            			reach[ v.second[ k1 ] ][ v.second[ k2 ] ] 	 = 1;
            			reachFrom[ v.second[ k2] ][ v.second[ k1 ] ] = 1;
        			}
            	} 

            	for(long long int k1 = pathSize - 1; k1 >= 0 ; k1-- ) {	
            		for( map<long long int, long long int>::iterator it = reachFrom[ v.second[ k1 ] ].begin(); it != reachFrom[ v.second[ k1 ] ].end(); it++) {
            			for(long long int k2 = k1 + 1; k2 < pathSize; k2++ ) {
	            			reach[ it->first ][ v.second[ k2 ] ] 	= 1;
	            			reachFrom[ v.second[ k2] ][ it->first ] = 1;
            			}
            		}
            	}
            	
        	}
		}
	}

	long long int bestScore = 0;

	for( map<int, double>::iterator it=scores[ endIndex ].begin(); it != scores[ endIndex ].end(); it++) {	
		if( it->second < distFromSourceToDestination*alpha ) {
			bestScore = it->first;
		}
	}

	return bestScore;

}

long long int calculateScore( vector< long long int > &path, map<long long int, long long int> &weights) {
	long long int ret = 0;
	for(int i = 1; i < path.size(); i++) {
		ret += weights[ path[i] ];
	}
	return ret;
}

void potentialScore(long long int source, long long int destination, long long int timeSlot, 
	map<long long int, vector< pair<double, vector<long long int> > > > &extendEdge)
{
	cout<<"LOL"<<endl;
	map< long long int, double > distanceFromSource;
	map< long long int, double > distanceFromDestination;
	map< long long int, double > distanceToDestination;

	vector<long long int> path = dijkstra_lengths(source, destination, distanceFromSource, edges, edgeWeight);
	dijkstra_lengths(destination, source, distanceFromDestination, edges, edgeWeight);
	
	if(distanceFromSource[destination] == maxDistance || !distanceFromSource[ destination ] ) {
		tripNumber--;
		return;
	}
	
	dijkstra_lengths(destination, source, distanceToDestination, edgesReverse,
	edgeWeightReverse);

	map<long long int, long long int> weights; 
	if( DEBUG ) {
		cout<<"dijkstra_lengths: ";
		cout<<distanceFromSource[ destination ]<<endl;
	}

	dagTimeTaken[ tripNumber ] = 0;
	startTimeTrip = clock();

	for( int i =0; i<nodeID.size(); i++) {	
		if( ( distanceFromSource[ nodeID[i] ] + distanceToDestination[ nodeID[i] ] ) 
				< alpha	*distanceFromSource[ destination ] ) {
			long long int passengersNumber;
			weights[ nodeID[i] ] = get_weight(source, destination, nodeID[i], timeSlot,
				distanceToDestination, distanceFromDestination);
		}
	}

	// cache this value
	cout<<"WEIGHTS WERE ASSIGNED"<<endl;
	cout<<idToNode [ source ] <<" Source - Destination"<<idToNode[ destination ]<<endl;
	distFromSourceToDestination = query(idToNode[ source ], idToNode[ destination ] );
	cout<<"WEIGHTS WERE ASSIGNED"<<endl;
	dagTimeTaken[ tripNumber ] += ( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC; 
	cout<<tripNumber<<endl;
	dagExTime[ tripNumber ] = dagTimeTaken[ tripNumber ];

	startTimeTrip = clock();
	distFromSourceToDestination = distanceFromSource[ destination ];
	
	int scoreMinPath = HD( source, destination, weights);
	minPathScore[ tripNumber ] = float( scoreMinPath  + 1);
	dagTimeTaken[ tripNumber ] = float( clock()- startTimeTrip )/CLOCKS_PER_SEC;
	/* calculating DAG parameters */
	startTimeTrip = clock();
	map<long long int, long long int> path1;
	long long int scoreDag = dagScore1(source, destination, distanceFromSource,
	distanceToDestination, weights);

	cout<<( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC<<endl;
	//dagTimeTaken[ tripNumber ] +=  ( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC;
	cout<<scoreMinPath<<" SCORES "<<scoreDag<<endl;
	dagScore[ tripNumber ] = float( scoreDag + 1)  / ( minPathScore[ tripNumber ] ) ;
	if(  scoreDag + 1 >=  minPathScore[ tripNumber ] )
		countWin += 1;
	/* calculating backDAG	 parameters */
	startTimeTrip = clock();
	
	map<long long int, vector< long long int> > extendEdgeWeights;
	for( int i = 0; i < nodeID.size(); i++) {		
		long long int node = nodeID[i];
		for(int j =0; j < extendEdge[node].size(); j++) {
			long long int scoreAlongPath = calculateScore( extendEdge[node][j].second, weights);
			extendEdgeWeights[ nodeID[i] ].push_back( scoreAlongPath );
		}
	}

	scoreDag = 	dagExtendedEdges( source, destination, distanceFromSource, distanceToDestination, extendEdge, extendEdgeWeights, weights);
	dagExScore[ tripNumber ] = float( scoreDag + 1 ) / ( minPathScore[tripNumber] ) ;
	dagExTime[ tripNumber ] += ( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC;
	cout<<	"SCORE ALONG EX "<<scoreDag<<"  RATIO WITH DAG "<< float( scoreDag + 1)/(dagScore[ tripNumber ]*minPathScore[ tripNumber] )<<endl;
	return ;	
}

/* edges global structure */
int main(int argc, char const *argv	[])
{
	txtName = "sfIndex";
	degName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(degName, "%s.deg", txtName);
	
	inName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(inName, "%s.labelin", txtName);
	outName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(outName, "%s.labelout", txtName);

	timer tm;
	
	loadIndex();
	
	printf("load time %lf (ms)\n", tm.getTime()*1000);
	
	memset(dagScore, 0, sizeof(dagScore));
	memset(minPathScore, 0, sizeof(minPathScore));
	memset(dagTimeTaken,0,sizeof(dagTimeTaken));
	string s = argv[1];
	stringstream ss(s);	
	stringstream ssDebug(argv[2]);
	ss>>maxDepth;
	ssDebug>>alpha;

	takeGraphInput();
	getTimeSourceDestination();
	map<long long int, vector< pair<double, vector<long long int> > > > extendEdge; 
	for( int i = 0; i < nodeID.size(); i++) {
		long long int node = nodeID[i];
		vector<long long int> path;
		vector< pair< double, vector<long long int> > > paths;
		extendEdges(node, node, path, paths, 0);
		extendEdge[ node ] = paths;
	}
	srand(time(NULL) );
	tripNumber = 0;
	for( int i=0; tripNumber < 100; i += 1) {
		
		long long int source, timeS, destination;
		
		source = nodeID[ rand() % nodeID.size() ] ;
		timeS = rand()%96;

		if( sourceTimeDestination.find( source ) == sourceTimeDestination.end() )
			continue;
		else if ( sourceTimeDestination[ source ].find( timeS ) == sourceTimeDestination[ source ].end() )
			continue;

		destination = sourceTimeDestination[ source ][ timeS ][ rand() % sourceTimeDestination[ source ][ timeS ].size()  ];
		potentialScore(source, destination, timeS, extendEdge);
		tripNumber++;

	}

	double avgMinPath = 0, avgDAGScore = 0, avgDAGtime = 0 ; 
	double avgExScore = 0, avgExTime = 0;
	int hist[101];
	
	memset(hist, 0, sizeof(hist));

	/*std::vector<double> v1;
	std::vector<double> v2;

	for(int i = 0; i < 100; i++)
	{
		v1.push_back(dagScore[i]);
		v2.push_back(dagExScore[i]);
	} 
	sort(v1.begin(), v1.end());
	sort(v2.begin(), v2.end());
	for(int i = 0; i < 100; i++)
	{ 
		cout<<v1[i]<<endl;
	}
	for(int i = 0; i < 100; i++)
	{ 
		cout<<v2[i]<<endl;
	}
	cout<<"\n\nNEXT VERBOSE::\n\n";
	*/
	for(int i = 0; i < 100; i++)
	{
		avgMinPath += minPathScore[i];
		avgDAGScore += dagScore[i];
		avgDAGtime += dagTimeTaken[i];
		avgExScore += dagExScore[i];
		avgExTime += dagExTime[i];
		hist[ int(dagExScore[i]) ] += 1;
	} 
	cout<<"COUNT_WIN "<<countWin<<endl;
	cout<<avgMinPath/100<<" "<<avgDAGScore/100<<" "<<avgDAGtime/100<<endl;
	cout<<avgExScore/100<<" "<<avgExTime/100<<endl;
	for(int j = 0; j <= 100; j++)
	{
		cout<<hist[j]<<" ";
	}
	return 0;
}
