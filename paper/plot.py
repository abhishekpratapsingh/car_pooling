import matplotlib.pyplot as plt
import numpy as np

# def plot_score_v_alpha(alphas,dag,p1,p3,p5):
#     fig = plt.figure(figsize=(4.8,3.6))
#     plt.plot(alphas,dag,'b^-',label='DAG')
#     plt.plot(alphas,p1,'r^-',label='0.1 km extension')
#     plt.plot(alphas,p3,'y^-',label='0.3 km extension')
#     plt.plot(alphas,p5,'g^-',label='0.5 km extension')
#     plt.xticks(alphas)
#     plt.xlabel(r'$\alpha$')
#     plt.ylabel('Ratio of score over Dijkstra')
#     plt.legend()
#     return fig

# alphas = [1.1,1.2,1.3]

# BJ_dag = [2.75,2.97,3.117]
# BJ_p1 = [3.3,3.7,3.9]
# BJ_p3 = [3.38,4.18,4.48]
# BJ_p5 = [3.44,4.37,4.87]

# fig = plot_score_v_alpha(alphas,BJ_dag,BJ_p1,BJ_p3,BJ_p5)
# plt.savefig('BJ_score.png',dpi=300)
# plt.savefig('BJ_score.pdf',dpi=300)

# SF_dag = [1.64,1.64,1.65]
# SF_p1 = [1.78,1.88,1.91]
# SF_p3 = [1.88,2.362,2.55]
# SF_p5 = [1.9,2.42,2.7]

# fig = plot_score_v_alpha(alphas,SF_dag,SF_p1,SF_p3,SF_p5)
# plt.savefig('SF_score.png',dpi=300)
# plt.savefig('SF_score.pdf',dpi=300)

# NY_dag = [2.55,2.6,2.5]
# NY_p1 = [3.76,3.97,3.97]
# NY_p3 = [4.64,5.64,6.14]
# NY_p5 = [4.74,6.13,6.3]

# fig = plot_score_v_alpha(alphas,NY_dag,NY_p1,NY_p3,NY_p5)
# plt.savefig('NY_score.png',dpi=300)
# plt.savefig('NY_score.pdf',dpi=300)



# def plot_time_v_ext(xlabel,a1p1,a1p2,a1p3):
#     fig = plt.figure(figsize=(4.8,3.6))
#     ax = list(range(len(xlabel)))
#     plt.plot(ax,a1p1,'r^-',label=r'$\alpha = 1.1$')
#     plt.plot(ax,a1p2,'y^-',label=r'$\alpha = 1.2$')
#     plt.plot(ax,a1p3,'g^-',label=r'$\alpha = 1.3$')
#     plt.xticks(ax,xlabel)
#     plt.xlabel('Extension')
#     plt.ylabel('Time (second)')
#     plt.legend()
#     return fig

# xlabel = ['DAG','Dijkstra','0.1 km','0.3 km','0.5 km']

# BJ_a1p1 = [0.04631,0.0601,0.084,0.321,0.73]
# BJ_a1p2 = [0.046,0.063,0.11,0.47,1.272]
# BJ_a1p3 = [0.048,0.066,0.124,0.53,1.56]

# fig = plot_time_v_ext(xlabel,BJ_a1p1,BJ_a1p2,BJ_a1p3)
# plt.savefig('BJ_time.png',dpi=300)
# plt.savefig('BJ_time.pdf',dpi=300)


# SF_a1p1 = [0.01,0.03,0.02,0.04,0.05]
# SF_a1p2 = [0.013,0.035,0.05,0.09,0.12]
# SF_a1p3 = [0.015,0.04,0.08,0.15,0.2]

# fig = plot_time_v_ext(xlabel,SF_a1p1,SF_a1p2,SF_a1p3)
# plt.savefig('SF_time.png',dpi=300)
# plt.savefig('SF_time.pdf',dpi=300)


# NY_a1p1 = [0.0024,0.086,0.30,2.56,11.5]
# NY_a1p2 = [0.0022,0.088,0.42,2.84,12.84]
# NY_a1p3 = [0.0026,0.095,0.58,3.65,15.6]

# fig = plot_time_v_ext(xlabel,NY_a1p1,NY_a1p2,NY_a1p3)
# plt.savefig('NY_time.png',dpi=300)
# plt.savefig('NY_time.pdf',dpi=300)

# def plot_time_v_network(xlabel,dag,p1,p3,p5):
#     fig = plt.figure(figsize=(4.8,3.6))
#     ax = [1,2,4,12]
#     plt.plot(ax,dag,'b^-',label='DAG')
#     plt.plot(ax,p1,'r^-',label='0.1 km extension')
#     plt.plot(ax,p3,'y^-',label='0.3 km extension')
#     plt.plot(ax,p5,'g^-',label='0.5 km extension')
#     plt.xticks(ax,xlabel)
#     plt.xlabel('No. of nodes')
#     plt.ylabel('Time (second)')
#     plt.legend()
#     return fig

# xlabel = ['5k','10k','20k','60k']

# NY_dag = [0.0019,0.0020,0.0022,0.0022]
# NY_p1 = [0.1507,0.2242,0.2489,0.319]
# NY_p3 = [1.255,1.386,1.707,2.844]
# NY_p5 = [1.967,3.493,5.488,12.899]

# fig = plot_time_v_network(xlabel,NY_dag,NY_p1,NY_p3,NY_p5)
# plt.savefig('NY_network.png',dpi=300)
# plt.savefig('NY_network.pdf',dpi=300)



# def plot_score_v_optimal(xlabel,best):
#     fig = plt.figure(figsize=(4.8,3.6))
#     ax = list(range(len(xlabel)))
#     plt.plot(ax,[1.0]*len(xlabel),'b^-',label='Optimal path')
#     plt.plot(ax,best,'r^-',label='Path suggested by our algorithm')
#     plt.xticks(ax,xlabel)
#     plt.xlabel('Extension')
#     plt.ylabel('Score relative to optimal')
#     plt.legend()
#     return fig

# xlabel = ['DAG','0.1 km','0.3 km','0.5 km']

# NY_best = [0.5655,0.6638,0.7941,0.8331]

# fig = plot_score_v_optimal(xlabel,NY_best)
# plt.savefig('NY_optimal.png',dpi=300)
# plt.savefig('NY_optimal.pdf',dpi=300)


# def plot_score_simulation(xlabel,a1p1,a1p2,a1p3):
#     fig = plt.figure(figsize=(4.8,3.6))
#     ax = list(range(len(xlabel)))
#     plt.plot(ax,a1p1,'r^-',label=r'$\alpha = 1.1$')
#     plt.plot(ax,a1p2,'y^-',label=r'$\alpha = 1.2$')
#     plt.plot(ax,a1p3,'g^-',label=r'$\alpha = 1.3$')
#     plt.xticks(ax, xlabel)
#     # Set the y limits making the maximum 5% greater
#     plt.ylim(0, 1)

#     plt.xlabel('Extension')
#     plt.ylabel('Percentage of pickup')
#     plt.legend(loc = 4, prop={'size': 15})
#     return fig

# xlabel = ['Shortest Path','DAG','0.1 km','0.3 km','0.5 km']
# num_trips = 200

# NY_a1p1 = [float(x)/num_trips for x in [88,128,145,152,158]]
# NY_a1p2 = [float(x)/num_trips for x in [115,152,170,179,184]]
# NY_a1p3 = [float(x)/num_trips for x in [128,164,184,188,189]] 

# fig = plot_score_simulation(xlabel,NY_a1p1,NY_a1p2,NY_a1p3)
# # plt.savefig('NY_simulation.png',dpi=300)
# plt.savefig('NY_simulation.pdf',dpi=300)

# BJ_a1p1 = [float(x)/num_trips for x in [34,43,50,52,53]]
# BJ_a1p2 = [float(x)/num_trips for x in [50,72,79,83,90]]
# BJ_a1p3 = [float(x)/num_trips for x in [57,88,90,98,101]]

# fig = plot_score_simulation(xlabel,BJ_a1p1,BJ_a1p2,BJ_a1p3)
# # plt.savefig('BJ_simulation.png',dpi=300)
# plt.savefig('BJ_simulation.pdf',dpi=300)


def plot_real_score_simulation(xlabel,realsim):
    fig = plt.figure(figsize=(4.8,3.6))
    ax = list(range(len(xlabel)))
    plt.plot(ax,realsim,'g^-',label=r'$\alpha = 1.2$')
    plt.xticks(ax, xlabel)
    # Set the y limits making the maximum 5% greater
    plt.ylim(0, np.max(realsim)+5)

    plt.xlabel('Extension')
    plt.ylabel('Percentage of trips with pickup')
    plt.legend(loc = 4, prop={'size': 15})
    return fig

xlabel = ['Shortest Path','DAG','0.1 km','0.3 km','0.5 km']

# NY_realsim = [7290/12065,8345/11163,8823/10759,9203/10476,9299/10456]
# NY_realsim = [x*100 for x in NY_realsim]

# fig = plot_real_score_simulation(xlabel,NY_realsim)
# plt.savefig('NY_real_simulation.pdf',dpi=300)

# BJ_realsim = [3286/11540,3638/11194,3758/11080,3835/11004,3919/10918]
# BJ_realsim = [x*100 for x in BJ_realsim]

# fig = plot_real_score_simulation(xlabel,BJ_realsim)
# plt.savefig('BJ_real_simulation.pdf',dpi=300)

# NY_realsim = [7290/12065,8345/11163,8823/10759,9203/10476,9299/10456]


# NY_realsim = [2*x/(1+x) for x in NY_realsim]
# NY_realsim = [x*100 for x in NY_realsim]
# fig = plot_real_score_simulation(xlabel,NY_realsim)
# plt.savefig('NY_real_simulation_pass.pdf',dpi=300)

# BJ_realsim = [3286/11540,3638/11194,3758/11080,3835/11004,3919/10918]

# BJ_realsim = [2*x/(1+x) for x in BJ_realsim]
# BJ_realsim = [x*100 for x in BJ_realsim]

# fig = plot_real_score_simulation(xlabel,BJ_realsim)
# plt.savefig('BJ_real_simulation_pass.pdf',dpi=300)



def plot_passenger_wait_time(xlabel,dij,dag,p1,p3):
	xlabel = xlabel[0:10]
	dij = dij[0:10]
	dag = dag[0:10]
	p1 = p1[0:10]
	p3 = p3[0:10]
	fig = plt.figure(figsize=(4.8,3.6))
	ax = xlabel
	plt.plot(ax,np.cumsum(dij)*100/np.sum(dij),'k^-',label='Dijkstra')
	plt.plot(ax,np.cumsum(dag)*100/np.sum(dag),'b^-',label='DAG')
	plt.plot(ax,np.cumsum(p1)*100/np.sum(p1),'r^-',label='0.1 km extension')
	plt.plot(ax,np.cumsum(p3)*100/np.sum(p3),'y^-',label='0.3 km extension')
	#plt.plot(ax,np.cumsum(p5)*100/np.sum(p5),'g^-',label='0.5 km extension')
	plt.xticks(ax,xlabel)
	plt.xlabel('No. of minutes wait')
	plt.ylabel('Cumulative % of car request fulfilled')
	plt.ylim(0,105)
	plt.legend()
	return fig

# xlabel = list(range(0, 25, 5))
# NY_dij_1 = [14315, 419, 86, 21, 5]
# NY_dag_1 = [13081, 563, 66, 18, 7]
# NY_p1_1 = [12193, 682, 74, 11, 7]
# NY_p3_1 = [11470, 793, 108, 25, 8]
# NY_p5_1 = [11524, 677, 86, 17, 6]


# fig = plot_passenger_wait_time(xlabel,NY_dij_1,NY_dag_1,NY_p1_1,NY_p3_1,NY_p5_1)
# plt.savefig('NY_1st_wait_time_12am.pdf',dpi=300)

# NY_dij_2 = [5269, 12, 0, 0, 0]
# NY_dag_2 = [6568, 37, 0, 0, 0]
# NY_p1_2 = [7359, 61, 0, 0, 0]
# NY_p3_2 = [7944, 103, 0, 0, 0]
# NY_p5_2 = [8110, 56, 0, 0, 0]


# fig = plot_passenger_wait_time(xlabel,NY_dij_2,NY_dag_2,NY_p1_2,NY_p3_2,NY_p5_2)
# plt.savefig('NY_2nd_wait_time_12am.pdf',dpi=300)

# NY_dij = [NY_dij_1[i]+NY_dij_2[i] for i in range(len(NY_dij_1))]
# NY_dag = [NY_dag_1[i]+NY_dag_2[i] for i in range(len(NY_dag_1))]
# NY_p1 = [NY_p1_1[i]+NY_p1_2[i] for i in range(len(NY_p1_1))]
# NY_p3 = [NY_p3_1[i]+NY_p3_2[i] for i in range(len(NY_p3_1))]
# NY_p5 = [NY_p5_1[i]+NY_p5_2[i] for i in range(len(NY_p5_1))]


# fig = plot_passenger_wait_time(xlabel,NY_dij,NY_dag,NY_p1,NY_p3,NY_p5)
# plt.savefig('NY_agg_wait_time_12am.pdf',dpi=300)



# NY_dij_1 = [1485, 157, 934, 557, 791, 579, 884, 676, 705, 697, 584, 783, 607, 881, 618, 721, 699, 524, 869, 562, 847, 714, 676, 737, 522, 771, 594, 828, 720, 720, 699, 675, 742, 529, 29]
# NY_dag_1 = [1485, 108, 787, 564, 750, 608, 732, 688, 606, 707, 502, 789, 706, 702, 653, 514, 820, 616, 786, 685, 632, 698, 563, 818, 640, 693, 611, 587, 776, 590, 865, 593, 753, 590, 74]
# NY_p1_1 = [1485, 98, 686, 593, 741, 599, 720, 655, 647, 670, 540, 759, 688, 698, 625, 519, 734, 600, 825, 678, 559, 743, 560, 784, 650, 710, 634, 608, 763, 584, 776, 612, 776, 601, 39]
# NY_p3_1 = [1485, 91, 628, 630, 726, 566, 717, 665, 674, 693, 492, 721, 646, 757, 665, 510, 820, 490, 834, 644, 705, 658, 540, 776, 608, 720, 622, 697, 691, 599, 758, 533, 799, 563, 24]
# xlabel = list(range(0, len(NY_dij_1)*5, 5))

# fig = plot_passenger_wait_time(xlabel,NY_dij_1,NY_dag_1,NY_p1_1,NY_p3_1)
# plt.savefig('NY_1st_wait_time_8am.pdf',dpi=300)

# NY_dij_2 = [4546, 2813, 2009, 1480, 1207, 1077, 888, 819, 770, 647, 589, 564, 496, 458, 455, 390, 415, 328, 345, 303, 310, 291, 242, 228, 204, 216, 172, 193, 165, 134, 91, 118, 64, 17, 0]
# NY_dag_2 = [3276, 2357, 1872, 1424, 1242, 1053, 966, 931, 848, 754, 705, 664, 559, 572, 536, 464, 468, 438, 414, 369, 326, 331, 296, 297, 268, 258, 217, 173, 175, 131, 123, 104, 78, 22, 0]
# NY_p1_2 = [2883, 2087, 1679, 1383, 1232, 1079, 976, 960, 843, 795, 691, 694, 648, 617, 549, 510, 545, 479, 430, 369, 390, 389, 300, 307, 271, 270, 250, 188, 190, 135, 118, 109, 73, 17, 0]
# NY_p3_2 = [2558, 1844, 1496, 1273, 1152, 1132, 1018, 893, 867, 816, 691, 722, 663, 633, 572, 560, 556, 478, 458, 434, 447, 413, 372, 377, 316, 308, 243, 225, 215, 170, 147, 113, 66, 20, 0]


# fig = plot_passenger_wait_time(xlabel,NY_dij_2,NY_dag_2,NY_p1_2,NY_p3_2)
# plt.savefig('NY_2nd_wait_time_8am.pdf',dpi=300)

# NY_dij = [NY_dij_1[i]+NY_dij_2[i] for i in range(len(NY_dij_1))]
# NY_dag = [NY_dag_1[i]+NY_dag_2[i] for i in range(len(NY_dag_1))]
# NY_p1 = [NY_p1_1[i]+NY_p1_2[i] for i in range(len(NY_p1_1))]
# NY_p3 = [NY_p3_1[i]+NY_p3_2[i] for i in range(len(NY_p3_1))]


# fig = plot_passenger_wait_time(xlabel,NY_dij,NY_dag,NY_p1,NY_p3)
# plt.savefig('NY_agg_wait_time_8am.pdf',dpi=300)



# xlabel = list(range(0, 20, 5))
# BJ_dij = [4954,1548,909,150]
# BJ_dag = [6614,1795,490,0]
# BJ_p1 = [8614,1895,390,0]
# BJ_p3 = [8614,1895,390,0]
# BJ_p5 = [8614,1895,390,0]


# fig = plot_passenger_wait_time(xlabel,BJ_dij,BJ_dag,BJ_p1,BJ_p3,BJ_p5)
# plt.savefig('BJ_2nd_wait_time.pdf',dpi=300)

# xlabel = list(range(0, 20, 5))
# BJ_dij = [4954,1548,909,150]
# BJ_dag = [6614,1795,490,0]
# BJ_p1 = [8614,1895,390,0]
# BJ_p3 = [8614,1895,390,0]
# BJ_p5 = [8614,1895,390,0]


# fig = plot_passenger_wait_time(xlabel,BJ_dij,BJ_dag,BJ_p1,BJ_p3,BJ_p5)
# plt.savefig('BJ_1st_wait_time.pdf',dpi=300)

# xlabel = list(range(0, 20, 5))
# BJ_dij = [4954,1548,909,150]
# BJ_dag = [6614,1795,490,0]
# BJ_p1 = [8614,1895,390,0]
# BJ_p3 = [8614,1895,390,0]
# BJ_p5 = [8614,1895,390,0]


# fig = plot_passenger_wait_time(xlabel,BJ_dij,BJ_dag,BJ_p1,BJ_p3,BJ_p5)
# plt.savefig('BJ_agg_wait_time.pdf',dpi=300)

def plot_avg_passenger_on_cab(xlabel,realsim):
    fig = plt.figure(figsize=(4.8,3.6))
    ax = list(range(len(xlabel)))
    plt.plot(ax,realsim,'g^-',label=r'$\alpha = 1.2$')
    plt.xticks(ax, xlabel)
    # Set the y limits making the maximum 5% greater
    plt.ylim(0.5, 1.2)

    plt.xlabel('Extension')
    plt.ylabel('Avg passenger on cab / km')
    plt.legend(loc = 4, prop={'size': 15})
    return fig

xlabel = ['Shortest Path','DAG','0.1 km','0.3 km','0.5 km']

NY_realsim = [0.88,0.98,1.03,1.11,1.13]

fig = plot_avg_passenger_on_cab(xlabel,NY_realsim)
plt.savefig('NY_avg_passenger.pdf',dpi=300)

BJ_realsim = [0.93,1.00,0.99,1.00,1.00]

fig = plot_avg_passenger_on_cab(xlabel,BJ_realsim)
plt.savefig('BJ_avg_passenger.pdf',dpi=300)

def plot_tot_dist_cab(xlabel,total,p0,p1,p2):
    fig = plt.figure(figsize=(4.8,3.6))
    ax = list(range(len(xlabel)))
    plt.plot(ax,total,'g^-',label='Total')
    plt.plot(ax,p0,'b^-',label='0 Passenger')
    plt.plot(ax,p1,'r^-',label='1 Passenger')
    plt.plot(ax,p2,'y^-',label='2 Passenger')
    plt.xticks(ax, xlabel)
    # Set the y limits making the maximum 5% greater
    plt.ylim(0, np.max(total)+10000)

    plt.xlabel('Extension')
    plt.ylabel('Total dist travelled')
    #plt.legend(loc = 4, prop={'size': 15})
    plt.legend()
    return fig

xlabel = ['Shortest Path','DAG','0.1 km','0.3 km','0.5 km']

NY_realsim = [120068,116451,118421,119118,121221]
NY_p0 = [33775.653,29945.7499,29927.5505,26701.4132,26167.9242]
NY_p1 = [66806.0004,58452.5466,54747.1104,52510.3247,53330.1891]
NY_p2 = [19486.8013,28053.5905,33747.0361,39906.4641,41723.0625]

fig = plot_tot_dist_cab(xlabel,NY_realsim,NY_p0,NY_p1,NY_p2)
plt.savefig('NY_tot_dist_cab.pdf',dpi=300)

BJ_realsim = [70697,72801,73370,77481,77753]
BJ_p0 = [14097.8869,15180.0605,15609.3111,15550.8599,15993.3056]
BJ_p1 = [55974.7839,63045.1761,61239.9355,68809.4425,63196.8861]
BJ_p2 = [2227.0876,4666.3357,4543.1839,4631.8973,5171.7219]

fig = plot_tot_dist_cab(xlabel,BJ_realsim,BJ_p0,BJ_p1,BJ_p2)
plt.savefig('BJ_tot_dist_cab.pdf',dpi=300)

