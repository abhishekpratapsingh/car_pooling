import matplotlib.pyplot as plt
import numpy as np
import pickle

# all
algos = [(0.1,'dij','Dijkstra'),(0.1,'dag','DAG'),(0.1,'dex','0.1 km ext'),(0.3,'dex','0.3 km ext')]
cab_caps = [2,3]
metrics = [('pcp','Pct of carpool'),('awt','Avg waiting time (min)'),('apk','Avg passenger / km'),('dis','Distance in km'),('rej','Rejection rate')]
# variables
alphas = [1.1,1.2,1.3]
alphas = [1.2,1.5,1.8,2.1,2.4]
alphas_def = 0
loc_dict = {'BJ': [400,800,1200,1600], 'NY': (np.arange(10)*1000+1000).tolist()}
end_dict = {'BJ': 240, 'NY': 150}
cab_def = {'BJ': 800,'NY': 3000}
start_times = [(0,'12am'),(480,'8am'),(960,'4pm')]
start_def = 1



def plot_actual_figure(xlabel,res,xlbl,ylbl):
	plt.close()
	fig = plt.figure(figsize=(4.8,3.6))
	ax = list(range(len(xlabel)))
	for i in range(len(algo)):
		plt.plot(ax,res[i],'^-',label=algos[i][2])
	plt.xticks(ax, xlabel)
	# Set the y limits making the maximum 5% greater
	# plt.ylim(0, np.max(total)+10000)

	plt.xlabel(xlbl)
	plt.ylabel(ylbl)
	#plt.legend(loc = 4, prop={'size': 15})
	plt.legend()
	return fig

def plot_relative_figure(xlabel,res,xlbl,ylbl):
	plt.close()
	fig = plt.figure(figsize=(4.8,3.6))
	ax = list(range(len(xlabel)))
	for i in range(1, len(algo)):
		rel = [x/y for x, y in zip(res[i], res[0])]
		plt.plot(ax,rel,'^-',label=algos[i][2])
	plt.xticks(ax, xlabel)
	# Set the y limits making the maximum 5% greater
	# plt.ylim(0, np.max(total)+10000)

	plt.xlabel(xlbl)
	plt.ylabel(ylbl)
	#plt.legend(loc = 4, prop={'size': 15})
	plt.legend()
	return fig



dt = pickle.load( open('output.p', 'rb') )

def get_pcp(fstr):
	if not fstr in dt:
		return np.nan
	cp = dt[fstr]['Car pooling']
	pick = dt[fstr]['Car pooling']+dt[fstr]['Cabs Dispatched']
	return cp/pick*100

def get_rej(fstr):
	if not fstr in dt:
		return np.nan
	rej = dt[fstr]['Rejection']
	pick = dt[fstr]['Car pooling']+dt[fstr]['Cabs Dispatched']
	return rej/(rej+pick)*100

def get_awt(fstr):
	if not fstr in dt:
		return np.nan
	total = 0
	wt = 0
	for k in range(0, 3):
		# print(dt[fstr]['Waiting'])
		# print(dt[fstr]['Waiting sum'])
		wt += dt[fstr]['Waiting'][k]*dt[fstr]['Waiting sum'][k]
		total += dt[fstr]['Waiting sum'][k]
	return wt/total*5

def get_apk(fstr):
	if not fstr in dt:
		return np.nan
	wt = dt[fstr]['Distance']
	return np.average(list(wt.keys()),weights=list(wt.values()))

def get_dis(fstr):
	if not fstr in dt:
		return np.nan
	wt = dt[fstr]['Distance']
	# return wt[0]
	return np.sum(list(wt.values()))


for metric in metrics:
	for loc in loc_dict.keys():
		for cab_cap in cab_caps:
			res = []
			for algo in algos:
				res_algo = []
				for alpha in alphas:
					num_cab = cab_def[loc]
					start_time = start_times[start_def]
					file_str = loc+'_'+algo[1]+str(algo[0])+'_a'+str(alpha)+'_cap'+str(cab_cap)+'_n'+str(num_cab)+'_'+start_time[1]
					if metric[0] == 'pcp':
						res_algo.append(get_pcp(file_str))
					elif metric[0] == 'awt':
						res_algo.append(get_awt(file_str))
					elif metric[0] == 'apk':
						res_algo.append(get_apk(file_str))
					elif metric[0] == 'dis':
						res_algo.append(get_dis(file_str))
					elif metric[0] == 'rej':
						res_algo.append(get_rej(file_str))

				res.append(res_algo)

			fig = plot_actual_figure(alphas,res,'Alpha',metric[1])
			out_str = loc + '_cab' + str(cab_cap) + '_alpha_'+metric[0]+'_act.pdf'
			plt.savefig(out_str,dpi=300)
			# fig = plot_relative_figure(alphas,res,'Alpha',metric[1])
			# out_str = loc + '_cab' + str(cab_cap) + '_alpha_'+metric[0]+'_rel.pdf'
			# plt.savefig(out_str,dpi=300)

for metric in metrics:
	for loc in loc_dict.keys():
		for cab_cap in cab_caps:
			res = []
			st = []
			for algo in algos:
				res_algo = []
				st = []
				for start_time in start_times:
					st.append(start_time[1])
					num_cab = cab_def[loc]
					alpha = alphas[alphas_def]
					file_str = loc+'_'+algo[1]+str(algo[0])+'_a'+str(alpha)+'_cap'+str(cab_cap)+'_n'+str(num_cab)+'_'+start_time[1]
					if metric[0] == 'pcp':
						res_algo.append(get_pcp(file_str))
					elif metric[0] == 'awt':
						res_algo.append(get_awt(file_str))
					elif metric[0] == 'apk':
						res_algo.append(get_apk(file_str))
					elif metric[0] == 'dis':
						res_algo.append(get_dis(file_str))
					elif metric[0] == 'rej':
						res_algo.append(get_rej(file_str))


				res.append(res_algo)

			fig = plot_actual_figure(st,res,'Start time',metric[1])
			out_str = loc + '_cab' + str(cab_cap) + '_stime_'+metric[0]+'_act.pdf'
			plt.savefig(out_str,dpi=300)
			# fig = plot_relative_figure(st,res,'Start time',metric[1])
			# out_str = loc + '_cab' + str(cab_cap) + '_stime_'+metric[0]+'_rel.pdf'
			# plt.savefig(out_str,dpi=300)

for metric in metrics:
	for loc in loc_dict.keys():
		for cab_cap in cab_caps:
			res = []
			nc = []
			for algo in algos:
				res_algo = []
				nc = []
				for num_cab in loc_dict[loc]:
					nc.append(num_cab)
					alpha = alphas[alphas_def]
					start_time = start_times[start_def]
					file_str = loc+'_'+algo[1]+str(algo[0])+'_a'+str(alpha)+'_cap'+str(cab_cap)+'_n'+str(num_cab)+'_'+start_time[1]
					if metric[0] == 'pcp':
						res_algo.append(get_pcp(file_str))
					elif metric[0] == 'awt':
						res_algo.append(get_awt(file_str))
					elif metric[0] == 'apk':
						res_algo.append(get_apk(file_str))
					elif metric[0] == 'dis':
						res_algo.append(get_dis(file_str))
					elif metric[0] == 'rej':
						res_algo.append(get_rej(file_str))


				res.append(res_algo)

			# print(res)
			fig = plot_actual_figure(nc,res,'No. of cabs',metric[1])
			out_str = loc + '_cab' + str(cab_cap) + '_ncabs_'+metric[0]+'_act.pdf'
			plt.savefig(out_str,dpi=300)
			# fig = plot_relative_figure(nc,res,'No. of cabs',metric[1])
			# out_str = loc + '_cab' + str(cab_cap) + '_ncabs_'+metric[0]+'_rel.pdf'
			# plt.savefig(out_str,dpi=300)