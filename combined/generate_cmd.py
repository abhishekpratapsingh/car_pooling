temp = []

with open('cmd_template.sh','r') as template:
	for line in template:
		temp.append(line)

fname = 1
prefix = '20180501_'
with open('call_param.out','r') as in_file:
	out_file = open(prefix+str(fname)+'.sh','w')
	for t in temp:
		out_file.write(t)
	for line in in_file:
		# line = line.rstrip()
		if len(line.rstrip()) == 0:
			out_file.close()
			fname += 1
			out_file = open(prefix+str(fname)+'.sh','w')
			for t in temp:
				out_file.write(t)
		else:
			out_file.write(line)
	out_file.close()