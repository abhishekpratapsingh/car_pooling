#include <bits/stdc++.h>
#include <ctime>
#include "carpooling.h"

using namespace std;

char * timeSourceName, * locationInputName, * edgeInputName;
char * txtName;
string location;
string algoUsed;

/* Parameters */
int DEBUG = 1;
bool PRINT_PATH = false;
bool FREQUENCY_OPT = false;
double alpha  = 1.3;
double maxDepth = 0.2;
long long int maxDistance = 100000;
/* nodes global structure */
vector< pair<double, double> > nodesToLatLon;
vector<long long int> nodeID;
map<long long int, long long int> idToNode;

/* Edges global structure */
vector< vector<long long int> > edges;
vector< vector<double> > edgeWeight;
vector< map<long long int, double> > edgeTime;
vector< vector<long long int> > edgesReverse;
vector< vector<double> > edgeWeightReverse;

vector< map<long long int, vector<long long int> > > sourceTimeDestination; 
vector< map<long long int, vector<long long int> > > passengerRequest;

/* Simulation */
int DAY = 6;
/*
* MODE 1: Infinity cab. After passengerWaitFactor timeSlot, the passenger will be picked up immediately
* MODE 2: Limited cab (availableCab). Infinity passengerWaitFactor
*/
int MODE = 2;
int ALGO = 0;
int availableCab = 800;
int passengerWaitFactor = 3;
int cabCapacity = 2;


const int MAX_CAB_CAPACITY = 4;

int globalPickups = 0, globalRequests = 0, globalRejections = 0;  
long long int passengerWaitTime[MAX_CAB_CAPACITY+1][24*60];
double globalDistTravelled[MAX_CAB_CAPACITY+1];

int startTime  = 0;
int endTime = 24*60;
int deltaTime = 5;
const int maxEndTime = 24*60;


struct passenger {
	int startTimeSlot;
	int pickedTimeSlot;
	int source;
	int destination;
	int passengerNo; // First passenger / Second passenger

	double shortestPathDist;
	double actualTravelDist;
};

vector< passenger > passengerPickedList;
// At source v - a list of passenger waiting for cab
map<long long int, vector< passenger > > passengerQueue;

vector<long long int> getBestPath(long long int source, long long int destination, long long int timeSlot, int reachOnly = 0, double alphaChanged = alpha ) {
	cout<<"Trip start"<<endl;
	printf("Source: %lld(%lld)\tDestination: %lld(%lld)\tTime slot: %lld\n",source,nodeID[source],destination,nodeID[destination],timeSlot); fflush(stdout);

	vector< double > distanceFromSource( nodeID.size() );
	vector< double > distanceFromDestination( nodeID.size() );
	vector< double > distanceToDestination( nodeID.size() );

	vector<long long int> dagExPath;
	dagExPath = dijkstra_lengths(nodeID.size(), source, destination, distanceFromSource, edges, edgeWeight);

	if(distanceFromSource[destination] == maxDistance || !distanceFromSource[ destination ] ) {
		printf("Cannot reach destination?\n");
		vector<long long int> emptyPath;
		return emptyPath;
	}
	
	if (reachOnly) {
		vector<long long int> emptyPath(1);
		return emptyPath;
	}

	dijkstra_lengths(nodeID.size(), destination, source, distanceToDestination, edgesReverse, edgeWeightReverse);
	dijkstra_lengths(nodeID.size(), destination, source, distanceFromDestination, edges, edgeWeight);

	if (ALGO == 1) {
		dagExPath = findDAGPath( nodeID.size(), source, destination, timeSlot, alphaChanged, sourceTimeDestination, distanceFromSource, distanceToDestination, distanceFromDestination, edges, edgeWeight);
	}

	if (ALGO == 2) {
		printf("  Start DAG EX\n"); fflush(stdout);
		dagExPath = findDAGExtendedPath( nodeID.size(), source, destination, timeSlot, alphaChanged, maxDepth, sourceTimeDestination, distanceFromSource, distanceToDestination, distanceFromDestination);
	}

	cout<<"Trip Found"<<endl; fflush(stdout);

	return dagExPath;
}

pair< vector<long long int>, int> getBestPath2(long long int source, pair<long long int, long long int> destination, long long int timeSlot, vector< double > alphaChanged ) {
	cout<<"Trip start"<<endl;
	printf("Source: %lld(%lld)\tDestination: %lld(%lld)\tTime slot: %lld\n",source,nodeID[source],destination.first,nodeID[destination.first],timeSlot);

	vector< double > distanceFromSource( nodeID.size() );
	vector< double > distanceFromDestination( nodeID.size() );
	vector< double > distanceToDestination( nodeID.size() );

	pair< vector<long long int>, int> dagExPath;
	dagExPath.first = dijkstra_lengths(nodeID.size(), source, destination.first, distanceFromSource, edges, edgeWeight);
	dagExPath.second = 0;
	if(distanceFromSource[destination.first] == maxDistance || !distanceFromSource[ destination.first ] ) {
		printf("Cannot reach destination?\n");
		vector<long long int> emptyPath;
		return make_pair(emptyPath, 0);
	}

	dijkstra_lengths(nodeID.size(), destination.first, source, distanceToDestination, edgesReverse, edgeWeightReverse);
	dijkstra_lengths(nodeID.size(), destination.first, source, distanceFromDestination, edges, edgeWeight);

	if (ALGO == 1) {
		dagExPath = findDAGPath2( nodeID.size(), source, destination, timeSlot, alphaChanged, sourceTimeDestination, distanceFromSource, distanceToDestination, distanceFromDestination, edges, edgeWeight);
	}

	if (ALGO == 2) {
		dagExPath = findDAGExtendedPath2( nodeID.size(), source, destination, timeSlot, alphaChanged, maxDepth, sourceTimeDestination, distanceFromSource, distanceToDestination, distanceFromDestination);
	}

	cout<<"Trip Found"<<endl;

	return dagExPath;
}

class cab {

private:
	int source;
	int destination;
	int pickupTime; // Pick up time for the first passenger

	double fractionLastEdge; // How much fraction the cab has traversed in the last edge?
	vector< long long int > path;
	// index into the path array
	int lastNodeIndex;
	bool arrivedDestination;
	vector<passenger> passengerPicked;

	vector<passenger> passengerOnCab;

	double totalDistTravelled[MAX_CAB_CAPACITY+1]; // Total distance travelled with k passengers on cab

public:
	void initialize(int timeSlot, passenger firstPass, vector<long long int> assignPath) {
		pickupTime = timeSlot;
		// TODO: reduce the score of the assign path so other cabs won't choose the same path
		path = assignPath;
		source = path[ 0 ];
		destination = path[ path.size() - 1];
		lastNodeIndex = 0;
		fractionLastEdge = 0;

		arrivedDestination = false;

		firstPass.shortestPathDist = query(firstPass.source, firstPass.destination);
		passengerOnCab.push_back(firstPass);

		for (int i=0; i<=cabCapacity; i++)
			totalDistTravelled[i] = 0;
	}

	void changePathAfterPickup(int currNodeIndex, vector<int> midPointList) {
		// Path should be modified to go from path[currNodeIndex] -> midStop -> dest
		int currNode = path[currNodeIndex];
		// Erase old path
		path.erase(path.begin() + currNodeIndex + 1, path.end());

		vector< double > dummy( nodeID.size() );
		for( int i = 0; i < midPointList.size() ; i++) {
			vector< long long int > midPath = dijkstra_lengths(nodeID.size(), currNode, midPointList[i], dummy, edges, edgeWeight);
			path.insert(path.end(), midPath.begin() + 1, midPath.end());
			currNode = midPointList[i];
		}

		destination = path[ path.size() - 1 ];

	}

	void recalculateDropoffPath(int currTimeSlot ) {

		vector<int> p(cabCapacity-1); // p for permute
		for (int i=0; i<p.size(); i++) {
			p[i] = i;
		}
		
		int v = path[ lastNodeIndex ];

		vector<long long int> dropoffPath;
		vector<int> dropoffPermutation;
		int bestDropoffScore = -1;

		do {
			// v -> d of p[0] -> d of p[1] -> ...
			// getBestPathX will return the best path from v to d of p[0], maintaining the compatibility with all d

			vector< double > alphaV;
			alphaV.push_back( alpha ); // base case

			bool validAlpha = true;
			double shortestDistToFirstDest = query( v, passengerOnCab[p[0]].destination );

			for (int i=0; i<p.size(); i++) {
				printf("  Permutation = %d\n",p[i]); fflush(stdout);
				double remainExtraDistance = alpha*passengerOnCab[p[i]].shortestPathDist - passengerOnCab[p[i]].actualTravelDist;
				for (int j=0; j<i; j++) {
					remainExtraDistance -= query( passengerOnCab[p[j]].destination, passengerOnCab[p[j+1]].destination );
				}
				if ( remainExtraDistance >= shortestDistToFirstDest ) {
					printf("  %.4f / %.4f\n",remainExtraDistance,shortestDistToFirstDest); fflush(stdout);
					alphaV.push_back(remainExtraDistance / shortestDistToFirstDest);
				}
				else {
					validAlpha = false;
					break;
				}
			}
			
			if ( validAlpha ) {
				printf("Recalculating path when a passenger got dropped off. "); fflush(stdout);
				for (int i=0; i<alphaV.size(); i++) {
					printf("Alpha[%d] = %.4f. ",i,alphaV[i]);
				}
				printf("\n");
				pair<vector<long long int>, int> candPath;

				// TODO: getBestPath is still hard-coded
				if (passengerOnCab.size() == 1) {
					candPath.first = getBestPath( path[lastNodeIndex], passengerOnCab[p[0]].destination, currTimeSlot, 0, alphaV[1]);
					candPath.second = 1;
				}
				else {
					candPath = getBestPath2( v, make_pair(passengerOnCab[p[0]].destination, passengerOnCab[p[1]].destination), currTimeSlot, alphaV);
				}
				if (candPath.second > bestDropoffScore) {
					bestDropoffScore = candPath.second;
					dropoffPath = candPath.first;
					dropoffPermutation = p; // this is deep copy
				}
			}

		} while (next_permutation(p.begin(), p.end()));


		if ( dropoffPath.size() == 0 ) {
			printf("ALERT! All dropoff path change are not possible\n"); fflush(stdout);
			return;
		}

		vector< double > dummy( nodeID.size() );
		path.erase(path.begin() + lastNodeIndex, path.end());
		path.insert(path.end(), dropoffPath.begin(), dropoffPath.end());
		for (int i=0; i<dropoffPermutation.size()-1; i++) {
			int d0 = passengerOnCab[dropoffPermutation[i]].destination;
			int d1 = passengerOnCab[dropoffPermutation[i+1]].destination;
			dropoffPath = dijkstra_lengths(nodeID.size(), d0, d1, dummy, edges, edgeWeight);
			path.insert(path.end(), dropoffPath.begin() + 1, dropoffPath.end());
		}

		destination = path[ path.size() - 1 ];

	}

	bool simulate(int currTimeSlot) {
		if (currTimeSlot < pickupTime) 
			return false;

		printf("Simulating cab at %lld (%d passengers) - Source: %d - Destination %d - Pickup Time %d\n",path[lastNodeIndex], (int)passengerOnCab.size(), source, destination, pickupTime);
		passengerPicked.clear();

		double timeLeft = deltaTime;
		while (timeLeft > 0 && path[lastNodeIndex] != destination) {

			if (lastNodeIndex > path.size()-2) {
				printf("!!! ALERT\n"); fflush(stdout);
			}

			if ( edgeTime[ path[ lastNodeIndex ] ][ path[ lastNodeIndex + 1 ] ] * ( 1 - fractionLastEdge ) >= timeLeft ) {
				// Move car on partial edge
				fractionLastEdge += timeLeft / edgeTime[ path[ lastNodeIndex ] ][ path[ lastNodeIndex + 1 ] ] ;
				timeLeft = 0;
			}
			else {
				// Add actual travel dist for each passenger
				for (int i=0; i<passengerOnCab.size(); i++) {
					passengerOnCab[i].actualTravelDist += edgeWeight[ path[ lastNodeIndex ] ][ path[ lastNodeIndex + 1 ] ];
				}
				totalDistTravelled[passengerOnCab.size()] += edgeWeight[ path[ lastNodeIndex ] ][ path[ lastNodeIndex + 1 ] ];

				// Move car on full edge
				timeLeft -= edgeTime[ path[ lastNodeIndex ] ][ path[ lastNodeIndex + 1 ] ] * ( 1 - fractionLastEdge );
				fractionLastEdge = 0;
				lastNodeIndex += 1;

				// Drop off passenger
				bool hasDroppedOff = false, contCheck;
				do {
					contCheck = false;
					for (int i=0; i<passengerOnCab.size(); i++) {
						if (passengerOnCab[i].destination == path[lastNodeIndex]) {
							printf("  Dropoff passenger %d\n",passengerOnCab[i].passengerNo); fflush(stdout);
							if (passengerOnCab[i].actualTravelDist > alpha * passengerOnCab[i].shortestPathDist) {
								printf("    ASSERT! Alpha constraint violated: %.4f/%.4f = %.4f\n", passengerOnCab[i].actualTravelDist, passengerOnCab[i].shortestPathDist, passengerOnCab[i].actualTravelDist/passengerOnCab[i].shortestPathDist);
							}
							passengerOnCab.erase(passengerOnCab.begin()+i);
							hasDroppedOff = true;
							contCheck = true;
							break;
						}
					}
				} while (contCheck);

				// Check if we can pick up another passenger here
				if ( path[lastNodeIndex] != destination && passengerOnCab.size() < cabCapacity ) {

					if ( passengerOnCab.size() == 1 && passengerOnCab.size() < cabCapacity) {
						int v = path[ lastNodeIndex ];

						// TODO: generalize to >2 passengers
						int d0 = passengerOnCab[0].destination;
						double atd0 = passengerOnCab[0].actualTravelDist;
						double spd0 = passengerOnCab[0].shortestPathDist;

						vector<passenger> &passList = passengerQueue[ v ];
						int firstPassengerFound = 0;
						vector< int > midPointList; 

						double tvd0 = query(v, d0);
						for (int i = 0; i < passList.size(); i++) {
							int w = passList[i].destination;	
							double td0w = query(d0, w);
							double tvw = query(v, w);
							if (tvd0 + td0w <= alpha * tvw) {
								midPointList.push_back(d0); midPointList.push_back(w);
								firstPassengerFound = i;
								break;
							}
							else {
								double twd0 = query(w, d0);
						  		if( atd0 + tvw + twd0 <= alpha * spd0) {
						  			midPointList.push_back(w); midPointList.push_back(d0);
									firstPassengerFound = i;
									break;
						  		}					
						  	}
						}

						if( firstPassengerFound ) {
							printf("  2nd passenger picked up at %d\n",v);
							passList[firstPassengerFound].pickedTimeSlot = currTimeSlot;
							passList[firstPassengerFound].passengerNo = 1;
							passList[firstPassengerFound].shortestPathDist = query(passList[firstPassengerFound].source, passList[firstPassengerFound].destination);
							passList[firstPassengerFound].actualTravelDist = 0;
							passengerOnCab.push_back(passList[firstPassengerFound]);
							passList.erase(passList.begin() + firstPassengerFound);
							changePathAfterPickup(lastNodeIndex, midPointList);
							passengerPicked.push_back(passengerOnCab[passengerOnCab.size()-1]);
						}
					}
					
					if ( passengerOnCab.size() == 2 && passengerOnCab.size() < cabCapacity) {
						int v = path[ lastNodeIndex ];

						long long int s0 = passengerOnCab[0].source;
						long long int s1 = passengerOnCab[1].source;
					  	long long int d0 = passengerOnCab[0].destination;
					  	long long int d1 = passengerOnCab[1].destination;
					
					  	double ts0d0 = passengerOnCab[0].shortestPathDist;
						double ts1d1 = passengerOnCab[1].shortestPathDist;
						double td1s1 = query(d1, s1);
				  		double tvd0 = query(v, d0);
				  		double tvd1 = query(v, d1);
				  		double td1d2 = query(d0, d1);
				  		double td2d1 = query(d1, d0);
				  		int secondPassengerFound = 0;
				  		vector< int > midPointList; 
				  		vector<passenger> &passList = passengerQueue[ v ];
					  	
					  	for (int i = 0; i < passList.size(); i++) {
							int w = passList[i].destination; 

					 		double tvw = query(v, w);
					 		double td1w = query(d1, w);
					 		double twd1 = query(w, d1);
							double td0w = query(d0, w);
							double twd0 = query(w, d0);

							/* 	1) V -> D First
								2) V -> D2 First  
								3) V -> W First */

						  	if( ( passengerOnCab[1].actualTravelDist + tvd0 + td1d2 < alpha*ts1d1) && ( tvd0 + td1d2 + td1w < alpha*tvw )  ) {
						  		midPointList.push_back(d0); midPointList.push_back(d1); midPointList.push_back(w); 
						  		secondPassengerFound = i;
						  		break;
						  	}
						  	else if( (tvd0 + td0w < alpha*tvw) &&   (passengerOnCab[1].actualTravelDist + tvd0 + td0w +twd1 < alpha*ts1d1 ) ) {
						  		midPointList.push_back(d0); midPointList.push_back(w); midPointList.push_back(d1); 
						  		secondPassengerFound = i;
						  		break;
						  	}
						  	else if ( passengerOnCab[0].actualTravelDist + tvd1 + td2d1 < alpha*ts0d0 && ( tvd1 + td2d1 + td0w < alpha*tvw ) ) {
						  		midPointList.push_back(d1); midPointList.push_back(d0); midPointList.push_back(w); 
						  		secondPassengerFound = i;
						  		break;
						  	}
							else if ( (tvd1 + td1w < alpha*tvw) &&  ( passengerOnCab[0].actualTravelDist + tvd1 + td1w +twd0  < alpha*ts0d0 ) ) {
						  		midPointList.push_back(d1); midPointList.push_back(w); midPointList.push_back(d0); 
						  		secondPassengerFound = i;
						  		break;
						  	}
						  	else if( ( passengerOnCab[0].actualTravelDist + tvw + twd0 < alpha*ts0d0 ) && ( passengerOnCab[1].actualTravelDist + tvw + twd0 + td1d2 < alpha*ts1d1 ) ) {
						  		midPointList.push_back(w); midPointList.push_back(d0); midPointList.push_back(d1); 
						  		secondPassengerFound = i;
						  		break;
						  	}
						  	else if( ( passengerOnCab[1].actualTravelDist + tvw + twd1 < alpha*ts1d1) &&   (passengerOnCab[0].actualTravelDist + tvw + twd1 +td2d1 ) < alpha*ts0d0 ) {
						  		midPointList.push_back(w); midPointList.push_back(d1); midPointList.push_back(d0); 	
						  		secondPassengerFound = i;
						  		break;
						  	}
						}

						if( secondPassengerFound ) {
							printf("  3rd passenger picked up at %d\n",v);
							passList[secondPassengerFound].pickedTimeSlot = currTimeSlot;
							passList[secondPassengerFound].passengerNo = 2;
							passList[secondPassengerFound].shortestPathDist = query(passList[secondPassengerFound].source, passList[secondPassengerFound].destination);
							passList[secondPassengerFound].actualTravelDist = 0;
							passengerOnCab.push_back(passList[secondPassengerFound]);
							passList.erase(passList.begin() + secondPassengerFound);
							changePathAfterPickup(lastNodeIndex, midPointList);
							passengerPicked.push_back(passengerOnCab[passengerOnCab.size()-1]);
						}

					}
				}

				// Single passenger remaining, reroute the route
				if ( path[lastNodeIndex] != destination && passengerOnCab.size() < cabCapacity && hasDroppedOff ) {
					printf("  Recalculating drop off\n"); fflush(stdout);
					recalculateDropoffPath(currTimeSlot);
					
				}
			
			}

		}

		if (path[ lastNodeIndex ] == destination) {
			arrivedDestination = true;
		}

		return (arrivedDestination) ;
	}

	int getNumPassenger() {
		return passengerOnCab.size();
	}

	vector<passenger> getPassengerPicked() {
		return passengerPicked;
	}

	int getDestination() {
		return destination;
	}

	double getTotalDistTravelled(int k) {
		return totalDistTravelled[k];
	}


};

void getTimeSourceDestination_Other() {
	ifstream file;
	file.open( timeSourceName );
	string s;
	int trips = 0;
	sourceTimeDestination.resize(nodeID.size());
	passengerRequest.resize( maxEndTime / deltaTime );
	while( getline(file, s) ) {
		stringstream ss( s );
		long long int date, source, timeSlot, dest;
		ss>>date>>source>>timeSlot;
		if (date > DAY) continue;
		if (!(timeSlot >= startTime / deltaTime)) continue;
		source = idToNode[source];
		while( ss>>dest ) {
			trips++;
			dest = idToNode[dest];
			if (date < DAY)
				sourceTimeDestination[ source ][ timeSlot ].push_back( dest );
			else {
				if (timeSlot < endTime / deltaTime)
					passengerRequest[ timeSlot ][ source ].push_back( dest );
			}
		}
	}
	file.close();
	cout<<trips<<endl;
}

void getTimeSourceDestination_NY() {
	ifstream file;
	file.open( timeSourceName );
	string s;
	sourceTimeDestination.resize( nodeID.size() );
	passengerRequest.resize( maxEndTime / deltaTime );
	int trips = 0;
	while( getline(file, s) ) {
		stringstream ss( s );
		long long int date, source, timeSlot, dest;
		ss>>date>>source>>timeSlot>>dest;
		//printf("Hello %lld %lld %lld %lld\n",date,source,timeSlot,dest);
		if (date > DAY) continue;
		if (source < nodeID.size() && dest < nodeID.size() && timeSlot >= startTime / deltaTime) {
			trips++;

			if( date < DAY )
				sourceTimeDestination[ source ][ timeSlot ].push_back( dest );
			else {
				if (timeSlot < endTime / deltaTime)
					passengerRequest[ timeSlot ][ source ].push_back( dest );	
			}
		}
		
	}

	file.close();
	cout<<trips<<endl;
}

void getTimeSourceDestination() {
	if (location.compare("NY") == 0) {
		getTimeSourceDestination_NY();
	}
	else {
		getTimeSourceDestination_Other();
	}
}

void takeGraphInput() {
	ifstream Location;
	Location.open( locationInputName );
	string s;
	int index = 0, numNodes = 0, numEdge = 0;
	
	getline(Location, s);
	stringstream ss(s);
	char ch;
	if (location.compare("BJ") == 0) {
		ss>>numNodes>>ch>>numEdge;
	}

	while( getline(Location, s) ) {
		ss.str( std::string() );
		ss.clear();
		ss<<s;
		long long int id; double lon; double lat; 
		ss>>id>>ch>>lat>>ch>>lon; 
		nodesToLatLon.push_back( make_pair( lat, lon) );
		nodeID.push_back( id );
		idToNode[ id ] = index;
		//printf("%lld = %d\n",id,index);
		index++;
		if(location.compare("BJ") == 0 && nodesToLatLon.size() == numNodes)
			break;
	}

	if (location.compare("SF") == 0 || location.compare("NY") == 0) {
		Location.close();
		Location.open( edgeInputName );
	}

	// Get edges
	int count = 0;
	edges.resize(nodeID.size());
	edgeWeight.resize(nodeID.size());
	edgeTime.resize(nodeID.size());
	edgesReverse.resize(nodeID.size());
	edgeWeightReverse.resize(nodeID.size());
	while( getline(Location, s) ) {
		ss.str( std::string() );
		ss.clear();
		ss<<s;
		long long int numRandom; long long int node1; long long int node2; double weight; char ch; int oneWay = 1; double timeNeeded;
		if (location.compare("BJ") == 0) {
			ss>>node1>>ch>>node2>>ch>>weight>>ch>>oneWay>>ch>>timeNeeded;
		}
		else {
			ss>>node1>>ch>>node2>>ch>>weight>>ch>>oneWay>>ch>>timeNeeded;
		}
		node1 = idToNode[node1];
		node2 = idToNode[node2];
		if (location.compare("NY") == 0) {
			weight /= 1000;
		}
		
		edges[ node1 ].push_back( node2 );
		edgeWeight[ node1 ].push_back( weight );
		edgeTime[ node1 ][ node2 ] = timeNeeded;
		edgesReverse[ node2 ].push_back( node1 );
		edgeWeightReverse[ node2 ].push_back( weight );
		count = oneWay ? count +1: count+2;
		if( !oneWay ) {
			long long int temp = node1; node1 = node2; node2 = temp;
			edges[ node1 ].push_back( node2 );
			edgeWeight[ node1 ].push_back( weight );
			edgeTime[ node1 ][ node2 ] = timeNeeded;

			edgesReverse[ node2 ].push_back( node1 );
			edgeWeightReverse[ node2 ].push_back( weight );
		}

	}
	cout<<count+1<<endl;
	Location.close();
	return ;
}

pair<double, double> estimatedTimeDist( int u, int v ) {
	vector< double > distanceFromSource( nodeID.size() );
	vector<long long int> path = dijkstra_lengths(nodeID.size(), u, v, distanceFromSource, edges, edgeWeight);
	double estTime = 0;
	for (int i=0; i < path.size()-1; i++) {
		estTime += edgeTime[ path[i] ][ path[i+1] ];
	}
	if (distanceFromSource[v] > 99999) {
		distanceFromSource[v] = 0;
	}
	return make_pair(estTime, distanceFromSource[v]);
}

vector<int> initializeFreeCabs() {
	srand( 0 );
	vector<int> freeCabs;
	for (int i=0; i<availableCab; i++) {
		freeCabs.push_back(rand()%nodeID.size());
	}
	return freeCabs;
}

int main(int argc, char const *argv	[])
{
	if (argc < 8) {
		printf("Usage: ./a.out [location={BJ|SF|NY}] [maxDepth=0.2] [alpha=1.3] [mode={1|2}] [maxCab=2000] [cabCapacity=2] [algo={dij|dag|dex}] [Optional: starttime endtime]\n");
		return 0;
	}

	if (argc >= 10) {
		stringstream ssst(argv[8]);
		ssst>>startTime;
		stringstream sset(argv[9]);
		sset>>endTime;
	}

	stringstream ssMode(argv[4]);
	ssMode>>MODE;
	algoUsed = argv[7];
	if (algoUsed.compare("dij") == 0) {
		ALGO = 0;
	}
	else if (algoUsed.compare("dag") == 0) {
		ALGO = 1;
	}
	else if (algoUsed.compare("dex") == 0) {
		ALGO = 2;
	}
	else {
		printf("Algo not recognized.");
		return 1;
	}
	printf("MODE = %d\nALGO = %d\n",MODE,ALGO); fflush(stdout);

	stringstream ssAva(argv[5]);
	ssAva>>availableCab;

	stringstream ssCap(argv[6]);
	ssCap>>cabCapacity;
	printf("Available cabs = %d\nCab capacity = %d\n",availableCab,cabCapacity);

	location = argv[1];
	txtName = (char*)malloc(50);
	timeSourceName = (char*)malloc(50);
	locationInputName = (char*)malloc(50);
	edgeInputName = (char*)malloc(50);
	if (location.compare("BJ") == 0) {
		sprintf(txtName, "beijingIndex");
		sprintf(timeSourceName, "bj_output_simulation_5");
		sprintf(locationInputName, "bj_graph_time");
		DAY = 6;
	}
	else if (location.compare("NY") == 0) {
		sprintf(txtName, "nyIndex");
		sprintf(timeSourceName, "ny_output_simulation_5");
		sprintf(locationInputName, "ny_location");
		sprintf(edgeInputName, "ny_edge_time");
		DAY = 15;
		locationFactor = 2;
	}
	else if (location.compare("SF") == 0) {
		sprintf(txtName, "sfIndex");
		sprintf(timeSourceName, "sf_output");
		sprintf(locationInputName, "sf_location");
		sprintf(edgeInputName, "sf_edge");
	}
	else {
		printf("Location not recognized.");
		return 1;
	}
	
	string s = argv[2];
	stringstream ss(s);	
	stringstream ssDebug(argv[3]);
	ss>>maxDepth;
	ssDebug>>alpha;
	printf("Max Depth = %.2f\nAlpha = %.2f\n",maxDepth,alpha); fflush(stdout);

	takeGraphInput();
	printf("Finish taking graph input\n"); fflush(stdout);
	getTimeSourceDestination();
	printf("Finish get time source destination\n"); fflush(stdout);

	queryInit( txtName );
	intializeFrequent(nodeID.size(), FREQUENCY_OPT, sourceTimeDestination, edges, edgeWeight);
	
	assignExtendEdge(nodeID.size(), maxDepth, edges, edgeWeight);

	// MAIN SIMULATION
	memset(passengerWaitTime, 0, sizeof(passengerWaitTime));
	int queueTimeSlot = 0,remainingQueueSize = 0;
	int maxWaitedSlot = passengerWaitFactor;
	vector< cab > cabs;
	vector< int > freeCabs = initializeFreeCabs();
	for (int k=0; k<=cabCapacity; k++) {
		globalDistTravelled[k] = 0;
	}

	for(int globalTime = startTime; globalTime < endTime || remainingQueueSize > 0; globalTime += deltaTime) {
		
		int timeSlot = globalTime / deltaTime;

		printf("=== Starting at time slot %d ===\n",timeSlot); fflush(stdout);
		if (globalTime >= maxEndTime) {
			printf("Max end time reached %d -- FORCE STOP\n",maxEndTime);
			break;
		}
		printf("Remaining queue size = %d\n",remainingQueueSize);

		printf("== Adding passenger ==\n"); fflush(stdout);
		// Add all passengers who arrive at this timeSlot to passengerQueue
		for (map<long long int, vector<long long int> >::iterator iter = passengerRequest[ timeSlot ].begin(); 
					iter != passengerRequest[ timeSlot ].end(); iter++) {
			int u = iter->first;
			vector<long long int> destList = iter->second;
			for (int i=0; i < destList.size(); i++) {
				int v = destList[i];
				passenger p;
				p.startTimeSlot = timeSlot;
				p.pickedTimeSlot = -1;
				p.source = u;
				p.destination = v;
				
				passengerQueue[u].push_back(p);
				remainingQueueSize++;
			}			
		}

		printf("== Simulate each existing cab ==\n"); fflush(stdout);
		// Simulate each existing cab
		vector<cab> newCabs;
		for( int i = 0; i < cabs.size(); i++) {
			bool terminate = cabs[ i ].simulate( timeSlot );
			if ( cabs[ i ].getPassengerPicked().size() > 0 ) {
				// We picked up some passengers in this turn
				vector< passenger > passengerPicked = cabs[ i ].getPassengerPicked();
				printf("  Picked up %d passengers\n",(int)passengerPicked.size()); fflush(stdout);
				globalPickups += passengerPicked.size();
				passengerPickedList.insert( passengerPickedList.end(), passengerPicked.begin(), passengerPicked.end() ); 
				remainingQueueSize -= passengerPicked.size();
			}
			if (terminate) {
				// The cab arrives at destination
				availableCab++;
				freeCabs.push_back( cabs[i].getDestination() );

				// Update global dist calculation
				for (int j=1; j<=cabCapacity; j++) {
					globalDistTravelled[j] += cabs[i].getTotalDistTravelled(j);
				}
			}
			else {
				newCabs.push_back( cabs[ i ] );
			}
		}

		// For the passenger requests which are not picked up, assign a cab to pick it up
		if (MODE == 1) {
			printf("== Entering MODE 1 ==\n"); fflush(stdout);
			int tooOldTimeSlot = timeSlot - passengerWaitFactor;
			for (map<long long int, vector<passenger> >::iterator iter = passengerQueue.begin(); 
						iter != passengerQueue.end(); iter++) {
				int u = iter->first;
				vector<passenger> &passList = iter->second;
				int i = 0;
				for (i=0; i < passList.size(); i++) {
					if (passList[i].startTimeSlot > tooOldTimeSlot) break;

					int v = passList[i].destination;
					vector<long long int> path = getBestPath(u, v, timeSlot);
					// Not reachable case
					if ( path.size() == 0 ) continue;
					passList[i].pickedTimeSlot = timeSlot;
					passList[i].passengerNo = 0;
					passengerPickedList.push_back(passList[i]);	
					// Initialize a cab
					cab freshcab;
					freshcab.initialize(timeSlot, passList[i], path);
					
					globalRequests += 1;
					newCabs.push_back(freshcab);
				}
				passList.erase(passList.begin(), passList.begin() + i);
				remainingQueueSize -= i;
			}
		}
		else if (MODE == 2) {
			printf("== Entering MODE 2 ==\n"); fflush(stdout);
			while (availableCab > 0 && queueTimeSlot <= timeSlot) {

				map<int, set<int> > markDelete;
				vector<int> pickupLocation;
				vector<pair<passenger, int > > pickupPassenger;

				printf("= queueTimeSlot : %d =\n", queueTimeSlot); fflush(stdout);

				for (map<long long int, vector<passenger> >::iterator iter = passengerQueue.begin(); 
							iter != passengerQueue.end(); iter++) {
					int u = iter->first;
					vector<passenger> passList = iter->second;
					
					for (int i=0; i < passList.size(); i++) {
						if (passList[i].startTimeSlot > queueTimeSlot) break;

						// Rejection case
						if ( queueTimeSlot < timeSlot - passengerWaitFactor ) {
							markDelete[u].insert(i);
							globalRejections++;
							continue;
						}

						int v = passList[i].destination;

						vector<long long int> path = getBestPath(u, v, timeSlot, 1); // TODO: duplicate calculation
						// Not reachable case
						if ( path.size() == 0 ) {
							markDelete[u].insert(i);
							continue;
						}
						pickupLocation.push_back(u);
						pickupPassenger.push_back(make_pair( passList[i], i ));
					}
					
				}

				// Hungarian Algorithm
				printf("= Start Hungarian =\n"); fflush(stdout);
				vector< int > cabToPickupAssignment = assignFreeCabs( freeCabs, pickupLocation);
				printf("= End Hungarian =\n"); fflush(stdout);

				// Move available cabs to pick up assigned passengers
				vector< int > newFreeCabs;
				for (int i=0; i<cabToPickupAssignment.size(); i++) {
					if (cabToPickupAssignment[i] == -1) {
						newFreeCabs.push_back( freeCabs[ i ] );
						continue;
					}
					
					passenger pass = pickupPassenger[ cabToPickupAssignment[i] ].first;
					int u = pass.source;
					int v = pass.destination;

					markDelete[u].insert( pickupPassenger[ cabToPickupAssignment[i] ].second ); 

					vector<long long int> path = getBestPath(u, v, timeSlot);
					
					pair<double, double> timeDist = estimatedTimeDist( freeCabs[i], u );
					globalDistTravelled[0] += timeDist.second;
					int timeSlotEst = timeSlot + timeDist.first / deltaTime; 
					printf("  Assigned cab : timeSlotEst = %d (Dist = %.4f) (Time = %.4f)\n", timeSlotEst, timeDist.second, timeDist.first); fflush(stdout);
					pass.pickedTimeSlot = timeSlotEst;
					pass.passengerNo = 0;
					passengerPickedList.push_back(pass);

					// Initialize a cab
					cab freshcab;
					freshcab.initialize(timeSlotEst, pass, path);
					globalRequests += 1;
					availableCab--;
					printf("Available Cab = %d\n",availableCab);
					newCabs.push_back(freshcab);
				}
				freeCabs = newFreeCabs;

				printf("= Delete processed passenger requests =\n"); fflush(stdout);
				// Delete processed passenger requests
				for (map<int, set<int> >::iterator iter = markDelete.begin(); iter != markDelete.end(); iter++) {
					int u = iter->first;
					vector<passenger> passList = passengerQueue[u];
					set<int> &toDelete = iter->second;
					vector<passenger> newPassList;

					for (int i=0; i < passList.size(); i++) {
						if (toDelete.find(i) == toDelete.end()) {
							newPassList.push_back(passList[i]);
						}
						else {
							remainingQueueSize--;
						}
					}
					passengerQueue[u] = newPassList;
				}

				if (availableCab > 0) {
					queueTimeSlot++;
				}
			}

		}

		
		cabs = newCabs;

		printf("Statistic for time slot (Stat %d)\n", timeSlot); fflush(stdout);
		printf("Total cabs dispatched: %d\n",globalRequests);
		printf("Total car pooling: %d\n",globalPickups);
		printf("Total rejection: %d\n",globalRejections);
		for (int i = 0; i < passengerPickedList.size(); i++) {
			if (passengerPickedList[i].pickedTimeSlot == timeSlot) {
				int waitedSlot = passengerPickedList[i].pickedTimeSlot - passengerPickedList[i].startTimeSlot;
				passengerWaitTime[passengerPickedList[i].passengerNo][waitedSlot]++;
				maxWaitedSlot = max(maxWaitedSlot, waitedSlot);
			}
		}
		for (int k = 0; k <= maxWaitedSlot; k++) {
			printf("Waited %d =",k);
			for (int i = 0; i<cabCapacity; i++) {
				printf(" %lld",passengerWaitTime[i][k]);
			}
			printf("\n");
		}
		printf("Total distance travelled with k passengers\n");
		for (int k=0; k<=cabCapacity; k++) {
			double baseDist = globalDistTravelled[k];
			for( int i = 0; i < cabs.size(); i++) {
				baseDist += cabs[i].getTotalDistTravelled(k);
			}
			printf("Passenger %d = %.4f\n",k,baseDist);
		}
		printf("\n"); fflush(stdout);
	}


	

	return 0;
}

