import sys

file1 = open(sys.argv[1])

scoreDAG = 0
scoreDAGEX = 0
timeDAG = 0
timeDAGEX = 0
maxO = 28
for x in range(0, 200):
	if( x >= maxO ):
		break
	bre = False
	while (True):
		temp = file1.readline().strip("\n")
		token = temp.split("\t")
		if (token[0] == 'Trip' and len(token) >= 2):
			break

	score = file1.readline().strip("\n").split("\t")[1:]
	time = file1.readline().strip("\n").split("\t")[1:]
	# print score
	scoreDAG += float( score[2] )/int( score[0] )
	timeDAG += (float( time[2]) + 0.001)
	scoreDAGEX += float( score[3] )/int( score[0] )
	print float( score[3] )/int( score[0] )
	timeDAGEX += (float( time[3]) + 0.001 )

print scoreDAG/maxO, timeDAG/maxO
print scoreDAGEX/maxO, timeDAGEX/maxO
