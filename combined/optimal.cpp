
int bfs(int source, int destination, map<int, int> &path, vector<long long int> &scores,
	int score, int pathLen, double pathLenMax) {
	
	if( ( source == destination ) && ( pathLen < pathLenMax ) ) {
		return score;
	}
	
	if( ( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC > 480) {
		return -1;
	}

	if( ( pathLen + query(source, destination) ) > pathLenMax )
		return 0;

	path[ source ] = 1;
	int maxx = -1;
	//cout<<( pathLen + query(source, destination) ) << " "<<pathLenMax<<endl;

	for(int j = 0; j < edges[ source ].size(); j++) {
		int newNode = edges[ source ][ j ];
		if( path[ newNode ] )
			continue;
		maxx = max( maxx, bfs( newNode, destination, path, scores,
		score + scores[ newNode ], pathLen + edgeWeight[ source ][ j ] , pathLenMax) );
	}	

	path[ source ] = 0;

	return maxx;
}