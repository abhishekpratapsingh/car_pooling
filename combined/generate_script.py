
import numpy as np

# all
# algos = [(0.1,'dij'),(0.1,'dag'),(0.1,'dex'),(0.3,'dex'),(0.5,'dex')]
algos = [(0.1,'dij'),(0.1,'dag'),(0.1,'dex'),(0.3,'dex')]
cab_caps = [2,3]
# variables
alphas = [1.1,1.2,1.3]
alphas_def = 1
loc_dict = {'BJ': [400,800,1200,1600], 'NY': (np.arange(17)*250+1000).tolist()}
end_dict = {'BJ': 240, 'NY': 150}
cab_def = {'BJ': 800,'NY': 3000}
start_times = [(0,'12am'),(480,'8am'),(960,'4pm')]
start_def = 1

param_set = set()

with open('call_param.out','w') as out_file:
	for loc in loc_dict.keys():
		for algo in algos:
			for cab_cap in cab_caps:
				for alpha in alphas:
					num_cab = cab_def[loc]
					start_time = start_times[start_def]
					cmd_str = './realtime_sim '+loc+' '+str(algo[0])+' '+str(alpha)+' 2 '+str(num_cab)+' '+str(cab_cap)+' '+algo[1]+' '+str(start_time[0])+' '+str(start_time[0]+end_dict[loc]);
					file_str = '20180501/'+loc+'_'+algo[1]+str(algo[0])+'_a'+str(alpha)+'_cap'+str(cab_cap)+'_n'+str(num_cab)+'_'+start_time[1]
					if not file_str in param_set:
						out_file.write(cmd_str + ' > ' + file_str + '\n')
						param_set.add(file_str)
	for loc in loc_dict.keys():
		for algo in algos:
			for cab_cap in cab_caps:
				for num_cab in loc_dict[loc]:
					alpha = alphas[alphas_def]
					start_time = start_times[start_def]
					cmd_str = './realtime_sim '+loc+' '+str(algo[0])+' '+str(alpha)+' 2 '+str(num_cab)+' '+str(cab_cap)+' '+algo[1]+' '+str(start_time[0])+' '+str(start_time[0]+end_dict[loc]);
					file_str = '20180501/'+loc+'_'+algo[1]+str(algo[0])+'_a'+str(alpha)+'_cap'+str(cab_cap)+'_n'+str(num_cab)+'_'+start_time[1]
					if not file_str in param_set:
						out_file.write(cmd_str + ' > ' + file_str + '\n')
						param_set.add(file_str)
	for loc in loc_dict.keys():
		for algo in algos:
			for cab_cap in cab_caps:
				for start_time in start_times:
					alpha = alphas[alphas_def]
					num_cab = cab_def[loc]
					cmd_str = './realtime_sim '+loc+' '+str(algo[0])+' '+str(alpha)+' 2 '+str(num_cab)+' '+str(cab_cap)+' '+algo[1]+' '+str(start_time[0])+' '+str(start_time[0]+end_dict[loc]);
					file_str = '20180501/'+loc+'_'+algo[1]+str(algo[0])+'_a'+str(alpha)+'_cap'+str(cab_cap)+'_n'+str(num_cab)+'_'+start_time[1]
					if not file_str in param_set:
						out_file.write(cmd_str + ' > ' + file_str + '\n')
						param_set.add(file_str)
