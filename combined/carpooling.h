#include <bits/stdc++.h>
#include "ioD.h"
#include "Hungarian.h"

using namespace std;

// Query utilities

char * degName, * outName, *inName;
edgeL * deg;
edgeS * labelout, *labelin;
edgeS * labelx, * labely;

map< pair<int, int>, double > timeOptimize; 
map< int, map<int, double> > distanceFrequentNodes;
vector< bool > frequentPickup;
vector< bool > frequentDrop;

int locationFactor = 1;


// Takes in node index
double query(int x, int y)
{	
	// if we already have this key pair value then return 
	if( frequentPickup[ x ] && frequentDrop[ y ] ) {
		return distanceFrequentNodes[ x ][ y ];
	}
	
	if( timeOptimize.find( make_pair(x, y) ) != timeOptimize.end() ) {
		return timeOptimize[ make_pair(x, y) ]; 
	}

	if (x == y) return 0;
	int xx = x, yy = y;

	x = ((deg[xx].x<<32)>>32);
	y = ((deg[yy].x<<32)>>32);
		
	if (x > y)
	{
		labelx = labelout + deg[xx].w;
		labely = labelin + deg[yy].y;
	}
	else
	{
		int xy = x; x = y; y = xy;
		labelx = labelin + deg[yy].y;
		labely = labelout + deg[xx].w;
	}

	int ans = 1000000, i = 0, j = 0;

	if (labelx[i].x != -1 && labely[j].x != -1)
	while (labelx[i].x < y)
	{
		if (labelx[i].x == labely[j].x) 
		{
			ans = ans>(labelx[i].w + labely[j].w)?(labelx[i].w + labely[j].w):ans;
			if (labelx[++i].x == -1) break;
			if (labely[++j].x == -1) break;
		}
		else if (labelx[i].x < labely[j].x)
		{
			if (labelx[++i].x == -1) break;
		}
		else if (labely[++j].x == -1) break;
	}
	
	while (labelx[i].x != -1 && labelx[i].x < y) i++;
	if (labelx[i].x == y) ans = ans>labelx[i].w?labelx[i].w:ans;

	// save the key-pair value here 

	timeOptimize[ make_pair(x, y) ] = float(ans * locationFactor)/1000;
	
	// For NY it is 2, otherwise it is 1
	return float(ans * locationFactor)/1000;
}

void loadIndex()
{
	long long n;
	inBufL degBuf(degName);
	inBufS inLabel(inName), outLabel(outName);
	
	n = checkB(degName)/sizeof(edgeL);

	deg = (edgeL *)malloc(sizeof(edgeL)*n);
	labelin = (edgeS*)malloc(checkB(inName));
	labelout = (edgeS*)malloc(checkB(outName));

	printf("%lld vertices\n", n);

	degBuf.start();
	for (int i = 0; i < n; i++)
		degBuf.nextEdge(deg[i]);

	inLabel.start();
	for (int i = 0; !inLabel.isEnd; i++)
		inLabel.nextEdge(labelin[i]);
	
	outLabel.start();
	for (int i = 0; !outLabel.isEnd; i++)
		outLabel.nextEdge(labelout[i]);			
}

int getPathScore(vector<long long int> &path, vector< long long int > &weights) {
	int score = 0;
	for(int i=1;i<path.size()-1; i++) {
		score += weights[ path[i] ];
	}
	return score;
}

vector<long long int> dijkstra_lengths(int N, long long int S, long long int D, vector< double > &distanceFromSource,
	vector< vector<long long int> > &edges, vector< vector<double> > &edgeWeight);

/* new function: we store distance to and from nodes which are in 
top 10 percentile wrt number of trips from them
*/
void intializeFrequent(int N, bool frequentOptimize, vector< map<long long int, vector<long long int> > > &sourceTimeDestination,
	vector< vector<long long int> > &edges, vector< vector<double> > &edgeWeight) {

	frequentPickup.resize( N, false);
	frequentDrop.resize( N, false);
	if (!frequentOptimize) {
		return;
	}

	printf("INITIALIZE SHORTEST DISTANCE FOR FREQUENT NODES\n"); fflush(stdout);

	// frequent pickups
	vector<int> v;
	for( int i = 0; i< N; i++ ) {
		int count = 0;
		for( int j = 0; j < 96; j++) {
			count += sourceTimeDestination[ i ][ j ].size() ; 
		}
		v.push_back( count );
	}
	sort( v.begin(), v.end() );
	int threshold = v[ v.size()*90/100 ];
	
	for( int i = 0; i< N; i++ ) {
		int count = 0;
		for( int j = 0; j < 96; j++) {
			count += sourceTimeDestination[ i ][ j ].size() ; 
		}
		
		if(  count >= threshold ) {
			frequentPickup[ i ] = 1;
		}
		else 
			frequentPickup[ i ] = 0; 
	}

	// frequent drops offs
	vector<int> des( N, 0);
	for( int i = 0; i< N; i++ ) {
		for( int j = 0; j < 96; j++) {
			for( int k = 0; k < sourceTimeDestination[ i ][ j ].size(); k++) {
				des[ sourceTimeDestination[ i ][ j ][ k ] ] += 1;
			} 
		}
	}

	vector< int > desSort = des; 
	sort( desSort.begin(), desSort.end() );
	threshold = desSort[ desSort.size()*90/100 ];
	
	for( int i = 0; i< N; i++ ) {
		if(  des[ i ] >= threshold ) {
			frequentDrop[ i ] = 1;
		}
		else 
			frequentDrop[ i ] = 0; 
	}

	for( int i = 0; i< N; i++ ) {	
		if( !frequentPickup[ i ] ) 
			continue;

	//	cout<<i<<" "<<nodeID.size()<<endl;
		vector< double > distanceFromSourceL( N );
		dijkstra_lengths(N, i, -1, distanceFromSourceL, edges, edgeWeight);
	
		for( int j = 0;  j < N; j++ ) {
			if( frequentDrop[ j ] ) {
				distanceFrequentNodes[ i ][ j ] = distanceFromSourceL[ j ]; 
			} 
		}
		//cout<<distanceFrequentNodes[i].size()<<endl;
	}

	cout<<"SIZE OF FREQUENT NODES"<<distanceFrequentNodes.size()<<endl;
}

void queryInit(char *txtName) {
	degName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(degName, "%s.deg", txtName);
	
	inName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(inName, "%s.labelin", txtName);
	outName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(outName, "%s.labelout", txtName);

	timer tm;
	loadIndex();
	printf("load time %lf (ms)\n", tm.getTime()*1000); fflush(stdout);
}


/* returns assignment of cabs to locations using Hungarian algorithm */
vector< int > assignFreeCabs( vector< int > &cabsLocation, vector< int > &pickups ) {
	vector< vector<double> > costMatrix( cabsLocation.size() );
	for (unsigned int x = 0; x < costMatrix.size(); x++)
		for( int i = 0; i < pickups.size(); i++) {
			costMatrix[ x ].push_back( query(cabsLocation[ x ], pickups[ i ] ) );
		}	
	
	HungarianAlgorithm HungAlgo;
	vector<int> assignment;

	double cost = HungAlgo.Solve(costMatrix, assignment);

	return assignment;
}  


// Subset functions

void subset_edge(map<int, int> &marked, vector< vector<long long int> > &edges, vector< vector<double> > &edgeWeight) {
	vector< vector<long long int> > edgesTrunc;
	vector< vector<double> > edgeWeightTrunc;
	edgesTrunc.resize(marked.size());
	edgeWeightTrunc.resize(marked.size());
	for (int i=0; i<edges.size(); i++) {
		int u = i;
		if (marked.find(u) == marked.end()) continue;
		for (int j=0; j<edges[i].size(); j++) {
			int v = edges[i][j];
			double w = edgeWeight[i][j];
			if (marked.find(v) == marked.end()) continue;
			int mu = marked[u];
			int mv = marked[v];
			edgesTrunc[mu].push_back(mv);
			edgeWeightTrunc[mu].push_back(w);
			
		}
	}
	edges.clear();
	edgeWeight.clear();
	edges = edgesTrunc;
	edgeWeight = edgeWeightTrunc;
}

void subset_dataset(map<int, int> &marked, vector< map<long long int, vector<long long int> > > &dataset) {
	vector< map<long long int, vector<long long int> > > datasetTrunc;
	datasetTrunc.resize(marked.size());
	for (int i=0; i<dataset.size(); i++) {
		int u = i;
		if (marked.find(u) == marked.end()) continue;
		for (map<long long int, vector<long long int> >::iterator iter=dataset[i].begin(); iter!=dataset[i].end(); iter++) {
			long long int timeSlot = iter->first;
			vector<long long int> destList = iter->second;
			for (int j=0; j<destList.size(); j++) {
				int v = destList[j];
				if (marked.find(v) == marked.end()) continue;
				int mu = marked[u];
				int mv = marked[v];
				datasetTrunc[mu][timeSlot].push_back(mv);
			}
			
		}
	}
	dataset.clear();
	dataset = datasetTrunc;
}



// Score calculation functions



void get_all_trips(long long int source,long long int start_time, vector<long long int> &tripList,
	 vector< map<long long int, vector<long long int> > > &dataset ) {

	if( source < dataset.size() ) {
		if( dataset[source].find(start_time) != dataset[source].end() ) {

			for(int i = 0; i < dataset[ source][start_time].size(); i++) {
				tripList.push_back( dataset[source][ start_time ][ i ] );
			}

		}
	}
}

long long int get_expected_trips(long long int source, long long int destination, long long int midStop,
 	long long int start_time, double alpha, vector< map<long long int, vector<long long int> > > &dataset,
 	const vector< double > &distanceFromSource, const vector< double > &distanceToDestination, const vector< double > &distanceFromDestination) {  	

	vector<long long int> tripList;
	
	get_all_trips(midStop, start_time, tripList, dataset);

  	int counter = 0;

  	long long int s = source;
  	long long int d = destination;
  	long long int v = midStop;
	double tvd, tsv;
	// no need ot calculate them for every iteration 
	if( tripList.size() ) {
		tsv = distanceFromSource[ v ];
  		tvd = distanceToDestination[ v ];
  	}

  	for(int i = 0; i < tripList.size(); i++) {

  		long long int w = tripList[ i ];
 		double tvw = query(v, w);
		double tdw = distanceFromDestination[ w ];

	  	if( tvd + tdw < alpha*tvw ) {
	  		counter += 1;
	  	}
	  	else {

	  		double twd = distanceToDestination[ w ];
	  		double distanceA = ( tsv + tvw + twd);
	  		double distanceB = alpha*distanceFromSource[d];
	  		if( distanceA <= distanceB) {
	  			counter += 1;
	  		}
	  	}
  	}

  	return counter;
}

long long int get_expected_trips2(long long int source, pair< long long int, long long int> destination, long long int midStop,
 	long long int start_time, vector<double> alphaParameter, vector< map<long long int, vector<long long int> > > &dataset,
 	const vector< double > &distanceFromSource, const vector< double > &distanceToDestination, const vector< double > &distanceFromDestination) {  	

	vector<long long int> tripList;
	
	get_all_trips(midStop, start_time, tripList, dataset);

  	int counter = 0;
  	double alpha = alphaParameter[0]; double alpha1 = alphaParameter[1]; double alpha2 = alphaParameter[2];
  	long long int s = source;
  	long long int d = destination.first;
  	long long int d2 = destination.second;
  	long long int v = midStop;
	double tvd, tsv, ts_d2, td_d2, td2_d, tsd, tv_d2;
	// no need ot calculate them for every iteration 
	if( tripList.size() ) {
		tsd = distanceFromSource[ d ];
		tsv = distanceFromSource[ v ];
		ts_d2 = distanceFromSource[ d2 ];
  		tvd = distanceToDestination[ v ];
  		td_d2 = distanceFromDestination[ d2 ];
  		td2_d = distanceToDestination[ d2 ];
  		tv_d2 = query(v, d2);
  	}

  	for(int i = 0; i < tripList.size(); i++) {

  		long long int w = tripList[ i ];
 		double tvw = query(v, w);
 		double td2_w = query(d2, w);
 		double tw_d2 = query(w, d2);
		double tdw = distanceFromDestination[ w ];
		double twd = distanceToDestination[ w ];

		/* 	1) V -> D First
			2) V -> D2 First  
			3) V -> W First */
	  	if(  ( tvd + td_d2 < alpha2*ts_d2 && ( tvd + td_d2 + td2_w < alpha*tvw ) ) 
	  				|| (tvd + tdw < alpha*tvw) &&  (tvd + tdw +tw_d2 ) < alpha2*ts_d2  ) {
	  		counter += 1;
	  	}
	  	else if( ( tv_d2 + td2_d < alpha1*tsd && ( tv_d2 + td2_d + tdw < alpha*tvw ) ) 
	  				|| ( (tv_d2 + td2_w < alpha*tvw) &&  (tv_d2 + td2_w +twd  < alpha1*tsd ) ) ) {
	  		
	  		counter += 1;
	  	}
	  	else if( ( tvw + twd < alpha1*tsd && ( tvw + twd + td_d2 < alpha2*ts_d2 ) ) 
	  				|| ( tvw + tw_d2 < alpha2*ts_d2) &&  (tvw + tw_d2 +td2_d ) < alpha1*tsd ) {
	  		
	  		counter += 1;
	  	}
  	}

  	return counter;
}


// Main algoirthms


/*
Dijkstra algorithm
Given (S,D), return the shortest path
*/
vector<long long int> dijkstra_lengths(int N, long long int S, long long int D, vector< double > &distanceFromSource,
	vector< vector<long long int> > &edges, vector< vector<double> > &edgeWeight) { 

   	vector<long long int> prevNode(N);
	for(int i=0; i< N; i++)
	{ 	distanceFromSource[ i ] = 100000;
		prevNode[ i ] = -1;
	}

	distanceFromSource[ S ] = 0;
	priority_queue< pair<float, long long int> > dj;
	dj.push( make_pair(0, S) );
	
	pair<float, long long int> x;
	long long int u, v;
	float alt;

	while( dj.size() ) { 
		x = dj.top(); 
		dj.pop();
		u = x.second;

		for(int i=0; i < edges[ u ].size(); i++) { 	
			v = edges[ u ][ i ];
			alt = distanceFromSource[ u ] + edgeWeight[ u ][ i ];
			if( alt < distanceFromSource[ v ] )
			{ 	distanceFromSource[ v ] = alt;
				dj.push( make_pair( -alt, v) );
				prevNode[ v ] = u;
			}
		}
	}

	vector<long long int> path;
	long long int node = D;
	while( true ) {
		path.push_back( node );
		if( ( node == S ) || ( prevNode[ node ] == -1) )
		  	break;
		node = prevNode[ node ];
	}
	
	reverse(path.begin(), path.end());

	return path;
}

/*
Greedy algorithm:
Given (S,D), take a step where node score / edge weight is maximized. Return a path
*/
vector<long long int> greedyScoreDivDist(int N, long long int S, long long int D, double alpha, vector< long long int > &expectedTrips,
	const vector< double > &distanceFromSource, vector< vector<long long int> > &edges, vector< vector<double> > &edgeWeight) { 

	vector<float> dist(N);
 
	for(int i=0; i< N ; i++) {
		dist[ i ] = 100000;
	}

	dist[S] = 0;

	// ( (score, node), (distance, path) )
	priority_queue< pair< pair<float, long long int>, pair< float, vector<long long int> > > > dj;
	
	vector<long long int> path;
	path.push_back(S);
	dj.push( make_pair( make_pair(0, S), make_pair(0, path) ) );

	while (dj.size()) { 
		pair< pair<float, long long int>, pair< float, vector<long long int> >  > x = dj.top();
		dj.pop();
		
		long long int u = x.first.second;
		//printf(" At %lld: %.4f [%.4f]\n",u,-x.first.first, x.second.first);
		
		if( x.second.first > alpha * distanceFromSource[D]  )
			continue; 
		
		if( u == D ) {
			return x.second.second;
		}
		
		for(int i=0; i< edges[u].size(); i++) {
			long long int v = edges[u][i];

			if (find(x.second.second.begin(), x.second.second.end(), v) != x.second.second.end()) {
				continue;
			}

			float alt = -x.first.first + (edgeWeight[u][i]/(expectedTrips[v]+1) );
			if( (x.second.first + edgeWeight[u][i]) < dist[v] )
			{ 	dist[v] = (x.second.first + edgeWeight[u][i]); 
				path = x.second.second;
				path.push_back(v);
				dj.push( make_pair( make_pair(-alt, v), make_pair(dist[v], path) ) );
			}
		}
	}

	// not reachable
	path.clear();
	return path; 
}


/*
DAG score algorithm
Given (S,D), construct a DAG based on distance to destination, then perform a dynamic programming to obtain max score path
*/
vector<long long int> findDAGPath(int N, long long int source, long long int destination, long long int timeSlot, double alpha, vector< map<long long int, vector<long long int> > > &dataset,
	const vector< double > &distanceFromSource, const vector< double > &distanceToDestination, const vector< double > &distanceFromDestination, const vector< vector<long long int> > &edges, const vector< vector<double> > &edgeWeight) {

	vector< pair< double, long long int> > dag;

	// This will be calculate on demand
	vector< long long int > weights(N);

	//cout<<distanceFromSource[ destination ]<<endl;
	for(int i=0; i < N ; i++) {	
		weights[i] = -1;
		if( ( distanceFromSource[i] + distanceToDestination[i] ) <= alpha * distanceFromSource[destination] ) {
			dag.push_back( make_pair(-distanceToDestination[ i ], i ) );
		}
	}

	sort( dag.begin(), dag.end() );
	// (Distance, Last node) at dag[i] given a particular score
	vector< map<int, pair<double, long long int> > > scores( dag.size() );
  
  	vector< long long int> nodeToDagIndex(N);
	// cout<<dag.size()<<endl;
	for (int i=0; i<N; i++) {
  		nodeToDagIndex[i] = -1;
  	}
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
		
	long long int startIndex = nodeToDagIndex[source];
  	long long int endIndex = nodeToDagIndex[destination];
	/*
  	for(int i = 0; i < dag.size(); i++)
		scores[ i ][ 0 ] = make_pair(distanceFromSource[ dag[i].second ], -1);
  	*/
	scores[ startIndex ][ 0 ] = make_pair(0, -1);
	int countDead = 0;
	for(int i = startIndex; i < dag.size(); i++) {
  		// Maintain the monocity of scores[i]
		double lastDist = 1e10; // infinity		
		vector<int> delScore;
		for(map<int, pair<double, long long int> >::reverse_iterator it=scores[ i ].rbegin(); it != scores[ i ].rend(); it++) { 
			if (it->second.first >= lastDist) {
				delScore.push_back(it->first);
				continue;
			}
			lastDist = it->second.first;		
		}
		//cout<<"FIRST"<< scores[i].size()<<endl;
		for (int j = 0; j < delScore.size(); j++) {
			scores[i].erase(delScore[j]);
		}
		//cout<<scores[i].size()<<endl;
		// We need not explore this DAG node
		if ( !scores[i].size() ) {
			countDead += 1;
			continue;
		}
		long long int u = dag[i].second;
		// cout<<weights[u]<<endl;
		for(int j = 0; j < edges[u].size(); j++) {
			long long int v = edges[u][j];
		  	long long int vIndex = nodeToDagIndex[ v ];
		  	if( nodeToDagIndex[ v ] == -1 )
				continue;
		  	//some nodes are ajacent to themselves in the graph
		  	if(u == v)
				continue;

		  	if( vIndex >= i ) {

		  		// The edge weight is calculated here (lazy calculation)
				if (weights[v] == -1) {
					weights[ v ] = get_expected_trips(source, destination, v, timeSlot, alpha, dataset, 
						distanceFromSource, distanceToDestination, distanceFromDestination);
				}

		  		for(map<int, pair<double, long long int> >::iterator it=scores[ i ].begin(); it != scores[ i ].end(); it++)
				{ 
					// cout<<it->first<<endl;
					if( it->first > 10000)
						continue;

					int curScore = it->first + weights[v];
					double prevDist = it->second.first;
			  		if( scores[ nodeToDagIndex[v] ].find( curScore ) != scores[ nodeToDagIndex[v] ].end() ) {
					  	if( scores[ nodeToDagIndex[v] ][curScore].first >= prevDist + edgeWeight[u][j] ) {
							scores[ nodeToDagIndex[v] ][curScore] = make_pair(prevDist + edgeWeight[u][j], u);
						}
					}
					else
					{
						scores[ nodeToDagIndex[v] ][curScore] = make_pair(prevDist + edgeWeight[u][j], u);	
					}
				}
			}
		}
	}
	//cout<<"DEAD = "<< countDead<<" "<<dag.size() - startIndex<<endl;
	long long int bestScore = 0;

	for( map<int, pair<double, long long int> >::iterator it=scores[ endIndex ].begin(); it != scores[ endIndex ].end(); it++) {
		//printf(" -- Score = %d (%.4f < %.4f?)\n",it->first, it->second.first, distFromSourceToDestination*alpha);
		if( it->second.first <= alpha * distanceFromSource[destination]  ) {
			bestScore = it->first;
		}
	}
	vector<long long int> path;
	long long int traceLocation = destination;
	int traceScore = bestScore;
	while (traceLocation != -1) {
		//printf(" At %lld[%lld]: Score = %d (dist: %.4f)\n",traceLocation, nodeToDagIndex[traceLocation], traceScore, scores[ nodeToDagIndex[traceLocation] ][traceScore].first);
		path.push_back(traceLocation);
		int prevScore = traceScore - weights[traceLocation];
		traceLocation = scores[ nodeToDagIndex[traceLocation] ][traceScore].second;	
		traceScore = prevScore;
	}
	reverse(path.begin(), path.end());

	//cout<<"BEST SCORE:   "<<bestScore<<endl;
	return path;

}

pair< vector<long long int>, int> findDAGPath2(int N, long long int source, pair< long long int, long long int> destination, long long int timeSlot, vector< double > alpha, vector< map<long long int, vector<long long int> > > &dataset,
	const vector< double > &distanceFromSource, const vector< double > &distanceToDestination, const vector< double > &distanceFromDestination, const vector< vector<long long int> > &edges, const vector< vector<double> > &edgeWeight) {

	vector< pair< double, long long int> > dag;

	// This will be calculate on demand
	vector< long long int > weights(N);

	//cout<<distanceFromSource[ destination ]<<endl;
	for(int i=0; i < N ; i++) {	
		weights[i] = -1;
		if( ( distanceFromSource[i] + distanceToDestination[i] ) < alpha[1]* distanceFromSource[destination.first] ) {
			dag.push_back( make_pair(-distanceToDestination[ i ], i ) );
		}
	}

	sort( dag.begin(), dag.end() );
	// (Distance, Last node) at dag[i] given a particular score
	vector< map<int, pair<double, long long int> > > scores( dag.size() );
  
  	vector< long long int> nodeToDagIndex(N);
	// cout<<dag.size()<<endl;
	for (int i=0; i<N; i++) {
  		nodeToDagIndex[i] = -1;
  	}
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
		
	long long int startIndex = nodeToDagIndex[source];
  	long long int endIndex = nodeToDagIndex[destination.first];
	/*
  	for(int i = 0; i < dag.size(); i++)
		scores[ i ][ 0 ] = make_pair(distanceFromSource[ dag[i].second ], -1);
  	*/
	scores[ startIndex ][ 0 ] = make_pair(0, -1);
	int countDead = 0;
	for(int i = startIndex; i < dag.size(); i++) {
  		// Maintain the monocity of scores[i]
		double lastDist = 1e10; // infinity		
		vector<int> delScore;
		for(map<int, pair<double, long long int> >::reverse_iterator it=scores[ i ].rbegin(); it != scores[ i ].rend(); it++) { 
			if (it->second.first >= lastDist) {
				delScore.push_back(it->first);
				continue;
			}
			lastDist = it->second.first;		
		}
		//cout<<"FIRST"<< scores[i].size()<<endl;
		for (int j = 0; j < delScore.size(); j++) {
			scores[i].erase(delScore[j]);
		}
		//cout<<scores[i].size()<<endl;
		// We need not explore this DAG node
		if ( !scores[i].size() ) {
			countDead += 1;
			continue;
		}
		long long int u = dag[i].second;
		// cout<<weights[u]<<endl;
		for(int j = 0; j < edges[u].size(); j++) {
			long long int v = edges[u][j];
		  	long long int vIndex = nodeToDagIndex[ v ];
		  	if( nodeToDagIndex[ v ] == -1 )
				continue;
		  	//some nodes are ajacent to themselves in the graph
		  	if(u == v)
				continue;

		  	if( vIndex >= i ) {

		  		// The edge weight is calculated here (lazy calculation)
				if (weights[v] == -1) {
					weights[ v ] = get_expected_trips2(source, destination, v, timeSlot, alpha, dataset, 
						distanceFromSource, distanceToDestination, distanceFromDestination);
				}

		  		for(map<int, pair<double, long long int> >::iterator it=scores[ i ].begin(); it != scores[ i ].end(); it++)
				{ 
					// cout<<it->first<<endl;
					if( it->first > 10000)
						continue;

					int curScore = it->first + weights[v];
					double prevDist = it->second.first;
			  		if( scores[ nodeToDagIndex[v] ].find( curScore ) != scores[ nodeToDagIndex[v] ].end() ) {
					  	if( scores[ nodeToDagIndex[v] ][curScore].first >= prevDist + edgeWeight[u][j] ) {
							scores[ nodeToDagIndex[v] ][curScore] = make_pair(prevDist + edgeWeight[u][j], u);
						}
					}
					else
					{
						scores[ nodeToDagIndex[v] ][curScore] = make_pair(prevDist + edgeWeight[u][j], u);	
					}
				}
			}
		}
	}
	//cout<<"DEAD = "<< countDead<<" "<<dag.size() - startIndex<<endl;
	long long int bestScore = 0;

	for( map<int, pair<double, long long int> >::iterator it=scores[ endIndex ].begin(); it != scores[ endIndex ].end(); it++) {
		//printf(" -- Score = %d (%.4f < %.4f?)\n",it->first, it->second.first, distFromSourceToDestination*alpha);
		if( it->second.first < alpha[1]* distanceFromSource[destination.first]  ) {
			bestScore = it->first;
		}
	}
	vector<long long int> path;
	long long int traceLocation = destination.first;
	int traceScore = bestScore;
	while (traceLocation != -1) {
		//printf(" At %lld[%lld]: Score = %d (dist: %.4f)\n",traceLocation, nodeToDagIndex[traceLocation], traceScore, scores[ nodeToDagIndex[traceLocation] ][traceScore].first);
		path.push_back(traceLocation);
		int prevScore = traceScore - weights[traceLocation];
		traceLocation = scores[ nodeToDagIndex[traceLocation] ][traceScore].second;	
		traceScore = prevScore;
	}
	reverse(path.begin(), path.end());

	//cout<<"BEST SCORE:   "<<bestScore<<endl;
	return make_pair(path, bestScore);

}
/*
DAG extended score algorithm
Given (S,D), construct a DAG based on distance to destination, then perform a dynamic programming to obtain max score path. 
Back edges are allowed as long as the distance is less than the maxDepth parameter.
Cycle is not allowed.
*/

vector< vector< pair<double, vector<long long int> > > > extendEdge;

void extendEdges(long long int source, long long int node, double maxDepth, vector<long long int> &path, vector< pair<double, vector<long long int> > > &paths,
	double pathLen, const vector< vector<long long int> > &edges, const vector< vector<double> > &edgeWeight) {
	
	for( int i = 0; i < path.size(); i++) {
		if( node == path[i] )
			return;
	}
	path.push_back( node );

	if( path.size() > 1 ) {
		paths.push_back( make_pair(pathLen, path) ) ;
	}

	if( pathLen >= maxDepth ) {
		path.pop_back();
		return ;
	}
	
	for(long long int j=0; j < edges[ node ].size(); j++) {
		long long int newNode = edges[ node ][j];
		extendEdges( source, newNode, maxDepth, path, paths, pathLen + edgeWeight[ node ][j], edges, edgeWeight ) ;
	}

	path.pop_back();
}

void assignExtendEdge(int N, double maxDepth, const vector< vector<long long int> > &edges, const vector< vector<double> > &edgeWeight) {
	extendEdge.resize(N); 
	for( int i = 0; i < N; i++) {
		vector<long long int> path;
		vector< pair< double, vector<long long int> > > paths;
		extendEdges(i, i, maxDepth, path, paths, 0, edges, edgeWeight);
		extendEdge[ i ] = paths;
	}
	printf("Extend edge assigned\n"); fflush(stdout);
}


vector<long long int> findDAGExtendedPath(int N, long long int source, long long int destination, long long int timeSlot, double alpha, double maxDepth, vector< map<long long int, vector<long long int> > > &dataset,
	const vector< double > &distanceFromSource, const vector< double > &distanceToDestination, const vector< double > &distanceFromDestination) {

	// This will be calculate on demand
	vector< long long int > weights(N);

	vector< vector< long long int> > extendEdgeWeights(N);
	for( int i = 0; i < N; i++) {
		weights[i] = -1;
		extendEdgeWeights[i].resize(extendEdge[i].size());
	}

	vector< pair< double, int> > dag;
	//printf("Start extended %.2f\n",( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC);

	for(int i=0; i < N ; i++) {	
		if( ( distanceFromSource[i] + distanceToDestination[i] ) <= alpha * distanceFromSource[destination]  ) {
			dag.push_back( make_pair(-distanceToDestination[ i ], i ) );
		}
	}

	sort( dag.begin(), dag.end() );

	// (Distance, (Last Node, Ex Edge used) ) at dag[i] given a particular score
  	vector< map<int, pair<double, pair<int,int> > >  > scores(dag.size());
  	
  	vector< int> nodeToDagIndex(N);
  	for (int i=0; i<N; i++) {
  		nodeToDagIndex[i] = -1;
  	}
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
	
	int startIndex = nodeToDagIndex[ source ];
  	int endIndex = nodeToDagIndex[ destination ];

  	scores[ startIndex ][ 0 ] = make_pair(0, make_pair(-1,0)) ;
  	vector< set<int> > reach(N);
  	vector< set<int> > reachFrom(N);

  	//printf("Start iterate %.2f\n",( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC);
	int countDead = 0;
  	int count_used = 0,count_reached = 0,count_edge = 0,count_relax = 0,count_update = 0,count_ops1 = 0,count_ops2 = 0,count_get_trip = 0;
  	for(int i = startIndex; i < dag.size(); i++) {
		int u = dag[i].second;
		
		// Maintain the monocity of scores[i]
		double lastDist = 1e10; // infinity		
		vector<int> delScore;
		for(map<int, pair<double, pair<int,int> > >::reverse_iterator it=scores[ i ].rbegin(); it != scores[ i ].rend(); it++) { 
			if (it->second.first >= lastDist) {
				delScore.push_back(it->first);
				continue;
			}
			lastDist = it->second.first;
		}
		for (int j = 0; j < delScore.size(); j++) {
			scores[i].erase(delScore[j]);
		}

		// We need not explore this DAG node
		if ( scores[i].empty() ) {
			countDead += 1;
			continue;
		}
		
		for(int j = 0; j < extendEdge[u].size(); j++) {
			
			pair<double, vector<long long int> > v = extendEdge[u][j];
			bool continueLoop = false;
			bool invalidNode = false;
			bool reachAbleError = false;
			int pathSize = v.second.size();

			count_edge++;

			if( pathSize < 1)
				continue;

			int lastNode =  v.second[ pathSize - 1 ];

			if ( lastNode == u )
				continue;

			// All nodes in the extended path except the last node has to be before u in the DAG
			for(int k = 1; k < pathSize ; k++ ) {
				int pathNode = v.second[ k]; 
				
				if( ( nodeToDagIndex[ pathNode ] >= i ) ^  ( k == ( pathSize -1 ) ) ) {
					continueLoop = true;
					break;
				}

				if( nodeToDagIndex[ pathNode ] == -1 ) {
					invalidNode = true;
					break;
				}
			}

			if( continueLoop || invalidNode )
				continue;

			// Avoid cycle while taking reverse path
			for(int k1 = 0; k1 < pathSize ; k1++ ) {	
				for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
					if( reach[ v.second[ k2 ] ].find( v.second[ k1 ] ) != reach[ v.second[ k2 ] ].end() ) {
						reachAbleError = true;
					}
				}
			}

			count_reached++;

		  	if( reachAbleError ) {
				continue;
		  	}

			count_used++;

		  	// Update reach and reachFrom so that we won't have cycles in the path
		  	for(int k1 = 0; k1 < pathSize ; k1++ ) {
				for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
					reach[ v.second[ k1 ] ].insert( v.second[ k2 ] );
					reachFrom[ v.second[ k2] ].insert( v.second[ k1 ] );
				}
			} 

			for(int k1 = pathSize - 1; k1 >= 0 ; k1-- ) {
				/*
				for( set<int>::iterator sit = reachFrom[ v.second[ k1 ] ].begin(); sit != reachFrom[ v.second[ k1 ] ].end(); sit++) {
					reach[ (*sit) ].insert( v.second.begin()+k1+1, v.second.end() );
					count_ops1++;
				}
				for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
					reachFrom[ v.second[ k2] ].insert( reachFrom[ v.second[ k1 ] ].begin(), reachFrom[ v.second[ k1 ] ].end() );
					count_ops2++;
				}
				*/
				for( set<int>::iterator sit = reachFrom[ v.second[ k1 ] ].begin(); sit != reachFrom[ v.second[ k1 ] ].end(); sit++) {
					for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
						// Two checks are needed because it is a directed graph
						if (abs(distanceFromSource[(*sit)] - distanceFromSource[v.second[k2]]) > maxDepth && abs(distanceToDestination[(*sit)] - distanceToDestination[v.second[k2]]) > maxDepth)
							continue;
						reach[ (*sit) ].insert( v.second[k2] );
						reachFrom[ v.second[ k2] ].insert( (*sit) );
					}
				}
				
			}

			// The extended edge weight is calculated here (lazy calculation)
			int vWeight = 0;
			for(int k = 1; k < pathSize ; k++ ) {
				int pathNode = v.second[ k];
				if (weights[pathNode] == -1) {
					count_get_trip++;
					weights[pathNode] = get_expected_trips(source, destination, pathNode, timeSlot, alpha, dataset, distanceFromSource, distanceToDestination, distanceFromDestination);
				}
				vWeight += weights[pathNode];
			}

			int vIndex = nodeToDagIndex[ lastNode ];
		  	extendEdgeWeights[u][j] = vWeight;

	  		for(map<int, pair<double, pair<int,int> > >::iterator it=scores[ i ].begin(); it != scores[ i ].end(); it++) { 

	  			count_relax++;

			  	if( it->first > 10000)
						continue;

				int curScore = it->first + vWeight;
				double prevDist = it->second.first;
				double curDist = v.first;
		  		if( scores[ vIndex ].find( curScore ) != scores[ vIndex ].end() ) {
				  	if( scores[ vIndex][curScore].first >= prevDist + curDist ) {
						scores[ vIndex ][curScore] = make_pair(prevDist + curDist, make_pair(u,j));;
					}
				}
				else
				{
					scores[ vIndex ][curScore] = make_pair(prevDist + curDist, make_pair(u,j));;	
				}
				
			}
		}
		
	}
	//cout<<"  DEAD EX :"<< countDead<<" "<<dag.size()- startIndex <<endl;
	long long int bestScore = 0;

	for( map<int, pair<double, pair<int,int> > >::iterator it=scores[ endIndex ].begin(); it != scores[ endIndex ].end(); it++) {	
		if( it->second.first <= alpha * distanceFromSource[destination] ) {
			bestScore = it->first;
		}
	}

	//printf("Start tracing %.2f\n",( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC);

	vector<long long int> path;
	int traceLocation = destination;
	int traceScore = bestScore;
	while (traceLocation != -1) {
		//if (traceScore > 0) printf(" At %d[%d]: Score = %d (dist: %.4f)\n",traceLocation, nodeToDagIndex[traceLocation], traceScore, scores[ nodeToDagIndex[traceLocation] ][traceScore].first); fflush(stdout);
		pair<int,int> prevInfo = scores[ nodeToDagIndex[traceLocation] ][traceScore].second;
		if (prevInfo.first != -1) {
			//if (extendEdgeWeights[prevInfo.first][prevInfo.second] > 0) {
			//	printf(" At %d[%lld]: Score = %d (this score = %lld)\n",traceLocation, nodeID[traceLocation], traceScore, extendEdgeWeights[prevInfo.first][prevInfo.second]); fflush(stdout);
			//}
			vector<long long int> extendPath = extendEdge[prevInfo.first][prevInfo.second].second;
			for (int i=extendPath.size()-1; i>0; i--) {
				path.push_back(extendPath[i]);
			}
		}
		else {
			path.push_back(traceLocation);
		}

		traceLocation = prevInfo.first;	
		if (traceLocation != -1) {
			traceScore -= extendEdgeWeights[prevInfo.first][prevInfo.second];
		}
	}
	reverse(path.begin(), path.end());
	//printf("Edge = %d\tReached = %d\tUsed = %d\tRelax = %d\tUpdate = %d (%d,%d)\n",count_edge,count_reached,count_used,count_relax,count_update,count_ops1,count_ops2);
	//printf("Path size = %d\n",(int)path.size());

	return path;
}


pair< vector<long long int>, int>  findDAGExtendedPath2(int N, long long int source, pair<long long int, long long int> destination, long long int timeSlot, vector< double > alpha, double maxDepth, vector< map<long long int, vector<long long int> > > &dataset,
	const vector< double > &distanceFromSource, const vector< double > &distanceToDestination, const vector< double > &distanceFromDestination) {

	// This will be calculate on demand
	vector< long long int > weights(N);

	vector< vector< long long int> > extendEdgeWeights(N);
	for( int i = 0; i < N; i++) {
		weights[i] = -1;
		extendEdgeWeights[i].resize(extendEdge[i].size());
	}

	vector< pair< double, int> > dag;
	//printf("Start extended %.2f\n",( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC);

	for(int i=0; i < N ; i++) {	
		if( ( distanceFromSource[i] + distanceToDestination[i] ) < alpha[1] * distanceFromSource[destination.first]  ) {
			dag.push_back( make_pair(-distanceToDestination[ i ], i ) );
		}
	}

	sort( dag.begin(), dag.end() );

	// (Distance, (Last Node, Ex Edge used) ) at dag[i] given a particular score
  	vector< map<int, pair<double, pair<int,int> > >  > scores(dag.size());
  	
  	vector< int> nodeToDagIndex(N);
  	for (int i=0; i<N; i++) {
  		nodeToDagIndex[i] = -1;
  	}
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
	
	int startIndex = nodeToDagIndex[ source ];
  	int endIndex = nodeToDagIndex[ destination.first ];

  	scores[ startIndex ][ 0 ] = make_pair(0, make_pair(-1,0)) ;
  	vector< set<int> > reach(N);
  	vector< set<int> > reachFrom(N);

  	//printf("Start iterate %.2f\n",( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC);
	int countDead = 0;
  	int count_used = 0,count_reached = 0,count_edge = 0,count_relax = 0,count_update = 0,count_ops1 = 0,count_ops2 = 0,count_get_trip = 0;
  	for(int i = startIndex; i < dag.size(); i++) {
		int u = dag[i].second;
		
		// Maintain the monocity of scores[i]
		double lastDist = 1e10; // infinity		
		vector<int> delScore;
		for(map<int, pair<double, pair<int,int> > >::reverse_iterator it=scores[ i ].rbegin(); it != scores[ i ].rend(); it++) { 
			if (it->second.first >= lastDist) {
				delScore.push_back(it->first);
				continue;
			}
			lastDist = it->second.first;
		}
		for (int j = 0; j < delScore.size(); j++) {
			scores[i].erase(delScore[j]);
		}

		// We need not explore this DAG node
		if ( scores[i].empty() ) {
			countDead += 1;
			continue;
		}
		
		for(int j = 0; j < extendEdge[u].size(); j++) {
			
			pair<double, vector<long long int> > v = extendEdge[u][j];
			bool continueLoop = false;
			bool invalidNode = false;
			bool reachAbleError = false;
			int pathSize = v.second.size();

			count_edge++;

			if( pathSize < 1)
				continue;

			int lastNode =  v.second[ pathSize - 1 ];

			if ( lastNode == u )
				continue;

			// All nodes in the extended path except the last node has to be before u in the DAG
			for(int k = 1; k < pathSize ; k++ ) {
				int pathNode = v.second[ k]; 
				
				if( ( nodeToDagIndex[ pathNode ] >= i ) ^  ( k == ( pathSize -1 ) ) ) {
					continueLoop = true;
					break;
				}

				if( nodeToDagIndex[ pathNode ] == -1 ) {
					invalidNode = true;
					break;
				}
			}

			if( continueLoop || invalidNode )
				continue;

			// Avoid cycle while taking reverse path
			for(int k1 = 0; k1 < pathSize ; k1++ ) {	
				for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
					if( reach[ v.second[ k2 ] ].find( v.second[ k1 ] ) != reach[ v.second[ k2 ] ].end() ) {
						reachAbleError = true;
					}
				}
			}

			count_reached++;

		  	if( reachAbleError ) {
				continue;
		  	}

			count_used++;

		  	// Update reach and reachFrom so that we won't have cycles in the path
		  	for(int k1 = 0; k1 < pathSize ; k1++ ) {
				for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
					reach[ v.second[ k1 ] ].insert( v.second[ k2 ] );
					reachFrom[ v.second[ k2] ].insert( v.second[ k1 ] );
				}
			} 

			for(int k1 = pathSize - 1; k1 >= 0 ; k1-- ) {
				/*
				for( set<int>::iterator sit = reachFrom[ v.second[ k1 ] ].begin(); sit != reachFrom[ v.second[ k1 ] ].end(); sit++) {
					reach[ (*sit) ].insert( v.second.begin()+k1+1, v.second.end() );
					count_ops1++;
				}
				for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
					reachFrom[ v.second[ k2] ].insert( reachFrom[ v.second[ k1 ] ].begin(), reachFrom[ v.second[ k1 ] ].end() );
					count_ops2++;
				}
				*/
				for( set<int>::iterator sit = reachFrom[ v.second[ k1 ] ].begin(); sit != reachFrom[ v.second[ k1 ] ].end(); sit++) {
					for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
						// Two checks are needed because it is a directed graph
						if (abs(distanceFromSource[(*sit)] - distanceFromSource[v.second[k2]]) > maxDepth && abs(distanceToDestination[(*sit)] - distanceToDestination[v.second[k2]]) > maxDepth)
							continue;
						reach[ (*sit) ].insert( v.second[k2] );
						reachFrom[ v.second[ k2] ].insert( (*sit) );
					}
				}
				
			}

			// The extended edge weight is calculated here (lazy calculation)
			int vWeight = 0;
			for(int k = 1; k < pathSize ; k++ ) {
				int pathNode = v.second[ k];
				if (weights[pathNode] == -1) {
					count_get_trip++;
					weights[pathNode] = get_expected_trips2(source, destination, pathNode, timeSlot, alpha, dataset, distanceFromSource, distanceToDestination, distanceFromDestination);
				}
				vWeight += weights[pathNode];
			}

			int vIndex = nodeToDagIndex[ lastNode ];
		  	extendEdgeWeights[u][j] = vWeight;

	  		for(map<int, pair<double, pair<int,int> > >::iterator it=scores[ i ].begin(); it != scores[ i ].end(); it++) { 

	  			count_relax++;

			  	if( it->first > 10000)
						continue;

				int curScore = it->first + vWeight;
				double prevDist = it->second.first;
				double curDist = v.first;
		  		if( scores[ vIndex ].find( curScore ) != scores[ vIndex ].end() ) {
				  	if( scores[ vIndex][curScore].first >= prevDist + curDist ) {
						scores[ vIndex ][curScore] = make_pair(prevDist + curDist, make_pair(u,j));;
					}
				}
				else
				{
					scores[ vIndex ][curScore] = make_pair(prevDist + curDist, make_pair(u,j));;	
				}
				
			}
		}
		
	}
	//cout<<"  DEAD EX :"<< countDead<<" "<<dag.size()- startIndex <<endl;
	long long int bestScore = 0;

	for( map<int, pair<double, pair<int,int> > >::iterator it=scores[ endIndex ].begin(); it != scores[ endIndex ].end(); it++) {	
		if( it->second.first < alpha[1]* distanceFromSource[destination.first] ) {
			bestScore = it->first;
		}
	}

	//printf("Start tracing %.2f\n",( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC);

	vector<long long int> path;
	int traceLocation = destination.first;
	int traceScore = bestScore;
	while (traceLocation != -1) {
		//if (traceScore > 0) printf(" At %d[%d]: Score = %d (dist: %.4f)\n",traceLocation, nodeToDagIndex[traceLocation], traceScore, scores[ nodeToDagIndex[traceLocation] ][traceScore].first); fflush(stdout);
		pair<int,int> prevInfo = scores[ nodeToDagIndex[traceLocation] ][traceScore].second;
		if (prevInfo.first != -1) {
			//if (extendEdgeWeights[prevInfo.first][prevInfo.second] > 0) {
			//	printf(" At %d[%lld]: Score = %d (this score = %lld)\n",traceLocation, nodeID[traceLocation], traceScore, extendEdgeWeights[prevInfo.first][prevInfo.second]); fflush(stdout);
			//}
			vector<long long int> extendPath = extendEdge[prevInfo.first][prevInfo.second].second;
			for (int i=extendPath.size()-1; i>0; i--) {
				path.push_back(extendPath[i]);
			}
		}
		else {
			path.push_back(traceLocation);
		}

		traceLocation = prevInfo.first;	
		if (traceLocation != -1) {
			traceScore -= extendEdgeWeights[prevInfo.first][prevInfo.second];
		}
	}
	reverse(path.begin(), path.end());
	//printf("Edge = %d\tReached = %d\tUsed = %d\tRelax = %d\tUpdate = %d (%d,%d)\n",count_edge,count_reached,count_used,count_relax,count_update,count_ops1,count_ops2);
	//printf("Path size = %d\n",(int)path.size());

	return make_pair(path, bestScore);
}