#include "ioD.h"
#include <vector>
#include <iostream>
using namespace std;

long long n;

int queryCnt;

char * txtName, * degName, * outName, *inName;

edgeL * deg;
edgeS * labelout, *labelin;
edgeS * labelx, * labely;

bool fc = 0;

int query(int x, int y)
{
	if (x == y) return 0;
	int xx = x, yy = y;

	x = ((deg[xx].x<<32)>>32);
	y = ((deg[yy].x<<32)>>32);
	
/*	if (fc)
	{
	printf("%d %d\n", x, y);	printf("%lld %lld %lld %lld\n", deg[xx].y, deg[xx].w, deg[yy].y, deg[yy].w);
	}
*/	
	if (x > y)
	{
		labelx = labelout + deg[xx].w;
		labely = labelin + deg[yy].y;
	}
	else
	{
		int xy = x; x = y; y = xy;
		labelx = labelin + deg[yy].y;
		labely = labelout + deg[xx].w;
	}
	
/*	if (fc)
	{
	printf("%d %d\n", x, y);		for (int i = 0; i < 500 && labelx[i].x > -1; i++)		printf("%d %d %d\n", x, labelx[i].x, labelx[i].w);for (int i = 0; i < 500 && labely[i].x > -1; i++)		printf("%d %d %d\n", y, labely[i].x, labely[i].w);
	}
*/	
	int ans = 1000000, i = 0, j = 0;
	
/*	
	while (labelx[i].x != -1 || labely[j].x != -1)
	{
		if (labelx[i].x == y) ans = ans>labelx[i].w?labelx[i].w:ans;
		if (labely[j].x == x) ans = ans>labely[j].w?labely[j].w:ans;

		if (labelx[i].x == labely[j].x) 
		{
			ans = ans>(labelx[i].w + labely[j].w)?(labelx[i].w + labely[j].w):ans;
			i++;
			j++;
		}
		else if (labelx[i].x != -1 && (labely[j].x == -1 || labelx[i].x < labely[j].x) ) i++;
		else j++;
	}
	return ans;
*/
	
	if (labelx[i].x != -1 && labely[j].x != -1)
	while (labelx[i].x < y)
	{
//		printf("---%d %d %d %d\n", labelx[i].x, labelx[i].w, labely[j].x, labely[j].w);
		if (labelx[i].x == labely[j].x) 
		{
			ans = ans>(labelx[i].w + labely[j].w)?(labelx[i].w + labely[j].w):ans;
			if (labelx[++i].x == -1) break;
			if (labely[++j].x == -1) break;
		}
		else if (labelx[i].x < labely[j].x)
		{
			if (labelx[++i].x == -1) break;
		}
		else if (labely[++j].x == -1) break;
	}
	
	while (labelx[i].x != -1 && labelx[i].x < y) i++;
	if (labelx[i].x == y) ans = ans>labelx[i].w?labelx[i].w:ans;

	
	return ans;
}


void loadIndex()
{
	inBufL degBuf(degName);
	inBufS inLabel(inName), outLabel(outName);
	
	n = checkB(degName)/sizeof(edgeL);

	deg = (edgeL *)malloc(sizeof(edgeL)*n);
	labelin = (edgeS*)malloc(checkB(inName));
	labelout = (edgeS*)malloc(checkB(outName));

	printf("%lld vertices\n", n);

	degBuf.start();
	for (int i = 0; i < n; i++)
		degBuf.nextEdge(deg[i]);

	inLabel.start();
	for (int i = 0; !inLabel.isEnd; i++)
		inLabel.nextEdge(labelin[i]);
	
	outLabel.start();
	for (int i = 0; !outLabel.isEnd; i++)
		outLabel.nextEdge(labelout[i]);
			
}


int main(int argc, char ** argv)
{
	txtName = argv[1];
	degName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(degName, "%s.deg", txtName);
	
	inName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(inName, "%s.labelin", txtName);
	outName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(outName, "%s.labelout", txtName);

	timer tm;
	
	loadIndex();
	
	printf("load time %lf (ms)\n", tm.getTime()*1000);
	

	double sumTime = 0;
	
		
	queryCnt = 1000;
	if (argc > 2) queryCnt = atoi(argv[2]);


	
	srand(time(NULL));
	int cnt = 0;
	for (int i = 0; i < queryCnt; i++)
	{
	 	int x = rand()%n, y = rand()%n;

	 	tm.restart();
	 	int ans = query(x, y);
		cout<<x<<" "<<y<<" "<<ans<<endl;
		if( ans == 1000000)
			cnt += 1;
	 	sumTime += tm.getTime();
	 }
	for(int i=0;i<58000;i++)
		if( query(0, i) + query(i, 15) == query(0, 15) )
			cout<<i<<endl;
	printf("average query time %lf (ms)\n", sumTime*1000/queryCnt);
	cout<<cnt<<endl;	
	free(degName);
	free(inName);
	free(outName);
	
	free(deg);
	free(labelin);
	free(labelout);
	
	return 0;
}












