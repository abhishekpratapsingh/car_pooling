#include <bits/stdc++.h>
#include <ctime>

using namespace std;

int DEBUG = 1;
float alpha  = 1.3;
double maxDepth = 0.2;
long long int beta = 0;
long long int maxDistance = 100000;
/* nodes global structure */
vector< pair<double, double> > nodes;
vector<long long int> nodeID;
map<long long int, long long int> idToNode;
map< long long int, pair<double, double> > nodeToLatLon;

/* Edges global structure */
map< long long int, vector<long long int> > edges;
map< long long int, vector<double> > edgeWeight;
map< long long int, vector<long long int> > edgesReverse;
map< long long int, vector<double> > edgeWeightReverse;

map< long long int, map<long long int, vector<long long int> > > sourceTimeDestination; 

double minPathScore[1000];

double dagScore[1000];
double dagTimeTaken[1000];

double dagExScore[1000];
double dagExTime[1000];

long long int tripNumber = 0;
clock_t startTimeTrip;
clock_t endTimeTrip;

void getTimeSourceDestination() {
	ifstream file;
	file.open( "outpu" );
	string s;
	while( getline(file, s) ) {
		stringstream ss( s );
		long long int source, timeSlot, dest;
		ss>>source>>timeSlot;
		while( ss>>dest ) {
			sourceTimeDestination[ source ][ timeSlot ].push_back( dest );
		}
	}
}


void takeGraphInput() {
	ifstream Location;
	Location.open("beijing_adj_fromhistory_new.txt");
	string s;
	int index = 0, numNodes = 0, numEdge = 0;
	
	getline(Location, s);
	stringstream ss(s);
	char ch;
	ss>>numNodes>>ch>>numEdge;

	while( getline(Location, s) ) {
		ss.str( std::string() );
		ss.clear();
		ss<<s;
		int id; double lon; double lat; 
		ss>>id>>ch>>lat>>ch>>lon; 
		nodes.push_back( make_pair( lon, lat) );
		nodeID.push_back( id );
		idToNode[ id ] = index;
		nodeToLatLon[ id ] = make_pair( lat, lon);
		index++;
		if(nodes.size() == numNodes)
			break;
	}

	while( getline(Location, s) ) {
		ss.str( std::string() );
		ss.clear();
		ss<<s;
		long long int numRandom; long long int node1; long long int node2; double weight; char ch; int oneWay;
		ss>>numRandom>>ch>>node1>>ch>>node2>>ch>>weight>>ch>>oneWay;
		edges[ node1 ].push_back( node2 );
		edgeWeight[ node1 ].push_back( weight );
		edgesReverse[ node2 ].push_back( node1 );
		edgeWeightReverse[ node2 ].push_back( weight );

		if( !oneWay ) {
			int temp = node1; node1 = node2; node2 = temp;
			edges[ node1 ].push_back( node2 );
			edgeWeight[ node1 ].push_back( weight );

			edgesReverse[ node2 ].push_back( node1 );
			edgeWeightReverse[ node2 ].push_back( weight );
		}

	}

	Location.close();
	return ;
}

vector<long long int>  dijkstra_lengths(long long int S, long long int D, map<long long int, double > &distanceFromSource,
 map<long long int, vector<long long int> > &edges, map<long long int, vector<double> > &edgeWeight ) { 
	
   	map<long long int, long long int> path;
	for(int i=0; i< nodeID.size(); i++)
	{ 	distanceFromSource[ nodeID[i] ] = 100000;
		path[ nodeID[i] ] = -1;
	}

	distanceFromSource[ S ] = 0;
	priority_queue< pair<float, long long int> > dj;
	dj.push( make_pair(0, S) );

	while(dj.size()) { 
		pair<float, long long int> x = dj.top();
		dj.pop();

		long long int u = x.second;
		for(int i=0; i< edges[ u ].size(); i++) { 	
			long long int v = edges[ u ][ i ];
			float alt = distanceFromSource[ u ] + edgeWeight[ u ][ i ];
			if(alt < distanceFromSource[ v ])
			{ 	distanceFromSource[ v ] = alt;
				dj.push( make_pair(-alt, v) );
				path[v] = u;
			}
		}
	}

	vector<long long int> path1;
	long long int x = D;
	while(1) {
		path1.push_back(x);
		if( ( x == S ) || ( x == -1 ) || ( path.find(x) == path.end() ) )
		  	break;
		x = path[ x ];
	}
	
	path1.push_back(S);

	return path1;
}

double getDistanceFromTo(long long int S, long long int D) { 
	map<long long int, float> dist;
 
	for(int i=0; i< nodeID.size() ; i++)
	{ 	dist[ nodeID[i] ] = 100000;
	}

	dist[S] = 0;
	priority_queue< pair<float, long long int> > dj;
	dj.push( make_pair(0, S) );

	while(dj.size())
	{ 
		pair<float, long long int> x = dj.top();
		dj.pop();
		
		long long int u = x.second;
		
		if( u == D)
			break;
		
		for(int i=0; i< edges[u].size(); i++)
		{ 	long long int v = edges[u][i];
			float alt = dist[u] + edgeWeight[u][i];
			if(alt < dist[v])
			{ 	dist[v] = alt; 
				dj.push(make_pair(-alt, v));
			}
		}
	}

	return dist[ D ];
}

int HD(long long int S, long long int D, map<long long int, long long int > &weights) { 
	map<long long int, float> dist;
 
	for(int i=0; i< nodeID.size() ; i++)
	{ 	dist[ nodeID[i] ] = 100000;
	}

	dist[S] = 0;

	// ( (score, node), (distance, path) )
	priority_queue< pair< pair<float, long long int>, pair< float, vector<int> > > > dj;
	
	vector<int> path;
	path.push_back(S);
	dj.push( make_pair( make_pair(0, S), make_pair(0, path) ) );

	while(dj.size())
	{ 
		pair< pair<float, long long int>, pair< float, vector<int> >  > x = dj.top();
		dj.pop();
		
		long long int u = x.first.second;
		
		if( u == D ) {
			int score = 0;
			for(int i=1;i<x.second.second.size(); i++) {
				score += weights[ x.second.second[i] ];
			}
			return score;
		}
		
		for(int i=0; i< edges[u].size(); i++)
		{ 	long long int v = edges[u][i];
			for(int j=0; j<x.second.second.size(); j++) 
			if( x.second.second[j] == v)
				continue;
			float alt = -x.first.first + (edgeWeight[u][i]/(weights[v]+1) );
			
			if( (x.second.first + edgeWeight[u][i]) < dist[v] )
			{ 	dist[v] = (x.second.first + edgeWeight[u][i]); 
			    path = x.second.second;
			    path.push_back(v);
				dj.push( make_pair( make_pair(-alt, v), make_pair(dist[v], path) ) );
			}
		}
	}

	// not reachable
	return -1; 
}

void get_all_trips(long long int source,long long int start_time, vector<long long int> &tripList) {

	if( sourceTimeDestination.find(source) != sourceTimeDestination.end() ) {
		if( sourceTimeDestination[source].find(start_time) 
					!= sourceTimeDestination[source].end() ) {
			
			vector<long long int> addMe = sourceTimeDestination[source][ start_time ] ;
			
			for(int i = 0; i < addMe.size(); i++) {
				tripList.push_back( addMe[i] );
			}

		}
	}
}

long long int get_weight(long long int source, long long int destination, long long int v, long long int start_time,
 	map<long long int, double> &distanceToDestination, map<long long int, double> &distanceFromDestination)
{  	
	vector<long long int> tripList;
	
	get_all_trips(v, start_time/15, tripList);
  	return tripList.size();
  	long long int score = 0;
  	for(int i = 0; i < tripList.size(); i++) {
  		long long int w = tripList[ i ]; 
		double tvw = getDistanceFromTo(v, w);
        double tvd = distanceToDestination[ v ];
      	double tdw = distanceFromDestination[ w ];
      	if( (tvd + tdw) < ( (1+alpha)*tvw) ) {
      		score += 1;
      	} 
  	}
  	return score;
}

long long int dagScore1(long long int source, long long int destination, map<long long int, double > &distanceFromSource,
	map<long long int, double > &distanceToDestination, map<long long int, long long int > &weights) {
	vector< pair< double, long long int> > dag;
	long long int sourceIndex = idToNode[ source ];
	long long int destinationIndex = idToNode[ destination ];
	// cout<<distanceFromSource[ destination ]<<endl;
	for(int i=0; i <  nodes.size() ; i++) {	
		if( ( distanceFromSource[ nodeID[ i ] ] + distanceToDestination[ nodeID[ i ] ] )
			 < alpha*distanceFromSource[ destination ] )
			  {
			dag.push_back( make_pair(-distanceToDestination[ nodeID[i] ], nodeID[ i ]) );
		}
	} 

	sort( dag.begin(), dag.end() );
	vector< map<int, double> > scores( dag.size() );
  	vector< map<int, pair<long long int, double> > > path( dag.size() );

  	map<long long int, long long int> nodeToDagIndex;
	// cout<<dag.size()<<endl;
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
		
	long long int startIndex = nodeToDagIndex[source];
  	long long int endIndex = nodeToDagIndex[destination];

  	scores[ startIndex ][ 0 ] = 0;
  	for(int i = startIndex; i < dag.size(); i++) {
    	long long int u = dag[i].second;
    	// cout<<weights[u]<<endl;
	    for(int j = 0; j < edges[u].size(); j++) {
	    	long long int v = edges[u][j];
	      	long long int vIndex = nodeToDagIndex[v];
	      	if( nodeToDagIndex.find( v ) == nodeToDagIndex.end()  )
	        	continue;
	      	//some nodes are ajacent to themselves in the graph
	      	if(u == v)
	        	continue;

	      	if( vIndex >= i ) {
	      		for(map<int, double>::iterator it=scores[ i ].begin(); it != scores[ i ].end(); it++)
	        	{ 
	        		// cout<<it->first<<endl;
	        		if( it->first > 1000)
	        			continue;

	          		if( scores[ nodeToDagIndex[v] ].find( it->first + weights[v] ) != scores[ nodeToDagIndex[v] ].end() ) {
			          	if( scores[ nodeToDagIndex[v] ][it->first + weights[v] ] >= it->second + edgeWeight[u][j] ) {
			            	scores[ nodeToDagIndex[v] ][it->first + weights[v] ] = it->second + edgeWeight[u][j];
			            	path[ nodeToDagIndex[v] ][it->first + weights[v] ] = make_pair(u, it->first);
			            }
			        }
			        else
			        {
			        	scores[ nodeToDagIndex[v] ][it->first + weights[v] ] = it->second + edgeWeight[u][j];
			        	path[ nodeToDagIndex[v] ][it->first + weights[v] ] = make_pair(u, it->first);	
			        }
				}
			}
		}
	}
	long long int bestScore = 0;

	for( map<int, double>::iterator it=scores[ nodeToDagIndex[13915] ].begin(); it != scores[ nodeToDagIndex[13915] ].end(); it++) {
		
		if( it->second < distanceToDestination[ source ]*alpha ) {
			bestScore = it->first;
		}
	}
	cout<<startIndex<<" "<<nodeToDagIndex[ 13915] <<" "<<endIndex<<endl;
	cout<<bestScore<<" BEST SCORE"<<endl;

	pair<int, int> lastNode = make_pair(destination, bestScore);
	while( 1 ) {
		if( lastNode.first == source ) 
			break;
		lastNode = path[ nodeToDagIndex[ lastNode.first ] ][ lastNode.second ]; 
		cout<<lastNode.first<<" "<<lastNode.second<<" "<<weights[ lastNode.first ]<<endl;
	}

	cout<<"BEST SCORE"<<bestScore<<endl;
	return bestScore;

}


void extendEdges(long long int source, long long int node, vector<long long int> &path, vector< pair<double, vector<long long int> > >  &paths,
	double pathLen) {
	
	for( int i = 0; i < path.size(); i++) {
		if( node == path[i] )
			return;
	}
	path.push_back( node );

	if( path.size() > 1 ) {
		paths.push_back( make_pair(pathLen, path) ) ;
	}

	if( pathLen >= maxDepth ) {
		path.pop_back();
		return ;
	}
	
	for(long long int j=0; j < edges[ node ].size(); j++) {
		long long int newNode = edges[ node ][j];
		extendEdges( source, newNode, path, paths, pathLen + edgeWeight[ node ][j] ) ;
	}

	path.pop_back();
}

int FLAGDEBUG = 0;
long long int dagExtendedEdges(long long int source, long long int destination, map<long long int, double > &distanceFromSource, map<long long int, double > &distanceToDestination,
  map<long long int, vector< pair<double, vector<long long int> > > > &extendEdge, map<long long int, vector< long long int > > &extendEdgeWeights, map<long long int, long long int > &weights) {

	vector< pair< double, long long int> > dag;
	long long int sourceIndex = idToNode[ source ];
	long long int destinationIndex = idToNode[ destination ];
	for(int i=0; i <  nodes.size() ; i++) {	
		if( ( distanceFromSource[ nodeID[ i ] ] + distanceToDestination[ nodeID[ i ] ] )
			 < alpha*distanceFromSource[ destination ] )
			  {
			dag.push_back( make_pair(-distanceToDestination[ nodeID[i] ], nodeID[ i ]) );
		}
	} 

	sort( dag.begin(), dag.end() );

	if( FLAGDEBUG ) {
		for(int i=0;i<dag.size();i++ ) {
			cout<< -dag[i].first << " " <<dag[i].second<<" EDGES TOO\n";
			for(int j=0; j< edges[ dag[i].second ].size(); j++ ) {
				cout<< edges[ dag[i].second ][j] << " " << weights[ edges[ dag[i].second ][j] ]<<endl;
			}
		}
		FLAGDEBUG = 0;
	}

	vector<long long int> sc(1000, maxDistance);
  	vector< vector<long long int> > scores(dag.size(), sc);
  	vector< map<int, pair<long long int, double> > > path( dag.size() );
  	
  	map<long long int, long long int> nodeToDagIndex;
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
	
	long long int startIndex = nodeToDagIndex[ source ];
  	long long int endIndex = nodeToDagIndex[ destination ];

  	scores[ startIndex ][ 0 ] = 0;
  	map<long long int, map<long long int, long long int> > reach;
  	map<long long int, map<long long int, long long int> > reachFrom;

  	int counter = 0;
  	for(int i = startIndex; i < dag.size(); i++) {
    	long long int u = dag[i].second;
	    
	    for(int j = 0; j < extendEdge[u].size(); j++) {
	    	
	    	pair<double, vector<long long int> > v = extendEdge[u][j];
	    	bool continueLoop = false;
	    	bool reachAbleError = false;
	    	long long int pathSize = v.second.size();

	    	if( pathSize < 1)
	    		continue;

	    	for(int k1 = 0; k1 < pathSize && !reachAbleError ; k1++ ) {	
    			for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
    				if( reach[ v.second[ k2 ] ][ v.second[ k1 ] ]  == 1) {
    					reachAbleError = true;
    					break;
    				}
    			}
        	} 

	    	for(int k = 1; k < pathSize ; k++ ) {
	    		long long int pathNode = v.second[ k]; 
	    		
	    		if( ( nodeToDagIndex[ pathNode ] >= i ) ^  ( k == ( pathSize -1 ) ) ) {
	    			continueLoop = true;
	    			break;
	    		}
	    		if( reach[ pathNode ][u] || ( nodeToDagIndex.find( pathNode ) == nodeToDagIndex.end() ) ) {
	    			reachAbleError = true;
	    			break;
	    		}
	    	}

	    	long long int lastNode =  v.second[ pathSize - 1 ];

	      	if( continueLoop || reachAbleError || ( lastNode == u ) )
	    		continue;

	    	counter++;
	       	long long int vIndex = nodeToDagIndex[ lastNode ];
	      	long long int vWeight = extendEdgeWeights[u][j];

	      	long long int updated = false;

        	for( int k =0; k + vWeight < 1000; k++) { 
          		if( scores[i][k] == maxDistance ) 
          			continue;

	          	if( scores[ vIndex ][k + vWeight ] >= scores[ i ][ k ] + v.first ) {
	  	          	scores[ vIndex ][k + vWeight ] = scores[ i ][ k ] + v.first;
	  	          	path[ vIndex ][k + vWeight ] = make_pair(u, k);
	   
	            	if( updated )
	            		continue;
	            	else
	            		updated = 1;

	            	for(int k1 = 0; k1 < pathSize ; k1++ ) {	
            			for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
	            			reach[ v.second[ k1 ] ][ v.second[ k2 ] ] 	 = 1;
	            			reachFrom[ v.second[ k2] ][ v.second[ k1 ] ] = 1;
            			}
	            	} 

	            	for(long long int k1 = pathSize - 1; k1 >= 0 ; k1-- ) {	
	            		for( map<long long int, long long int>::iterator it = reachFrom[ v.second[ k1 ] ].begin(); it != reachFrom[ v.second[ k1 ] ].end(); it++) {
	            			for(long long int k2 = k1 + 1; k2 < pathSize; k2++ ) {
		            			reach[ it->first ][ v.second[ k2 ] ] 	= 1;
		            			reachFrom[ v.second[ k2] ][ it->first ] = 1;
	            			}
	            		}
	            	}
	            	
	            }
			}
		}
	}

	long long int bestScore = 0;

	for( int k = 999; k >= 0; k--) {
	
		if(scores[ endIndex ][ k ] == -1)
			continue;

		if( scores[ endIndex ][ k ] < distanceToDestination[ source ]*alpha ) {
			bestScore = k - weights[ destination ];
			break;
		}
	}
	pair<int, int> lastNode = make_pair(destination, bestScore);
	while( 1 ) {
		cout<<lastNode.first<<" "<<lastNode.second<<" "<<weights[ lastNode.first ]<<endl;
		if( lastNode.first == source ) 
			break;
		lastNode = path[ nodeToDagIndex[ lastNode.first ] ][ lastNode.second ]; 
		
	}
	return bestScore;

}

long long int calculateScore( vector< long long int > &path, map<long long int, long long int> &weights) {
	long long int ret = 0;
	for(int i = 1; i < path.size(); i++) {
		ret += weights[ path[i] ];
	}
	return ret;
}

void potentialScore(long long int source, long long int destination, long long int timeSlot, 
	map<long long int, vector< pair<double, vector<long long int> > > > &extendEdge)
{
	map< long long int, double > distanceFromSource;
	map< long long int, double > distanceFromDestination;
	map< long long int, double > distanceToDestination;

	vector<long long int> path = dijkstra_lengths(source, destination, distanceFromSource, edges, edgeWeight);
	dijkstra_lengths(destination, source, distanceFromDestination, edges, edgeWeight);
	
	if( DEBUG )
		cout<<"dijkstra_lengths"<<endl;
	
	if(distanceFromSource[destination] == maxDistance || !distanceFromSource[ destination ] ) {
		tripNumber--;
		return;
	}
	
	dijkstra_lengths(destination, source, distanceToDestination, edgesReverse,
	edgeWeightReverse);

	map<long long int, long long int> weights; 
			
	if( DEBUG ) 
		cout<<distanceFromSource[ destination ]<<endl;
	dagTimeTaken[ tripNumber ] = 0;
	for( int i =0; i<nodeID.size(); i++) {
		dagTimeTaken[ tripNumber ] += 2*(0.0000006);
		if( ( distanceFromSource[ nodeID[i] ] + distanceToDestination[ nodeID[i] ] ) 
				< alpha	*distanceFromSource[ destination ] ) {
			long long int passengersNumber;
			weights[ nodeID[i] ] = get_weight(source, destination, nodeID[i], timeSlot,
				distanceToDestination, distanceFromDestination);
			dagTimeTaken[ tripNumber ] += weights[ nodeID[i] ]*(0.0000006)*6;
		}
	}

	dagExTime[ tripNumber ] = dagTimeTaken[ tripNumber ];

	startTimeTrip = clock();
	
	/* Score along min-path*/
	
	/*long long int scoreMinPath = 0;
	for( int i = 1; i < path.size() - 1; i++) {
		scoreMinPath += weights[ path[i] ];
	}
	*/
	// cout<<extendEdge[13752].size()<<endl;
	// for(int i=0;i< extendEdge[ 13752 ].size() ; i++) {
	// 	cout<<" SIZE "<<extendEdge[13752][i].second.size()<<endl;
	// 	for( int j= 0 ; j< extendEdge[13752][i].second.size(); j++) {
	// 		cout<< extendEdge[ 13752 ][i].second[j]<<" "<<weights[ extendEdge[ 13752][i].second[j] ] <<endl;
	// 	}
	// }
	// exit(1);
	
	int scoreMinPath = HD( source, destination, weights);
	minPathScore[ tripNumber ] = float( scoreMinPath  + 1);
	/* calculating DAG parameters */
	startTimeTrip = clock();
	map<long long int, long long int> path1;
	long long int scoreDag = dagScore1(source, destination, distanceFromSource,
	distanceToDestination, weights);

	cout<<( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC<<endl;
	dagTimeTaken[ tripNumber ] +=  ( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC;
	cout<<scoreMinPath<<" SCORES "<<scoreDag<<endl;
	dagScore[ tripNumber ] = float( scoreDag + 1)  / ( minPathScore[ tripNumber ] ) ;

	/* calculating backDAG	 parameters */
	startTimeTrip = clock();
	
	map<long long int, vector< long long int> > extendEdgeWeights;
	for( int i = 0; i < nodeID.size(); i++) {		
		long long int node = nodeID[i];
		for(int j =0; j < extendEdge[node].size(); j++) {
			long long int scoreAlongPath = calculateScore( extendEdge[node][j].second, weights);
			extendEdgeWeights[ nodeID[i] ].push_back( scoreAlongPath );
		}
	}

	scoreDag = 	dagExtendedEdges( source, destination, distanceFromSource, distanceToDestination, extendEdge, extendEdgeWeights, weights);
	dagExScore[ tripNumber ] = float( scoreDag + 1 ) / ( minPathScore[tripNumber] ) ;
	dagExTime[ tripNumber ] += ( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC;
	if( float( scoreDag + 1)/(dagScore[ tripNumber ]*minPathScore[ tripNumber] ) > 2) {
		cout<<"WE SHOULD LOOK INTO THIS \n";
		FLAGDEBUG = 1;
		dagExtendedEdges( source, destination, distanceFromSource, distanceToDestination, extendEdge, extendEdgeWeights, weights);
	}
	cout<<	"SCORE ALONG EX "<<scoreDag<<"  RATIO WITH DAG "<< float( scoreDag + 1)/(dagScore[ tripNumber ]*minPathScore[ tripNumber] )<<endl;

	exit(1);
	return;
}

/* edges global structure */
int main(int argc, char const *argv																						[])
{
	memset(dagScore, 0, sizeof(dagScore));
	memset(minPathScore, 0, sizeof(minPathScore));
	memset(dagTimeTaken,0,sizeof(dagTimeTaken));
	string s = argv[1];
	stringstream ss(s);	
	stringstream ssDebug(argv[2]);
	ss>>maxDepth;
	ssDebug>>alpha;
	getTimeSourceDestination();
	takeGraphInput();
	map<long long int, vector< pair<double, vector<long long int> > > > extendEdge; 
	for( int i = 0; i < nodeID.size(); i++) {
		long long int node = nodeID[i];
		vector<long long int> path;
		vector< pair< double, vector<long long int> > > paths;
		extendEdges(node, node, path, paths, 0);
		extendEdge[ node ] = paths;
	}

	tripNumber = 0;
	for( int i=0;tripNumber < 20; i += 1) {
		
		long long int source, timeS, destination;
		source = nodeID[i];

		if( sourceTimeDestination.find( nodeID[i] ) == sourceTimeDestination.end() )
			continue;

		for(map<long long int, vector<long long int> >::iterator it = sourceTimeDestination[ nodeID[i] ].begin(); 
			it != sourceTimeDestination[ nodeID[i] ].end() ; it++ ) {
			timeS =it->first;
			destination = it->second[ 0 ];
			if( sourceTimeDestination.find( destination ) == sourceTimeDestination.end() )
				continue;
			startTimeTrip = clock();
			cout<<tripNumber<<endl;

			if(tripNumber >= 20)
				break;
			
			potentialScore(source, destination, timeS, extendEdge);
			tripNumber++;
		}

	}

	double avgMinPath = 0, avgDAGScore = 0, avgDAGtime = 0 ; 
	double avgExScore = 0, avgExTime = 0;
	int hist[101];
	
	memset(hist, 0, sizeof(hist));

	std::vector<double> v1;
	std::vector<double> v2;

	for(int i = 0; i < 20; i++)
	{
		v1.push_back(dagScore[i]);
		v2.push_back(dagExScore[i]);
	} 
	sort(v1.begin(), v1.end());
	sort(v2.begin(), v2.end());
	for(int i = 0; i < 20; i++)
	{ 
		cout<<v1[i]<<endl;
	}
	for(int i = 0; i < 20; i++)
	{ 
		cout<<v2[i]<<endl;
	}
	cout<<"\n\nNEXT VERBOSE::\n\n";
	for(int i = 0; i < 20; i++)
	{
		avgMinPath += minPathScore[i];
		avgDAGScore += dagScore[i];
		avgDAGtime += dagTimeTaken[i];
		avgExScore += dagExScore[i];
		avgExTime += dagExTime[i];
		hist[ int(dagExScore[i]) ] += 1;
	} 

	cout<<avgMinPath/20<<" "<<avgDAGScore/20<<" "<<avgDAGtime/20<<endl;
	cout<<avgExScore/20<<" "<<avgExTime/20<<endl;
	for(int j = 0; j <= 100; j++)
	{
		cout<<hist[j]<<" ";
	}
	return 0;
}

