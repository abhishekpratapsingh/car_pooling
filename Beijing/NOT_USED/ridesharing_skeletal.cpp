#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <fstream>
#include <sstream>
#include <queue>

using namespace std;

vector <map <int,float> > shortest_lengths;
vector <vector <int> > edges;
vector < map<int,float> > edge_weight;
vector<int> nodes;
int index_to_node[70000];
int node_to_index[300000];
float weights[300000];
map <int,float> sld;
float alpha = 0.3;
int beta = 30;
int num_nodes,num_edges;

map< int,map<int, vector<int> > > sourceTimeDestination;


vector<int> get_all_trips(int source, int start_time, int window)
{
	vector<int> tripList;
	if(sourceTimeDestination.find(source)!=sourceTimeDestination.end())
	{	for(int tw=0; tw < window/15 + 1; tw++)
		{	if(sourceTimeDestination[source].find(tw+start_time)!=sourceTimeDestination[source].end())
				{
					for(int it; it < sourceTimeDestination[source][tw+start_time].size(); it++)
						tripList.push_back(sourceTimeDestination[source][tw+start_time][it]);
				}
		}
		return tripList;
	}	
	else
		return tripList;
}


int get_weight(int S,int D,int V,int start_time,float Tsd)
{
	
  int score = 0;
  float Te = 0;
  int V0 = S;
  

  vector<int> dest_list ; 

  dest_list = get_all_trips(V,start_time, beta);
  for(int i=0;i<dest_list.size();i++)
  	cout<<dest_list[i]<<"# ";
  for (int i=0;i<dest_list.size();i++)
  { 
  	int W = dest_list[i];
    float Tsv = shortest_lengths[node_to_index[S]][V];;
    float Tvw = shortest_lengths[node_to_index[V]][W];
	float Twd = shortest_lengths[node_to_index[W]][D];

    if((Tsv + Tvw + Twd) < ((1+alpha)*Tsd))
  		score +=1;
    else {
    
        float Tvd = shortest_lengths[node_to_index[V]][D];
    	float Tdw = shortest_lengths[node_to_index[D]][W];
   
      	if((Tvd + Tdw) < ((1+alpha)*Tvw))
  			score +=1;
  		  }
  }
  return score;
}


vector<int> dijkstra_lengths(int S, int D)
{ 
  map<int, int> path;
  
  for(int i=0; i< num_nodes; i++)
  { sld[nodes[i]] = 1000000;
    path[nodes[i]] = -1;
  }

  sld[S] = 0;
  priority_queue< pair<float, int> > dj;
  dj.push(make_pair(0, S));

  while(dj.size())
  { 
    pair<float, int> x = dj.top();
    dj.pop();  
    int u = x.second;
    for(int i=0; i<edges[node_to_index[u]].size(); i++)
    { int v = edges[node_to_index[u]][i];
      float alt = sld[u] + edge_weight[node_to_index[u]][v];
      if(alt < sld[v])
      { sld[v] = alt; path[v] = u;
        dj.push(make_pair(-alt, v));
      }
    }
  }

  vector<int> sp;
  int x = D;
  while(1)
  { sp.push_back(x);
    if(x==S)
      break;
    x = path[x];
  }
  sp.push_back(S);

  return sp;
}

vector<int> dijkstra_pscore(int S, int D,float lim)
{
	map<int, int> path;
	map<int, float> dist;
	map<int, float> abs_dist;
  
  for(int i=0; i< num_nodes; i++)
  { abs_dist[nodes[i]] = 1000000;
  	dist[nodes[i]] = 1000000;
    path[nodes[i]] = -1;
  }

  dist[S] = 0;
  abs_dist[S] = 0;
  priority_queue< pair<float, int> > dj;
 dj.push(make_pair(0, S));

  while(dj.size())
  { 
    pair<float, int> x = dj.top();
    dj.pop();
    
    int u = x.second;
    if(abs_dist[u]>lim)
    	continue;
    for(int i=0; i<edges[node_to_index[u]].size(); i++)
    { int v = edges[node_to_index[u]][i];
      float alt = dist[u] + ( edge_weight[node_to_index[u]][v] )/(weights[u] + 1);
      if(alt < dist[v])
      { dist[v] = alt; path[v] = u;
      	abs_dist[v] = abs_dist[u] + edge_weight[node_to_index[u]][v];
        dj.push(make_pair(-alt, v));
      }
    }
  }

  vector<int> sp;
  int x = D;
  while(1)
  { sp.push_back(x);
    if(x==S)
      break;
    x = path[x];
  }
  sp.push_back(S);
  reverse(sp.begin(),sp.end());
  return sp;
}


struct request { 
			int source;
			int destination;
			int start_time;
	};

vector<float> potential_score(struct request req_ob) //return[0] will be npscore,[1] would be spscore,[2] would be length ratios
{
  int S = req_ob.source;
  int D = req_ob.destination;
  int start_time = req_ob.start_time;
  vector<int> sp;
  sp = dijkstra_lengths(D,S);
  // for(int i=0; i<sp.size(); i++)
  // {
  // 	cout<<"#"<<sp[i]<<endl;
  // }

  cout<<S<<" "<<D<<endl;
   float spl = shortest_lengths[node_to_index[S]][D];
  cout<<"*****"<<node_to_index[S]<<" "<<spl<<endl;
  int total = 0;
  int sp_score = 0;
  for (int i=0;i<nodes.size();i++)
  {
  	int node = nodes[i];
   weights[node] = get_weight(S,D,node,start_time,spl);
 
  }
  for(int i=0;i<sp.size();i++)
  {
  	int node = sp[i];
  	sp_score += weights[node];
  }
 vector<int> np;
 np = dijkstra_pscore(S,D,(1+alpha)*spl);


 // for(int i=0; i<np.size(); i++)
 //  {
 //  	cout<<"*"<<np[i]<<endl;
 //  }

  int np_score = 0;
  float npl = 0;

  for(int i=0;i<np.size()-1;i++)
  {	
    np_score += weights[np[i]];
    try
  	 { npl += edge_weight[node_to_index[np[i]]][np[i+1]]; }
    catch(...)
      { npl+=0; }
  }
  cout<<sld[S]<<" "<<npl<<endl;
  if(np.size()>0)
    np_score += weights[np[np.size()-1]];
 vector<float> ret_ob;
 ret_ob.push_back(np_score);
 ret_ob.push_back(sp_score);
 if(npl>0)
 ret_ob.push_back(spl/float(npl));
else
	ret_ob.push_back(-1);
return ret_ob;   

 }

      


int main()
{
	
	ifstream file;
	file.open("sourceDestinationTime");
	while(file) {
	    string line;
	  	getline(file, line);
	  	int source,destination,_time;
	  	stringstream ss(line);
	    ss>>source; ss>>destination; ss>>_time;
	    sourceTimeDestination[source][_time].push_back(destination);
	}	

	ifstream ifile("beijing_adj_fromhistory_new.txt");
	
	string read_buf;
	getline(ifile,read_buf,',');	
	num_nodes = atoi(read_buf.c_str());
	getline(ifile,read_buf);
	num_edges = atoi(read_buf.c_str());
	cout<<num_nodes<<" "<<num_edges;
	int index = 0;
	
	for(int i=0;i<num_nodes;i++,index++)
	{
		getline(ifile,read_buf);
		int node = atoi(read_buf.c_str());
		nodes.push_back(node);
		node_to_index[node] = index;
		index_to_node[index] = node;
		vector<int> temp1;
		map < int,float > temp2;
		map < int,float > temp3;
		shortest_lengths.push_back(temp2);
		edges.push_back(temp1);
		edge_weight.push_back(temp3);
		
	}

	for(int i=0;i<num_edges;i++)
	{
		getline(ifile,read_buf,',');
		int node1,node2;
		getline(ifile,read_buf,',');
		node1 = atoi(read_buf.c_str());
		getline(ifile,read_buf,',');
		node2 = atoi(read_buf.c_str());
		float weight;
		getline(ifile,read_buf,',');
		weight = atof(read_buf.c_str());
		getline(ifile,read_buf,'\n');
		int is_one_way = (read_buf[(read_buf.length())-1]) - '0';
		//if(!is_one_way)
		{
			edges[node_to_index[node2]].push_back(node1);
			edge_weight[node_to_index[node2]][node1] = weight;
		}
		edges[node_to_index[node1]].push_back(node2);
		
		edge_weight[node_to_index[node1]][node2] = weight;		

	}
	ifile.close();


	fstream sl_file;
	sl_file.open("all_pairs_shortest_lengths.txt",ios::in);
	cout<<"\n";
	while(!sl_file.eof())
	{   
		int m;
		int s;
		sl_file>>m>>s;
		int n; float d;
		for(int i=0;i<s;i++)
		{	sl_file>>n>>d;
			shortest_lengths[node_to_index[m]][n] = d;
		}

	}
	sl_file.close();

	cout<<"Now begins the task\n\n\n\n\n\n";



	map< int,map<int, vector<int> > >::iterator it1;
	for(it1 = sourceTimeDestination.begin(); it1!=sourceTimeDestination.end(); it1++)
	{
		for(map<int, vector<int> >::iterator it2 = (it1->second).begin(); it2 != (it1->second).end(); it2++)
		{	
		
			for(int it3 = 0; it3<(it2->second).size();it3++)
			{
				int s = it1->first;
				int d = (it2->second)[it3];
				int t = it2->first;
				request ob;
				ob.start_time = t;
				ob.source = s;
				ob.destination = d;
				cout<<s<<" "<<d<<endl;
				vector<float> result = potential_score(ob);
				cout<<result[0]<<" "<<result[1]<<" "<<result[2]<<endl;
				break;
			}
			break;
		}
		break;
	}


	cout<<"Done";
	return 0;
}