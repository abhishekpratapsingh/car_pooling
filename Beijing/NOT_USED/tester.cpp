#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <fstream>
#include <sstream>
#include <queue>

using namespace std;

#define mk(a, b) make_pair(a, b)
#define pb(a) push_back(a)

int main()
{
	vector< pair<int,int> > mp;
	
	mp.pb( mk(0,1) ); mp.pb( mk(50,20) ); mp.pb( mk(30, 30) );

	sort(mp.begin(), mp.end());
	for(int i=0;i<100;i++)
		cout<<i<<" "<<(lower_bound(mp.begin(), mp.end(), mk(i, 0) )-mp.begin())<<endl;

return 0;
}