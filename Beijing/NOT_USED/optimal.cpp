#include <bits/stdc++.h>
#include <ctime>
#include "ioD.h"

using namespace std;

float alpha  = 1.1;
long long int beta = 0;
long long int maxDistance = 100000;
/* nodes global structure */
vector< pair<double, double> > nodes;
vector<long long int> nodeID;
map<long long int, long long int> idToNode;
map< long long int, pair<double, double> > nodeToLatLon;

/* Edges global structure */
map< long long int, vector<long long int> > edges;
map< long long int, vector<double> > edgeWeight;
map< long long int, vector<long long int> > edgesReverse;
map< long long int, vector<double> > edgeWeightReverse;

map< long long int, map<long long int, vector<long long int> > > sourceTimeDestination; 

double minPathScore[2000];

double dagScore[2000];
double dagTimeTaken[2000];

double BdagScore[2000][10];
double BdagTimeTaken[2000][10];

long long int tripNumber = 0;
clock_t startTimeTrip;
clock_t endTimeTrip;


/* TWO HOP*/
long long n;

int queryCnt;

char * txtName, * degName, * outName, *inName;

edgeL * deg;
edgeS * labelout, *labelin;
edgeS * labelx, * labely;

/* TWO HOP */

bool fc = 0;

int query(int x, int y)
{
	if (x == y) return 0;
	int xx = x, yy = y;

	x = ((deg[xx].x<<32)>>32);
	y = ((deg[yy].x<<32)>>32);
	
	if (x > y)
	{
		labelx = labelout + deg[xx].w;
		labely = labelin + deg[yy].y;
	}
	else
	{
		int xy = x; x = y; y = xy;
		labelx = labelin + deg[yy].y;
		labely = labelout + deg[xx].w;
	}
	
	int ans = 1000000, i = 0, j = 0;

	
	if (labelx[i].x != -1 && labely[j].x != -1)
	while (labelx[i].x < y)
	{
		if (labelx[i].x == labely[j].x) 
		{
			ans = ans>(labelx[i].w + labely[j].w)?(labelx[i].w + labely[j].w):ans;
			if (labelx[++i].x == -1) break;
			if (labely[++j].x == -1) break;
		}
		else if (labelx[i].x < labely[j].x)
		{
			if (labelx[++i].x == -1) break;
		}
		else if (labely[++j].x == -1) break;
	}
	
	while (labelx[i].x != -1 && labelx[i].x < y) i++;
		if (labelx[i].x == y) ans = ans>labelx[i].w?labelx[i].w:ans;

	return ans;
}


void loadIndex()
{
	inBufL degBuf(degName);
	inBufS inLabel(inName), outLabel(outName);
	
	n = checkB(degName)/sizeof(edgeL);

	deg = (edgeL *)malloc(sizeof(edgeL)*n);
	labelin = (edgeS*)malloc(checkB(inName));
	labelout = (edgeS*)malloc(checkB(outName));

	printf("%lld vertices\n", n);

	degBuf.start();
	for (int i = 0; i < n; i++)
		degBuf.nextEdge(deg[i]);

	inLabel.start();
	for (int i = 0; !inLabel.isEnd; i++)
		inLabel.nextEdge(labelin[i]);
	
	outLabel.start();
	for (int i = 0; !outLabel.isEnd; i++)
		outLabel.nextEdge(labelout[i]);
			
}

void getTimeSourceDestination() {
	ifstream file;
	file.open( "outpu" );
	string s;
	while( getline(file, s) ) {
		stringstream ss( s );
		long long int source, timeSlot, dest;
		ss>>source>>timeSlot;
		while( ss>>dest ) {
			sourceTimeDestination[ source ][ timeSlot ].push_back( dest );
		}
	}
}

void takeGraphInput() {
	ifstream Location;
	Location.open("beijing_adj_fromhistory_new.txt");
	string s;
	int index = 0, numNodes = 0, numEdge = 0;
	
	getline(Location, s);
	stringstream ss(s);
	char ch;
	ss>>numNodes>>ch>>numEdge;

	while( getline(Location, s) ) {
		ss.str( std::string() );
		ss.clear();
		ss<<s;
		int id; double lon; double lat; 
		ss>>id>>ch>>lat>>ch>>lon; 
		nodes.push_back( make_pair( lon, lat) );
		nodeID.push_back( id );
		idToNode[ id ] = index;
		nodeToLatLon[ id ] = make_pair( lat, lon);
		index += 1;
		if(nodes.size() == numNodes)
			break;
	}

	while( getline(Location, s) ) {
		ss.str( std::string() );
		ss.clear();
		ss<<s;
		long long int numRandom; long long int node1; long long int node2; double weight; char ch; int oneWay;
		ss>>numRandom>>ch>>node1>>ch>>node2>>ch>>weight>>ch>>oneWay;
		edges[ node1 ].push_back( node2 );
		edgeWeight[ node1 ].push_back( weight );
		edgesReverse[ node2 ].push_back( node1 );
		edgeWeightReverse[ node2 ].push_back( weight );

		if( !oneWay ) {
			int temp = node1; node1 = node2; node2 = temp;
			edges[ node1 ].push_back( node2 );
			edgeWeight[ node1 ].push_back( weight );

			edgesReverse[ node2 ].push_back( node1 );
			edgeWeightReverse[ node2 ].push_back( weight );
		}

	}

	Location.close();
	return ;
}

vector<long long int>  dijkstra_lengths(long long int S, long long int D, map<long long int, double > &distanceFromSource,
 map<long long int, vector<long long int> > &edges, map<long long int, vector<double> > &edgeWeight ) { 
	
   	map<long long int, long long int> path;
	for(int i=0; i< nodeID.size(); i++)
	{ 	distanceFromSource[ nodeID[i] ] = 100000;
		path[ nodeID[i] ] = -1;
	}
	
	distanceFromSource[S] = 0;
	priority_queue< pair<float, long long int> > dj;
	dj.push( make_pair(0, S) );

	while(dj.size()) { 
		pair<float, long long int> x = dj.top();
		dj.pop();
		long long int u = x.second;
		for(int i=0; i<edges[u].size(); i++) { 	
			long long int v = edges[u][i];
			float alt = distanceFromSource[ u ] + edgeWeight[u][i];
			if(alt < distanceFromSource[v])
			{ 	distanceFromSource[v] = alt;
				dj.push( make_pair(-alt, v) );
				path[v] = u;
			}
		}
	}

	vector<long long int> path1;
	long long int x = D;
	while(1) {
		path1.push_back(x);
		
		if( x == S || x == -1 || path.find(x) == path.end() )
		  	break;

		x = path[ x ];
	}
	
	path1.push_back(S);

	return path1;
}

double getDistanceFromTo(long long int S, long long int D) { 
	map<long long int, float> dist;
 
	for(int i=0; i< nodeID.size() ; i++)
	{ 	dist[ nodeID[i] ] = 100000;
	}

	dist[S] = 0;
	priority_queue< pair<float, long long int> > dj;
	dj.push( make_pair(0, S) );

	while(dj.size())
	{ 
		pair<float, long long int> x = dj.top();
		dj.pop();
		
		long long int u = x.second;
		
		if( u == D)
			break;
		
		for(int i=0; i< edges[u].size(); i++)
		{ 	long long int v = edges[u][i];
			float alt = dist[u] + edgeWeight[u][i];
			if(alt < dist[v])
			{ 	dist[v] = alt; 
				dj.push(make_pair(-alt, v));
			}
		}
	}

	return dist[ D ];
}

void get_all_trips(long long int source,long long int start_time, vector<long long int> &tripList) {

	if( sourceTimeDestination.find(source) != sourceTimeDestination.end() ) {
		if( sourceTimeDestination[source].find(start_time) 
					!= sourceTimeDestination[source].end() ) {
			
			vector<long long int> addMe = sourceTimeDestination[source][ start_time ] ;
			
			for(int i = 0; i < addMe.size(); i++) {
				tripList.push_back( addMe[i] );
			}

		}
	}
}

long long int get_weight(long long int source, long long int destination, long long int v, long long int start_time,
 	map<long long int, double> &distanceToDestination, map<long long int, double> &distanceFromDestination)
{  	
	vector<long long int> tripList;
	
	get_all_trips(v, start_time/15, tripList);
  	return tripList.size();

  	long long int score = 0;
  	for(int i = 0; i < tripList.size(); i++) {
  		long long int w = tripList[ i ]; 
		double tvw = getDistanceFromTo(v, w);
        double tvd = distanceToDestination[ v ];
      	double tdw = distanceFromDestination[ w ];
      	if( (tvd + tdw) < ( (1+alpha)*tvw) ) {
      		score += 1;
      	} 
  	}
  	return score;
}

long long int dagScore1(long long int source, long long int destination, map<long long int, double > &distanceFromSource,
	map<long long int, double > &distanceToDestination, map<long long int, long long int > &weights) {
	vector< pair< double, long long int> > dag;
	long long int sourceIndex = idToNode[ source ];
	long long int destinationIndex = idToNode[ destination ];
	// cout<<distanceFromSource[ destination ]<<endl;
	for(int i=0; i <  nodes.size() ; i++) {	
		if( ( distanceFromSource[ nodeID[ i ] ] + distanceToDestination[ nodeID[ i ] ] )
			 < alpha*distanceFromSource[ destination ] )
			  {
			dag.push_back( make_pair(-distanceToDestination[ nodeID[i] ], nodeID[ i ]) );
		}
	} 

	sort( dag.begin(), dag.end() );
	vector< map<int, double> > scores( dag.size() );
  
  	map<long long int, long long int> nodeToDagIndex;
	// cout<<dag.size()<<endl;
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
		
	long long int startIndex = nodeToDagIndex[source];
  	long long int endIndex = nodeToDagIndex[destination];

  	scores[ startIndex ][ 0 ] = 0;
  	for(int i = startIndex; i < dag.size(); i++) {
    	long long int u = dag[i].second;
    	// cout<<weights[u]<<endl;
	    for(int j = 0; j < edges[u].size(); j++) {
	    	long long int v = edges[u][j];
	      	long long int vIndex = nodeToDagIndex[v];
	      	if( nodeToDagIndex.find( v ) == nodeToDagIndex.end()  )
	        	continue;
	      	//some nodes are ajacent to themselves in the graph
	      	if(u == v)
	        	continue;

	      	if( vIndex >= i ) {
	      		for(map<int, double>::iterator it=scores[ i ].begin(); it != scores[ i ].end(); it++)
	        	{ 
	        		// cout<<it->first<<endl;
	        		if( it->first > 1000)
	        			continue;

	          		if( scores[ nodeToDagIndex[v] ].find( it->first + weights[v] ) != scores[ nodeToDagIndex[v] ].end() ) {
			          	if( scores[ nodeToDagIndex[v] ][it->first + weights[v] ] >= it->second + edgeWeight[u][j] ) {
			            	scores[ nodeToDagIndex[v] ][it->first + weights[v] ] = it->second + edgeWeight[u][j];
			            }
			        }
			        else
			        {
			        	scores[ nodeToDagIndex[v] ][it->first + weights[v] ] = it->second + edgeWeight[u][j];	
			        }
				}
			}
		}
	}
	long long int bestScore = 0;

	for( map<int, double>::iterator it=scores[ endIndex ].begin(); it != scores[ endIndex ].end(); it++) {
		
		if( it->second < distanceToDestination[ source ]*alpha ) {
			bestScore = it->first;
		}
	}
	cout<<"BEST SCORE"<<bestScore<<endl;
	return bestScore;

}



long long int dagScoreBack(long long int source, long long int destination, map<long long int, double > &distanceFromSource,
	map<long long int, double > &distanceToDestination, map<long long int, long long int > &weights) {

	vector< pair< double, long long int> > dag;
	long long int sourceIndex = idToNode[ source ];
	long long int destinationIndex = idToNode[ destination ];
	for(int i=0; i <  nodes.size() ; i++) {	
		if( ( distanceFromSource[ nodeID[ i ] ] + distanceToDestination[ nodeID[ i ] ] )
			 < alpha*distanceFromSource[ destination ] )
			  {
			dag.push_back( make_pair(-distanceToDestination[ nodeID[i] ], nodeID[ i ]) );
		}
	} 

	sort( dag.begin(), dag.end() );
	
	vector<double> sc(1000, maxDistance);
  	vector< vector<double> > scores(dag.size(), sc);
  
  	map<long long int, long long int> nodeToDagIndex;
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
		
	long long int startIndex = nodeToDagIndex[source];
  	long long int endIndex = nodeToDagIndex[destination];

  	scores[ startIndex ][ 0 ] = 0;
  	map<long long int, map<long long int, long long int> > reach;
  	map<long long int, map<long long int, long long int> > reachFrom;

  	for( int iter = 0 ; iter < 1; iter++) {
  		long long int backPass = 0;
  		if( iter%2 )
  			backPass = 1;
	  	for(int i = 0; i < dag.size(); i++) {
	    	long long int u = dag[i].second;

		    for(int j = 0; j < edges[u].size(); j++) {

		    	long long int v = edges[u][j];
		      	
		      	if( reach[v][u] )
		    		continue;
		      	
		      	long long int vIndex = nodeToDagIndex[v];
		      		   
		      	if( nodeToDagIndex.find( v ) == nodeToDagIndex.end() )
		        	continue;
		      	//some nodes are ajacent to themselves in the graph
		      	if(u == v)
		        	continue;

		      	if( (vIndex >= i) ^ backPass) {
		      		bool updated = false;
		        	for( int k =0; k + weights[v] < 1000; k++) { 
		         
		          		if( scores[i][k] == maxDistance ) 
		          			continue;

			          	if( scores[ nodeToDagIndex[v] ][k + weights[v] ] >= scores[i][k] + edgeWeight[u][j] ) {
			            	scores[ nodeToDagIndex[v] ][k + weights[v] ] = scores[i][k] + edgeWeight[u][j];
			          
			            	if( updated )
		          				continue;
		          			
		          			updated = true;

			            	reachFrom[v][u] = 1;
			            	reach[u][v] = 1;
			            	for( map<long long int, long long int>::iterator it = reachFrom[u].begin(); it != reachFrom[u].end(); it++) {
			            		reach[it->first][v] = 1;
			            		reachFrom[v][it->first] = 1;
			            	}

			            }
					}
				}
			}
		}
		if( !backPass )
			continue;
		for( int k = 999; k >= 0; k--) {
			if(scores[endIndex][k] == maxDistance)
				continue;
			if( scores[endIndex][k] < distanceToDestination[ source ]*alpha ) {
				BdagScore[tripNumber][iter/2] = k - weights[destination];
				break;
			}
		}
		BdagTimeTaken[tripNumber][iter/2] = (clock()-startTimeTrip)/ (float) CLOCKS_PER_SEC;
	}
	long long int bestScore = 0;

	for( int k = 999; k >= 0; k--) {

		if(scores[endIndex][k] == maxDistance)
			continue;

		if( scores[endIndex][k] < distanceToDestination[ source ]*alpha ) {
			bestScore = k - weights[destination];
			break;
		}
	}

	return bestScore;

}

void potentialScore(long long int source, long long int destination, long long int timeSlot)
{

	srand(time(NULL));
	int cnt = 0;

	startTimeTrip = clock();
	map< long long int, double > distanceFromSource;
	map< long long int, double > distanceFromDestination;
	map< long long int, double > distanceToDestination;
	if( ( idToNode.find( source ) == idToNode.end() ) || ( idToNode.find( destination ) == idToNode.end() )  )
	{	tripNumber--;
		return;
	}

	vector<long long int> path = dijkstra_lengths(source, destination, distanceFromSource,
	 edges, edgeWeight);

	if( distanceFromSource[destination] == maxDistance ) {
		tripNumber--;
		return;
	}
	
	dijkstra_lengths(destination, source, distanceFromDestination, edges, edgeWeight);
	dijkstra_lengths(destination, source, distanceToDestination, edgesReverse,
	 edgeWeightReverse);

	cout<<float(clock()-startTimeTrip)/CLOCKS_PER_SEC<<endl;
	cout<<"DONE"<<endl;
	// cout<<(clock()- startTimeTrip)/CLOCKS_PER_SEC<<endl;
	map<long long int, long long int> weights; 
	startTimeTrip = clock();
	srand(time(NULL));
	int coutt =0;
	
	for( int i =0; i < nodeID.size(); i++)
	{
		if( ( distanceFromSource[ nodeID[i] ] + distanceToDestination[ nodeID[i] ] ) 
				< alpha*distanceFromSource[ destination ] ) {
			long long int passengersNumber;
			weights[ nodeID[i] ] = rand()%10;
			coutt += 1;
		}
	}
	cout<<"NUM"<<" "<<coutt<<endl;
	long long int scoreMinPath = 0;
	for( int i = 1; i < path.size() - 1; i++) {
		scoreMinPath += weights[ path[i] ];
	}
	cout<<"PATH SIE"<<path.size()<<endl;
	cout<<( clock() - startTimeTrip )/CLOCKS_PER_SEC<<endl;
	minPathScore[ tripNumber ] = float( scoreMinPath  + 1);
	/* calculating DAG parameters */
	startTimeTrip = clock();
	long long int scoreDag = dagScore1(source, destination, distanceFromSource,
	distanceToDestination, weights);
	dagTimeTaken[ tripNumber ] =  ( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC;
	dagScore[ tripNumber ] = float( scoreDag + 1) / minPathScore[ tripNumber ];
	cout<<"Time Taken for DAG"<<dagTimeTaken[ tripNumber ]<<endl;
		
	/* calculating backDAG parameters */
	startTimeTrip = clock();
	scoreDag = dagScoreBack(source, destination, distanceFromSource,
	distanceToDestination, weights);
	cout<<scoreDag<<endl;
	cout<<"TIME TAKEN ITER"<<( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC<<endl;
	for( int i=0 ; i < 10; i++)
		BdagScore[ tripNumber ][ i ] = float( BdagScore[ tripNumber ][ i ] + 1 ) /( minPathScore[ tripNumber ] );
	
	return ;

}

/* edges global structure */
int main(int argc, char const *argv[])
{
	char *txt = "beijingIndex";
	txtName = txt;
	degName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(degName, "%s.deg", txtName);
	
	inName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(inName, "%s.labelin", txtName);
	outName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(outName, "%s.labelout", txtName);

	timer tm;
	
	loadIndex();
	//cout<<"QUERY: "<<query(58574, 35588)<<endl;
	memset(dagScore, 0, sizeof(dagScore));
	memset(minPathScore, 0, sizeof(minPathScore));
	memset(BdagScore,0,sizeof(BdagScore));
	memset(BdagTimeTaken,0,sizeof(BdagTimeTaken));
	memset(dagTimeTaken,0,sizeof(dagTimeTaken));
	string s = argv[1];
	stringstream ss(s);
	ss>>alpha; 

	getTimeSourceDestination();
	takeGraphInput();
	//cout<<getDistanceFromTo( nodeID[58574], nodeID[35588])<<endl;
	tripNumber = 0;
	for( int i=0; tripNumber < 30; i += 1) {
		long long int source, timeS, destination;
		source = nodeID[i];
		if( sourceTimeDestination.find( nodeID[i] ) == sourceTimeDestination.end() )
			continue;
		cout<<tripNumber<<endl;
		for(map<long long int, vector<long long int> >::iterator it = sourceTimeDestination[ nodeID[i] ].begin(); 
			it != sourceTimeDestination[ nodeID[i] ].end() ; it++ )
		{
			timeS =it->first;
			destination = it->second[ 0 ];
			
			startTimeTrip = clock();
			potentialScore(source, destination, timeS);
			tripNumber++;
			if( tripNumber > 30 )
				break;
		}

	}

	double avgMinPath, avgDAGScore, avgDAGtime;
	double avgBackDAGScore[10], avgTimeBack[10];
	long long int hist[10001];
	memset(hist, 0, sizeof(hist));
	
	for(int i = 0; i < 30; i++) {

		avgMinPath += minPathScore[i];
		avgDAGScore += dagScore[i];
		avgDAGtime += dagTimeTaken[i];

		for(int j = 0; j < 10; j++) {
			avgBackDAGScore[j] += BdagScore[i][j];
			avgTimeBack[j] += BdagTimeTaken[i][j]/dagTimeTaken[i];
		}

		hist[  int(BdagScore[i][9]) ] += 1;
	}

	cout<<avgMinPath*alpha/100<<" "<<avgDAGScore/100<<" "<<avgDAGtime/100<<endl;
	
	for(int j = 0; j < 10; j++)
	{
		cout<<avgBackDAGScore[j]/100<<" "<<avgTimeBack[j]/100<<endl;
	}
	for(int j = 0; j <= 100; j++)
	{
		cout<<hist[j]<<" ";
	}
	
	free(degName);
	free(inName);
	free(outName);
	
	free(deg);
	free(labelin);
	free(labelout);

	return 0;
}


/*
long long int Optimal(long long int source, long long int destination, map<long long int, double > &distanceToDestination,
 	map<long long int, long long int> &nodeToDagIndex, vector< vector<long long int> > &scores, long long int pathLen,
 	map<long long int, long long int > &weights, long long int maxIndex, vector<long long int> &path, long long int limDistance) {
	cout<<source<<" "<<pathLen<<endl;
	
	for(int i=0;i<path.size();i++)
	{
		if( path[i]==source)
			return 0;
	}

	if( nodeToDagIndex.find( source) == nodeToDagIndex.end() )
		return 0;
	cout<<source<<" 2 "<<pathLen<<endl;
	if( source == destination) {
		for(int k=100; k>=0; k--){
			if( scores[ idToNode[destination] ][k] )
				return k;
		}
	}
	cout<<source<<" 3 "<<pathLen<<endl;
	if( pathLen + distanceToDestination[source] > limDistance)
		return 0;
	cout<<source<<" 4 "<<pathLen<<endl;
	path.push_back( source);
	
	if( path.size() > beta ) {
		maxIndex = max(maxIndex, nodeToDagIndex[source]);
	}

	long long int bestScore = 0;
	long long int u = source;
	for(int j = 0; j< edges[source].size(); j++) {
    	long long int v = edges[u][j];
      	bool pushed = false;
      	cout<<nodeToDagIndex[v]<<endl;
      	if( maxIndex >= nodeToDagIndex[v] ) {	
        	for( int k =0; k + weights[v] < 100; k++) { 
          		if( scores[u][k] == -1 )
          			continue;
	          	if( scores[u][k + weights[v] ] > scores[u][k] + edgeWeight[u][j] ) {
	            	scores[u][k + weights[v] ] = scores[u][k] + edgeWeight[u][j];
	            	pushed = true;
	            }
			}
		}
		if( pushed ) {
			bestScore = max( bestScore, Optimal( v, destination, 
			distanceToDestination, nodeToDagIndex, scores, pathLen + edgeWeight[u][j],
			weights, maxIndex, path, limDistance));
		}
	}

	path.pop_back();

	return bestScore;
}
;
	beta = 0;
	startTimeTrip = clock();
	
	fill(scores.begin(), scores.end(), sc);
	scores[ nodeToDagIndex[source] ][0] = 0;
  	beta = 0;
	sc[0] += Optimal(source, destination, distanceToDestination, nodeToDagIndex, scores, 0,
	weights, -1, path2, limDistance);
	ttime[0] += float( clock() - startTimeTrip + preProcessTime)/(dagTime + preProcessTime);
	
	fill(scores.begin(), scores.end(), sc);
	scores[ nodeToDagIndex[source] ][0] = 0;
	beta = 1;
	startTimeTrip = clock();
	sc[1] += Optimal(source, destination, distanceToDestination, nodeToDagIndex, scores, 0,
	weights, -1, path2, limDistance);
	ttime[1] += float( clock() - startTimeTrip + preProcessTime)/(dagTime + preProcessTime);
	
	fill(scores.begin(), scores.end(), sc);
	scores[ nodeToDagIndex[source] ][0] = 0;
	beta = 2;
	sc[2] += Optimal(source, destination, distanceToDestination,nodeToDagIndex, scores, 0,
	weights, -1, path2, limDistance);
	ttime[2] += float( clock() - startTimeTrip + preProcessTime)/(dagTime + preProcessTime);
	
	fill(scores.begin(), scores.end(), sc);
	scores[ nodeToDagIndex[source] ][0] = 0;
	beta = 5;
	sc[3] += Optimal(source, destination, distanceToDestination, nodeToDagIndex, scores, 0,
	weights, -1, path2, limDistance);
	ttime[3] += float( clock() - startTimeTrip + preProcessTime)/(dagTime + preProcessTime);
	
	fill(scores.begin(), scores.end(), sc);
  	scores[ nodeToDagIndex[source] ][0] = 0;
	beta = 10;
	sc[4] += Optimal(source, destination, distanceToDestination, nodeToDagIndex, scores, 0,
	weights, -1, path2, limDistance);
	ttime[4] += float( clock() - startTimeTrip + preProcessTime)/(dagTime + preProcessTime); */