#include <bits/stdc++.h>
#include <ctime>
#include "ioD.h"

using namespace std;

long long n;
int queryCnt;
char * txtName, * degName, * outName, *inName;
edgeL * deg;
edgeS * labelout, *labelin;
edgeS * labelx, * labely;
bool fc = 0;

// Takes in node index
double query(int x, int y)
{
	if (x == y) return 0;
	int xx = x, yy = y;

	x = ((deg[xx].x<<32)>>32);
	y = ((deg[yy].x<<32)>>32);
		
	if (x > y)
	{
		labelx = labelout + deg[xx].w;
		labely = labelin + deg[yy].y;
	}
	else
	{
		int xy = x; x = y; y = xy;
		labelx = labelin + deg[yy].y;
		labely = labelout + deg[xx].w;
	}

	int ans = 1000000, i = 0, j = 0;

	if (labelx[i].x != -1 && labely[j].x != -1)
	while (labelx[i].x < y)
	{
		if (labelx[i].x == labely[j].x) 
		{
			ans = ans>(labelx[i].w + labely[j].w)?(labelx[i].w + labely[j].w):ans;
			if (labelx[++i].x == -1) break;
			if (labely[++j].x == -1) break;
		}
		else if (labelx[i].x < labely[j].x)
		{
			if (labelx[++i].x == -1) break;
		}
		else if (labely[++j].x == -1) break;
	}
	
	while (labelx[i].x != -1 && labelx[i].x < y) i++;
	if (labelx[i].x == y) ans = ans>labelx[i].w?labelx[i].w:ans;

	return float(ans)/1000;
}

void loadIndex()
{
	inBufL degBuf(degName);
	inBufS inLabel(inName), outLabel(outName);
	
	n = checkB(degName)/sizeof(edgeL);

	deg = (edgeL *)malloc(sizeof(edgeL)*n);
	labelin = (edgeS*)malloc(checkB(inName));
	labelout = (edgeS*)malloc(checkB(outName));

	printf("%lld vertices\n", n);

	degBuf.start();
	for (int i = 0; i < n; i++)
		degBuf.nextEdge(deg[i]);

	inLabel.start();
	for (int i = 0; !inLabel.isEnd; i++)
		inLabel.nextEdge(labelin[i]);
	
	outLabel.start();
	for (int i = 0; !outLabel.isEnd; i++)
		outLabel.nextEdge(labelout[i]);			
}

int DEBUG = 1;
bool PRINT_PATH = true;
float alpha  = 1.3;
double maxDepth = 0.2;
long long int beta = 0;
long long int maxDistance = 100000;
/* nodes global structure */
vector< pair<double, double> > nodes;
vector<long long int> nodeID;
map<long long int, long long int> idToNode;
map< long long int, pair<double, double> > nodeToLatLon;

/* Edges global structure */
vector< vector<long long int> > edges;
vector< vector<double> > edgeWeight;
vector< vector<long long int> > edgesReverse;
vector< vector<double> > edgeWeightReverse;

vector< map<long long int, vector<long long int> > > sourceTimeDestination; 

int countWin = 0;

double minPathScore[1000];

double dagScore[1000];
double dagTimeTaken[1000];

double dagExScore[1000];
double dagExTime[1000];

long long int tripNumber = 0;
clock_t startTimeTrip;
clock_t endTimeTrip;

// TODO: To get rid of
double distFromSourceToDestination; 

void getTimeSourceDestination() {
	ifstream file;
	file.open( "outpu" );
	string s;
	int trips = 0;
	sourceTimeDestination.resize(nodeID.size());
	while( getline(file, s) ) {
		stringstream ss( s );
		long long int source, timeSlot, dest;
		ss>>source>>timeSlot;
		source = idToNode[source];
		while( ss>>dest ) {
			trips++;
			dest = idToNode[dest];
			sourceTimeDestination[ source ][ timeSlot ].push_back( dest );
		}
	}
	cout<<trips<<endl;
	//exit(1);
}


void takeGraphInput() {
	ifstream Location;
	Location.open("beijing_adj_fromhistory_new.txt");
	string s;
	int index = 0, numNodes = 0, numEdge = 0;
	
	getline(Location, s);
	stringstream ss(s);
	char ch;
	ss>>numNodes>>ch>>numEdge;

	while( getline(Location, s) ) {
		ss.str( std::string() );
		ss.clear();
		ss<<s;
		int id; double lon; double lat; 
		ss>>id>>ch>>lat>>ch>>lon; 
		nodes.push_back( make_pair( lon, lat) );
		nodeID.push_back( id );
		idToNode[ id ] = index;
		nodeToLatLon[ id ] = make_pair( lat, lon); // not used?
		index++;
		if(nodes.size() == numNodes)
			break;
	}
	int count = 0;
	edges.resize(nodeID.size());
	edgeWeight.resize(nodeID.size());
	edgesReverse.resize(nodeID.size());
	edgeWeightReverse.resize(nodeID.size());
	while( getline(Location, s) ) {
		ss.str( std::string() );
		ss.clear();
		ss<<s;
		long long int numRandom; long long int node1; long long int node2; double weight; char ch; int oneWay;
		ss>>numRandom>>ch>>node1>>ch>>node2>>ch>>weight>>ch>>oneWay;
		node1 = idToNode[node1];
		node2 = idToNode[node2];
		
		edges[ node1 ].push_back( node2 );
		edgeWeight[ node1 ].push_back( weight );
		edgesReverse[ node2 ].push_back( node1 );
		edgeWeightReverse[ node2 ].push_back( weight );
		count = oneWay ? count +1: count+2;
		if( !oneWay ) {
			long long int temp = node1; node1 = node2; node2 = temp;
			edges[ node1 ].push_back( node2 );
			edgeWeight[ node1 ].push_back( weight );

			edgesReverse[ node2 ].push_back( node1 );
			edgeWeightReverse[ node2 ].push_back( weight );
		}

	}
	cout<<count+1<<endl;
	Location.close();
	return ;
}

vector<long long int>  dijkstra_lengths(long long int S, long long int D, vector< double > &distanceFromSource,
	vector< vector<long long int> > edges, vector< vector<double> > edgeWeight) { 
	
   	vector<long long int> prevNode(nodeID.size());
	for(int i=0; i< nodeID.size(); i++)
	{ 	distanceFromSource[ i ] = 100000;
		prevNode[ i ] = -1;
	}

	distanceFromSource[ S ] = 0;
	priority_queue< pair<float, long long int> > dj;
	dj.push( make_pair(0, S) );

	while(dj.size()) { 
		pair<float, long long int> x = dj.top();
		dj.pop();

		long long int u = x.second;
		for(int i=0; i< edges[ u ].size(); i++) { 	
			long long int v = edges[ u ][ i ];
			float alt = distanceFromSource[ u ] + edgeWeight[ u ][ i ];
			if(alt < distanceFromSource[ v ])
			{ 	distanceFromSource[ v ] = alt;
				dj.push( make_pair(-alt, v) );
				prevNode[v] = u;
			}
		}
	}

	vector<long long int> path;
	long long int x = D;
	while(true) {
		path.push_back(x);
		if( ( x == S ) || ( x == -1 ) )
		  	break;
		x = prevNode[ x ];
	}
	
	path.push_back(S);

	return path;
}

void printPath(vector<long long int> &path, vector< long long int > &weights) {
	double cumDist = 0;
	for (int i=0; i<path.size(); i++) {
		long long int v = path[i];
		if (i > 0) {
			long long int u = path[i-1];
			for (int j=0; j<edgeWeight[u].size(); j++) {
				if (edges[u][j] == v) {
					cumDist += edgeWeight[u][j];
					break;
				}
			}
		}
		printf("  %7lld:\t%lld\t(Dist: %.4f)\n",nodeID[v], weights[v], cumDist);
	}
	fflush(stdout);
}

int getPathScore(vector<long long int> &path, vector< long long int > &weights) {
	int score = 0;
	for(int i=1;i<path.size(); i++) {
		score += weights[ path[i] ];
	}
	return score;
}

vector<long long int> HD(long long int S, long long int D, vector< long long int > &expectedTrips) { 
	vector<float> dist(nodeID.size());
 
	for(int i=0; i< nodeID.size() ; i++)
	{ 	dist[ i ] = 100000;
	}

	dist[S] = 0;

	// ( (score, node), (distance, path) )
	priority_queue< pair< pair<float, long long int>, pair< float, vector<long long int> > > > dj;
	
	vector<long long int> path;
	path.push_back(S);
	dj.push( make_pair( make_pair(0, S), make_pair(0, path) ) );

	while(dj.size())
	{ 
		pair< pair<float, long long int>, pair< float, vector<long long int> >  > x = dj.top();
		dj.pop();
		
		long long int u = x.first.second;
		
		if( x.second.first > distFromSourceToDestination )
			continue; 
		
		if( u == D ) {
			return x.second.second;
		}
		
		for(int i=0; i< edges[u].size(); i++)
		{ 	long long int v = edges[u][i];

			/* BUG: should skip the whole i loop 
			for(int j=0; j<x.second.second.size(); j++) 
				if( x.second.second[j] == v)
					continue;
			*/
			if (find(x.second.second.begin(), x.second.second.end(), v) != x.second.second.end()) {
				continue;
			}

			float alt = -x.first.first + (edgeWeight[u][i]/(expectedTrips[v]+1) );
			if( (x.second.first + edgeWeight[u][i]) < dist[v] )
			{ 	dist[v] = (x.second.first + edgeWeight[u][i]); 
			    path = x.second.second;
			    path.push_back(v);
				dj.push( make_pair( make_pair(-alt, v), make_pair(dist[v], path) ) );
			}
		}
	}

	// not reachable
	path.clear();
	return path; 
}


void get_all_trips(long long int source,long long int start_time, vector<long long int> &tripList) {

	if( source < sourceTimeDestination.size() ) {
		if( sourceTimeDestination[source].find(start_time) != sourceTimeDestination[source].end() ) {

			for(int i = 0; i < sourceTimeDestination[ source][start_time].size(); i++) {
				tripList.push_back( sourceTimeDestination[source][ start_time ][ i ] );
			}

		}
	}
}

long long int get_expected_trips(long long int source, long long int destination, long long int midStop, long long int start_time,
 	vector<double> &distanceToDestination, vector<double> &distanceFromDestination)
{  	
	vector<long long int> tripList;
	
	get_all_trips(midStop, start_time/15, tripList);
  
  	int counter = 0;

  	long long int s = source;
  	long long int d = destination;
  	long long int v = midStop;

  	for(int i = 0; i < tripList.size(); i++) {

  		long long int w = tripList[ i ]; 
		double tsv = query(s, v);
 		double tvw = query(v, w);
        double tvd = query(v, d);
        double twd = query(w, d);
        double tdw = query(d, w);

      	if( tvd + tdw < (1+alpha)*tvw ) {
      		counter += 1;
      	}
      	else {
      		double distanceA = ( tsv + tvw + twd);
      		double distanceB = (1+alpha)*distFromSourceToDestination;
      		if( distanceA <= distanceB) {
      			counter += 1;
      		}
      	}
  	}
  	
  	return counter;
}

vector<long long int> dagScore1(long long int source, long long int destination, vector< double > &distanceFromSource,
	vector< double > &distanceToDestination, vector< long long int > &weights) {
	vector< pair< double, long long int> > dag;

	//cout<<distanceFromSource[ destination ]<<endl;
	int startTripTime = clock();
	for(int i=0; i <  nodes.size() ; i++) {	
		if( ( query(source, i) + query(i, destination ) ) < alpha*distFromSourceToDestination ) {
			dag.push_back( make_pair(-distanceToDestination[ i ], i ) );
		}
	}
	//dagTimeTaken[ tripNumber ] += float(clock()-startTripTime)/CLOCKS_PER_SEC;
	sort( dag.begin(), dag.end() );
	// (Distance, Last node) at dag[i] given a particular score
	vector< map<int, pair<double, long long int> > > scores( dag.size() );
  
  	vector< long long int> nodeToDagIndex(nodeID.size());
	// cout<<dag.size()<<endl;
	for (int i=0; i<nodeID.size(); i++) {
  		nodeToDagIndex[i] = -1;
  	}
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
		
	long long int startIndex = nodeToDagIndex[source];
  	long long int endIndex = nodeToDagIndex[destination];

  	scores[ startIndex ][ 0 ] = make_pair(0.0, -1);
  	for(int i = startIndex; i < dag.size(); i++) {

		double lastDist = 1e10; // infinity    	
		vector<int> delScore;
    	for(map<int, pair<double, long long int> >::reverse_iterator it=scores[ i ].rbegin(); it != scores[ i ].rend(); it++) { 
    		if (it->second.first >= lastDist) {
    			delScore.push_back(it->first);
    			continue;
    		}
    		lastDist = it->second.first;
    	}
    	for (int j = 0; j < delScore.size(); j++) {
    		scores[i].erase(delScore[j]);
    	}

    	long long int u = dag[i].second;
    	// cout<<weights[u]<<endl;
	    for(int j = 0; j < edges[u].size(); j++) {
	    	long long int v = edges[u][j];
	      	long long int vIndex = nodeToDagIndex[ v ];
	      	if( nodeToDagIndex[ v ] == -1 )
	        	continue;
	      	//some nodes are ajacent to themselves in the graph
	      	if(u == v)
	        	continue;

	      	if( vIndex >= i ) {
	      		for(map<int, pair<double, long long int> >::iterator it=scores[ i ].begin(); it != scores[ i ].end(); it++)
	        	{ 
	        		// cout<<it->first<<endl;
	        		if( it->first > 10000)
	        			continue;

	        		int curScore = it->first + weights[v];
	        		double prevDist = it->second.first;
	          		if( scores[ nodeToDagIndex[v] ].find( curScore ) != scores[ nodeToDagIndex[v] ].end() ) {
			          	if( scores[ nodeToDagIndex[v] ][curScore].first >= prevDist + edgeWeight[u][j] ) {
			            	scores[ nodeToDagIndex[v] ][curScore] = make_pair(prevDist + edgeWeight[u][j], u);
			            }
			        }
			        else
			        {
			        	scores[ nodeToDagIndex[v] ][curScore] = make_pair(prevDist + edgeWeight[u][j], u);	
			        }
				}
			}
		}
	}
	long long int bestScore = 0;

	for( map<int, pair<double, long long int> >::iterator it=scores[ endIndex ].begin(); it != scores[ endIndex ].end(); it++) {
		
		if( it->second.first < distFromSourceToDestination*alpha ) {
			bestScore = it->first;
		}
	}
	vector<long long int> path;
	long long int traceLocation = destination;
	int traceScore = bestScore;
	while (traceLocation != -1) {
		//printf(" At %lld[%lld]: Score = %d (dist: %.4f)\n",traceLocation, nodeToDagIndex[traceLocation], traceScore, scores[ nodeToDagIndex[traceLocation] ][traceScore].first);
		path.push_back(traceLocation);
		int prevScore = traceScore - weights[traceLocation];
		traceLocation = scores[ nodeToDagIndex[traceLocation] ][traceScore].second;	
		traceScore = prevScore;
	}
	reverse(path.begin(), path.end());

	//cout<<"BEST SCORE:   "<<bestScore<<endl;
	return path;

}


void extendEdges(long long int source, long long int node, vector<long long int> &path, vector< pair<double, vector<long long int> > >  &paths,
	double pathLen) {
	
	for( int i = 0; i < path.size(); i++) {
		if( node == path[i] )
			return;
	}
	path.push_back( node );

	if( path.size() > 1 ) {
		paths.push_back( make_pair(pathLen, path) ) ;
	}

	if( pathLen >= maxDepth ) {
		path.pop_back();
		return ;
	}
	
	for(long long int j=0; j < edges[ node ].size(); j++) {
		long long int newNode = edges[ node ][j];
		extendEdges( source, newNode, path, paths, pathLen + edgeWeight[ node ][j] ) ;
	}

	path.pop_back();
}

vector<long long int> dagExtendedEdges(long long int source, long long int destination, vector< double > &distanceFromSource, vector< double > &distanceToDestination,
  vector< vector< pair<double, vector<long long int> > > > &extendEdge, vector< vector< long long int > > &extendEdgeWeights, vector< long long int > &weights) {

	vector< pair< double, long long int> > dag;

	for(int i=0; i <  nodes.size() ; i++) {	
		if( ( query(source , i) + query(i, destination ) )
			 < alpha*distFromSourceToDestination )
			  {
			dag.push_back( make_pair(-distanceToDestination[ i ], i) );
		}
	} 

	sort( dag.begin(), dag.end() );

	// (Distance, (Last Node, Ex Edge used) ) at dag[i] given a particular score
  	vector< map<int, pair<double, pair<long long int,int> > >  > scores(dag.size());
  	
  	vector< long long int> nodeToDagIndex(nodeID.size());
  	for (int i=0; i<nodeID.size(); i++) {
  		nodeToDagIndex[i] = -1;
  	}
	for(int i=0; i <  dag.size() ; i++) {
		nodeToDagIndex[ dag[i].second ] = i;
	}
	
	long long int startIndex = nodeToDagIndex[ source ];
  	long long int endIndex = nodeToDagIndex[ destination ];

  	scores[ startIndex ][ 0 ] = make_pair(0, make_pair(-1,0)) ;
  	vector< set<long long int> > reach(nodeID.size());
  	vector< set<long long int> > reachFrom(nodeID.size());

  	int counter = 0;
  	for(int i = startIndex; i < dag.size(); i++) {
    	long long int u = dag[i].second;
    	
		double lastDist = 1e10; // infinity    	
		vector<int> delScore;
    	for(map<int, pair<double, pair<long long int,int> > >::reverse_iterator it=scores[ i ].rbegin(); it != scores[ i ].rend(); it++) { 
    		if (it->second.first >= lastDist) {
    			delScore.push_back(it->first);
    			continue;
    		}
    		lastDist = it->second.first;
    	}
    	for (int j = 0; j < delScore.size(); j++) {
    		scores[i].erase(delScore[j]);
    	}
    	
	    
	    for(int j = 0; j < extendEdge[u].size(); j++) {
	    	
	    	pair<double, vector<long long int> > v = extendEdge[u][j];
	    	bool continueLoop = false;
	    	bool reachAbleError = false;
	    	long long int pathSize = v.second.size();

	    	if( pathSize < 1)
	    		continue;

	    	for(int k1 = 0; k1 < pathSize && !reachAbleError ; k1++ ) {	
    			for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
    				if( reach[ v.second[ k2 ] ].find( v.second[ k1 ] ) != reach[ v.second[ k2 ] ].end() ) {
    					reachAbleError = true;
    					break;
    				}
    			}
        	} 

	    	for(int k = 1; k < pathSize ; k++ ) {
	    		long long int pathNode = v.second[ k]; 
	    		
	    		if( ( nodeToDagIndex[ pathNode ] >= i ) ^  ( k == ( pathSize -1 ) ) ) {
	    			continueLoop = true;
	    			break;
	    		}
	    		if( ( nodeToDagIndex[ pathNode ] == -1 ) || reach[ pathNode ].find(u) != reach[ pathNode ].end() ) {
	    			reachAbleError = true;
	    			break;
	    		}
	    	}

	    	long long int lastNode =  v.second[ pathSize - 1 ];

	      	if( continueLoop || reachAbleError || ( lastNode == u ) )
	    		continue;

	    	counter++;
	       	long long int vIndex = nodeToDagIndex[ lastNode ];
	      	long long int vWeight = extendEdgeWeights[u][j];

	      	bool updated = false;

      		for(map<int, pair<double, pair<long long int,int> > >::iterator it=scores[ i ].begin(); it != scores[ i ].end(); it++) { 

	          	if( it->first > 10000)
	        			continue;

	        	int curScore = it->first + vWeight;
	        	double prevDist = it->second.first;
	        	double curDist = v.first;
	        	/* BUG on edgeWeight[u][j], should be v.first */
          		if( scores[ vIndex ].find( curScore ) != scores[ vIndex ].end() ) {
		          	if( scores[ vIndex][curScore].first >= prevDist + curDist ) {
		            	scores[ vIndex ][curScore] = make_pair(prevDist + curDist, make_pair(u,j));;
		            }
		        }
		        else
		        {
		        	scores[ vIndex ][curScore] = make_pair(prevDist + curDist, make_pair(u,j));;	
		        }
				
            	if( updated )
            		continue;
            	else
            		updated = true;

            	for(int k1 = 0; k1 < pathSize ; k1++ ) {	
        			for(int k2 = k1 + 1; k2 < pathSize; k2++ ) {
            			reach[ v.second[ k1 ] ].insert( v.second[ k2 ] );
            			reachFrom[ v.second[ k2] ].insert( v.second[ k1 ] );
        			}
            	} 

            	for(long long int k1 = pathSize - 1; k1 >= 0 ; k1-- ) {	
            		for( set<long long int>::iterator it = reachFrom[ v.second[ k1 ] ].begin(); it != reachFrom[ v.second[ k1 ] ].end(); it++) {
            			for(long long int k2 = k1 + 1; k2 < pathSize; k2++ ) {
	            			reach[ (*it) ].insert( v.second[ k2 ] );
	            			reachFrom[ v.second[ k2] ].insert( (*it) );
            			}
            		}
            	}
            	
        	}
		}
		
	}

	long long int bestScore = 0;

	for( map<int, pair<double, pair<long long int,int> > >::iterator it=scores[ endIndex ].begin(); it != scores[ endIndex ].end(); it++) {	
		if( it->second.first < distFromSourceToDestination*alpha ) {
			bestScore = it->first;
		}
	}

	vector<long long int> path;
	long long int traceLocation = destination;
	int traceScore = bestScore;
	while (traceLocation != -1) {
		//printf(" At %lld[%lld]: Score = %d (dist: %.4f)\n",traceLocation, nodeToDagIndex[traceLocation], traceScore, scores[ nodeToDagIndex[traceLocation] ][traceScore].first); fflush(stdout);
		pair<long long int,int> prevInfo = scores[ nodeToDagIndex[traceLocation] ][traceScore].second;
		if (prevInfo.first != -1) {
			vector<long long int> extendPath = extendEdge[prevInfo.first][prevInfo.second].second;
			for (int i=extendPath.size()-1; i>0; i--) {
				path.push_back(extendPath[i]);
			}
		}

		traceLocation = prevInfo.first;	
		if (traceLocation != -1) traceScore -= extendEdgeWeights[prevInfo.first][prevInfo.second];
	}
	reverse(path.begin(), path.end());

	//cout<<"BEST SCORE:   "<<bestScore<<endl;
	return path;
}

long long int calculateScore( vector< long long int > &path, vector< long long int> &expectedTrips) {
	long long int ret = 0;
	for(int i = 1; i < path.size(); i++) {
		ret += expectedTrips[ path[i] ];
	}
	return ret;
}

void potentialScore(long long int source, long long int destination, long long int timeSlot, 
	vector< vector< pair<double, vector<long long int> > > > &extendEdge)
{
	cout<<"Potential Score called"<<endl;
	printf("Source: %lld\tDestination: %lld\tTime slot: %lld\n",source,destination,timeSlot);

	vector< double > distanceFromSource(nodeID.size());
	vector< double > distanceFromDestination(nodeID.size());
	vector< double > distanceToDestination(nodeID.size());

	vector<long long int> path = dijkstra_lengths(source, destination, distanceFromSource, edges, edgeWeight);
	dijkstra_lengths(destination, source, distanceFromDestination, edges, edgeWeight);
	
	if(distanceFromSource[destination] == maxDistance || !distanceFromSource[ destination ] ) {
		tripNumber--;
		printf("Cannot reach destination?\n");
		return;
	}
	
	dijkstra_lengths(destination, source, distanceToDestination, edgesReverse, edgeWeightReverse);

	if( DEBUG ) {
		printf("  [Dijkstra length] %.4f\n",distanceFromSource[ destination ]);
	}

	//dagTimeTaken[ tripNumber ] = 0;
	//startTimeTrip = clock();

	vector<long long int> expectedTrips(nodeID.size()); 
	// From s to d, via v, where v is within 1+alpha
	for( int i =0; i<nodeID.size(); i++) {	
		if( ( distanceFromSource[ i ] + distanceToDestination[ i ] ) 
				< alpha	*distanceFromSource[ destination ] ) {
			//long long int passengersNumber;
			expectedTrips[ i ] = get_expected_trips(source, destination, i, timeSlot,
				distanceToDestination, distanceFromDestination);
		}
	}

	// cache this value
	//cout<<"WEIGHTS WERE ASSIGNED"<<endl;
	cout<< source <<" Source - Destination "<< destination <<endl;
	distFromSourceToDestination = query(source, destination );
	printf("  [Query length] %.4f\n",distFromSourceToDestination);
	//cout<<"WEIGHTS WERE ASSIGNED"<<endl;
	//dagExTime[ tripNumber ] = ( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC; 


	startTimeTrip = clock();
	distFromSourceToDestination = distanceFromSource[ destination ];
	
	vector<long long int> maxScoreShortestPath = HD( source, destination, expectedTrips);
	int shortestPathScore = getPathScore(maxScoreShortestPath, expectedTrips);
	double minTimeTaken = float( clock()- startTimeTrip )/ CLOCKS_PER_SEC;
	minPathScore[ tripNumber ] = float( shortestPathScore  + 1);
	printf("  [Min path score] %d (Time: %.2f)\n",shortestPathScore  + 1,minTimeTaken);
	if (PRINT_PATH) {
		printPath(maxScoreShortestPath, expectedTrips);
	}
	/* calculating DAG parameters */
	startTimeTrip = clock();
	vector<long long int> dagPath = dagScore1(source, destination, distanceFromSource,
	distanceToDestination, expectedTrips);
	long long int scoreDag = getPathScore(dagPath, expectedTrips);

	//cout<<( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC<<endl;
	//dagTimeTaken[ tripNumber ] +=  ( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC;
	//cout<<scoreMinPath<<" SCORES "<<scoreDag<<endl;
	dagTimeTaken[ tripNumber ] = float( clock()- startTimeTrip )/ CLOCKS_PER_SEC;
	printf("  [DAG score] %lld (Time: %.2f)\n",scoreDag+1,dagTimeTaken[ tripNumber ]);
	if (PRINT_PATH) {
		printPath(dagPath, expectedTrips);
	}
	dagScore[ tripNumber ] = min(5.0, float( scoreDag + 1)  / ( minPathScore[ tripNumber ] ) ) ;
	
	if( scoreDag + 1  >= minPathScore[ tripNumber ] )
		countWin += 1;

	/* calculating backDAG	 parameters */
	startTimeTrip = clock();
	
	vector< vector< long long int> > extendEdgeWeights(nodeID.size());
	for( int i = 0; i < nodeID.size(); i++) {		
		for(int j =0; j < extendEdge[i].size(); j++) {
			long long int scoreAlongPath = calculateScore( extendEdge[i][j].second, expectedTrips);
			extendEdgeWeights[ i ].push_back( scoreAlongPath );
		}
	}

	vector<long long int> dagExPath = dagExtendedEdges( source, destination, distanceFromSource, distanceToDestination, extendEdge, extendEdgeWeights, expectedTrips);
	long long int scoreExDag = getPathScore(dagExPath, expectedTrips);
	dagExScore[ tripNumber ] = min( 5.0, float( scoreExDag + 1 ) / ( minPathScore[tripNumber] ) ) ;
	dagExTime[ tripNumber ] = ( clock() - startTimeTrip ) / (float) CLOCKS_PER_SEC;
	printf("  [Extended DAG score] %lld (Time: %.2f)\n",scoreExDag+1,dagExTime[ tripNumber ]);
	if (PRINT_PATH) {
		printPath(dagExPath, expectedTrips);
	}
	//cout<<	"SCORE ALONG EX "<<scoreDag<<"  RATIO WITH DAG "<< float( scoreDag + 1)/(dagScore[ tripNumber ]*minPathScore[ tripNumber] )<<endl;
	return ;	
}

/* edges global structure */
int main(int argc, char const *argv	[])
{
	if (argc < 3) {
		printf("Usage: ./a.out [maxDepth=0.2] [alpha=1.3]\n");
		return 0;
	}
	txtName = "beijingIndex";
	degName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(degName, "%s.deg", txtName);
	
	inName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(inName, "%s.labelin", txtName);
	outName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(outName, "%s.labelout", txtName);

	timer tm;
	
	loadIndex();
	
	printf("load time %lf (ms)\n", tm.getTime()*1000);
	
	memset(dagScore, 0, sizeof(dagScore));
	memset(minPathScore, 0, sizeof(minPathScore));
	memset(dagTimeTaken,0,sizeof(dagTimeTaken));
	string s = argv[1];
	stringstream ss(s);	
	stringstream ssDebug(argv[2]);
	ss>>maxDepth;
	ssDebug>>alpha;
	printf("Max Depth = %.2f\nAlpha = %.2f\n",maxDepth,alpha); fflush(stdout);

	takeGraphInput();
	printf("Finish taking graph input\n"); fflush(stdout);
	getTimeSourceDestination();
	printf("Finish get time source destination\n"); fflush(stdout);
	vector< vector< pair<double, vector<long long int> > > > extendEdge(nodeID.size()); 
	for( int i = 0; i < nodeID.size(); i++) {
		vector<long long int> path;
		vector< pair< double, vector<long long int> > > paths;
		extendEdges(i, i, path, paths, 0);
		extendEdge[ i ] = paths;
	}
	printf("Extend edge assigned\n"); fflush(stdout);

	//srand (time(NULL));
	srand(0);
	
	tripNumber = 0;
	for( int i=0; tripNumber < 100; i += 1) {
		
		long long int source, timeS, destination;
		
		source = rand() % nodeID.size() ;
		timeS = rand()%96;

		if( source >= sourceTimeDestination.size() )
			continue;
		else if ( sourceTimeDestination[ source ].find( timeS ) == sourceTimeDestination[ source ].end() )
			continue;

		destination = sourceTimeDestination[ source ][ timeS ][ rand() % sourceTimeDestination[ source ][ timeS ].size()  ];
		potentialScore(source, destination, timeS, extendEdge);
		printf("Trip num %lld done\n\n",tripNumber);
		tripNumber++;

	}

	double avgMinPath = 0, avgDAGScore = 0, avgDAGtime = 0 ; 
	double avgExScore = 0, avgExTime = 0;
	int hist[101];
	
	memset(hist, 0, sizeof(hist));

	/*std::vector<double> v1;
	std::vector<double> v2;

	for(int i = 0; i < 100; i++)
	{
		v1.push_back(dagScore[i]);
		v2.push_back(dagExScore[i]);
	} 
	sort(v1.begin(), v1.end());
	sort(v2.begin(), v2.end());
	for(int i = 0; i < 100; i++)
	{ 
		cout<<v1[i]<<endl;
	}
	for(int i = 0; i < 100; i++)
	{ 
		cout<<v2[i]<<endl;
	}
	cout<<"\n\nNEXT VERBOSE::\n\n";
	*/
	for(int i = 0; i < 100; i++)
	{
		avgMinPath += minPathScore[i];
		avgDAGScore += dagScore[i];
		avgDAGtime += dagTimeTaken[i];
		avgExScore += dagExScore[i];
		avgExTime += dagExTime[i];
		hist[ int(dagExScore[i]) ] += 1;
	} 
	cout<<"Counter-WIN "<<countWin<<endl;
	cout<<avgMinPath/100<<" "<<avgDAGScore/100<<" "<<avgDAGtime/100<<endl;
	cout<<avgExScore/100<<" "<<avgExTime/100<<endl;
	for(int j = 0; j <= 100; j++)
	{
		cout<<hist[j]<<" ";
	}
	return 0;
}

