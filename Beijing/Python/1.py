
from math import radians, cos, sin, asin, sqrt
import numpy 
import pickle
import datetime 
import os

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees) 
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r

nodes = file("LocationData.txt")
edgesFile = file("SegmentData.txt")
cab = file("./cabspottingdata/new_abcoij.txt")
path = "./cabspottingdata/"


nodesMap = {}
nodesMap2 = {}
listCoords = []

while(1):
    line = nodes.readline()
    if( line == "" ):
        break
    lineData = line.split(",")
    nodesMap[ int( lineData[0] ) ] = ( float(lineData[1]), float(lineData[2]) )
    nodesMap2[ ( float(lineData[1]), float(lineData[2]) ) ] = int( lineData[0] )
    listCoords.append( ( float(lineData[1]), float(lineData[2]) )  ) 

listCoords.sort(key=lambda tup: (tup[0], tup[1]) )


edge_weight = {}
edges = {}
tedge_weight = {}
tedges = {}
counter_bad = 0
while(1):
    line = edgesFile.readline()
    if( line == "" ):
        break
    lineData = line.split(";")
    edge_weight[ ( int( lineData[0] ) , int( lineData[1] ) ) ] = float( lineData[2] )
    tedge_weight[ ( int( lineData[1] ) , int( lineData[0] ) ) ] = float( lineData[2] )

    try: 
        edges[ int(lineData[0]) ].append( int(lineData[1]) )
    except: 
        edges[ int(lineData[0]) ] = [ int(lineData[1]) ]
    try: 
        tedges[ int(lineData[1]) ].append( int(lineData[0]) )
    except: 
        tedges[ int(lineData[1]) ] = [ int(lineData[0]) ]

def missingNodes(S, D, extraEdges = False):
    global edge_weight
    global edges
    global counter_bad
    dist = {}
    path = {}
    vset = set()
    for node in nodesMap:
        dist[node] = 1000000
    vset.add(S)
    dist[S] = 0
    # print(S, D)
    try:
        while( len(vset) > 0):
            x = 1000000; u = 0 
            for node in vset:
              if(dist[node] < x):
                x = dist[node]
                u = node
            # print( path, vset)
            if(x == 1000000 or u == D):
              break

            if( extraEdges ):
                try:
                    list1 = edges[u]
                except:
                    list1 = []
                try:
                    list2 = tedges[u]
                except:
                    list2 = []

                for v in (list1 ):
                    alt = dist[u] + edge_weight[ (u, v) ]
                    if(alt < dist[v]):
                        dist[v] = alt
                        path[v] = u
                        vset.add(v)

                for v in (list2 ):
                    alt = dist[u] + tedge_weight[ (u, v) ]
                    if(alt < dist[v]):
                        dist[v] = alt
                        path[v] = u
                        vset.add(v)
            else:
                for v in edges[u]:
                    alt = dist[u] + edge_weight[ (u, v) ]
                    if(alt < dist[v]):
                        dist[v] = alt
                        path[v] = u
                        vset.add(v)
            vset.remove( u )    
        sp = []
        x = D

        while(1):
            sp.append(x)
            if(x == S):
                break
            x = path[x]

        sp.append(S)
        sp.reverse()
        return( sp )
    except : 
        raise

traj = []
currentTraj = []
node = prevnode = ( 0, 0)

listing = os.listdir( path )
skipTraj = False
count = 10 
for infile in listing:
    count -= 1
    if( not(count) ):
        break
    if(infile[0:4] == "new_"):
        cab = open(path+infile, 'r')
    print infile
    while( 1 ):
        line = cab.readline()
        if( line == "" ):
            break
        lineData = line.split(" ")
        lineData[-1] = lineData[-1][0:-2]
        lat = float( lineData[0] ); lon = float( lineData[1] )
        occupied = int(lineData[2]) 
        time = int(lineData[-1])
        timeStamp = datetime.datetime.utcfromtimestamp(time)
        nodeIndex = numpy.argmin( ( [ haversine(lonList, latList, lon, lat)  for (lonList, latList) in listCoords ] ) )
        
        node = nodesMap2[ listCoords[nodeIndex] ]
        
        if( not( occupied ) ):
            if( len( currentTraj ) ):
                if( not(skipTraj) ):
                    traj.append( currentTraj )
                currentTraj = []
                skipTraj = False
        else:
            if( not( len(currentTraj) ) ):
                prevnode = node
                currentTraj.append( node )
            else:
                try:
                    missingNodesList = missingNodes(prevnode, node)
                except Exception, e:
                    try:
                        missingNodesList = missingNodes(prevnode, node, True)
                    except:
                        skipTraj = True

                currentTraj += missingNodesList 
                prevnode = node
    if( len( currentTraj ) ):
            traj.append( currentTraj )        

fp1 = open('traj.p', 'w')
pickle.dump( traj, fp1)
fp1.close()

