import os
import pickle

pathFiles = './TrajData/'
listing = os.listdir(pathFiles)
pointsRequired = 1000
trajectoryList = []

counter = 0

for infile in listing:
	if(infile[0:2]!="be"):
		continue
	file1 =  open(pathFiles+infile, 'r')
	if(counter == pointsRequired):
		break

	while 1:
		line1 = file1.readline()
		line2 = file1.readline()
		line2 = line2[1:-1].split('][')
		if((counter == pointsRequired) or (line1 == "")):
			break
		path = [0]*len(line2) 
		for i in xrange(0, len(path)):
			temp = line2[i].split(',')
			path[i] = int(temp[0])
		counter += 1
		trajectoryList.append(path)

import numpy
import copy
import Queue
import networkx as nx
alpha = 0.3
beta = 10 #in minutes
#Convert all times to minutes

# fp_read = open('all_pairs_shortest_lengths.p')
# # shortest_lengths = pickle.load(fp_read)
# shortest_lengths = {}

print "here"
# ------------Abhishek---------------------------------
def parseTimeFromString(s):
	components = s.split(':')
	return (int(components[0])*60 + int(components[1]))/15

def getMeTime(s):
	return parseTimeFromString(s.split(" ")[1])

class node:
    nodeId = 0
    time = 0
    def nodeInit(self):
	    nodeId = 0
	    time = 0

# ------ Loading dictionary for time to trip query -------

def load_obj(name ):
    with open('obj/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)
        
sourceTimeDestination = load_obj("TrajData")

# ---------------------------------------------------------
ifile = open('beijing_adj_fromhistory_new.txt')

lines = ifile.readlines()

l1 = lines[0].split(',')
num_nodes = int(l1[0])
num_edges = int(l1[1])

nodes_to_edges_dic = {}
nodes_list = []
adj_list = {}
for i in range(1,num_nodes+1):
	node_line = lines[i].split(',')
	nodes_list.append(int(node_line[0]))
	nodes_to_edges_dic[int(node_line[0])] = []
	for j in range(3, len(node_line)):
		nodes_to_edges_dic[int(node_line[0])].append(node_line[j])
	adj_list[int(node_line[0])] = []

edges_to_nodes_dic = {}
edges_list = []
full_edges_list = []
edge_weight = {}

G_dist = nx.Graph()
G_dist.add_nodes_from(nodes_list)

for i in range(num_nodes+1,len(lines)):
	edge_line = lines[i].split(',')	
	edges_list.append(int(edge_line[0]))
	edges_to_nodes_dic[int(edge_line[0])] = (int(edge_line[1]),int(edge_line[2]),float(edge_line[3]))
	full_edges_list.append( (int(edge_line[1]),int(edge_line[2])) )
	G_dist.add_edge(int(edge_line[1]),int(edge_line[2]),weight = float(edge_line[3]) )
	edge_weight[(int(edge_line[1]),int(edge_line[2]))] = float(edge_line[3])
	edge_weight[(int(edge_line[2]),int(edge_line[1]))] = float(edge_line[3]) #for undirected only
	adj_list[int(edge_line[1])].append(int(edge_line[2]))
	adj_list[int(edge_line[2])].append(int(edge_line[1])) #for undirected only

class request:
	city = 'beijing'
	def __init__(self,source,destination,req_time):
		self.source = source
		self.destination = destination
		self.start_time = req_time # For now request time is equated with pickup time


def dijkstra_lengths(S,D):
  dist = {}
  path = {}
  vset = set()
  nodes = set(nodes_list)
  for node in nodes_list:
    dist[node] = 1000000
    path[node] = -1
    vset.add(node)

  dist[S] = 0
  while(len(vset)>0):
    x = 1000000
    for node in vset:
      if(dist[node]<x):
        x = dist[node]
        u = node
    if(x == 1000000):
      break
    vset.remove(u)
    for v in adj_list[u]:
      if(v in vset):
        alt = dist[u] + edge_weight[(u,v)]
        if(alt<dist[v]):
          dist[v] = alt
          path[v] = u

  sp = []
  x = D

  while(1):
    sp.append(x)
    if(x==S):
      break
    x = path[x]
  sp.append(S)
  return sp,dist

total_count = range(10)
ratios = []
j = 0
count = range(10)
import networkx as nx
for traj in trajectoryList:
	if(j%100==0):
		print j
	j+=1
	n = len(traj)
	S = traj[0]
	D = traj[n-1]
	sld = nx.single_source_dijkstra_path_length(G_dist, D,weight = 'weight')
	for i in range(n-1):
		for j in range(1,11):
			try:
				if(sld[traj[i+j]] > sld[traj[i]]):
					count[j-1]+=1
			except:
				count[j-1] += 0 
	for j in range(10):
		total_count[j] += n-j-1


for j in range(10):
	print j+1, float(count[j])/total_count[j]


# For 2000 trajectories, average of violations across all trajectories was 0.278272285949



