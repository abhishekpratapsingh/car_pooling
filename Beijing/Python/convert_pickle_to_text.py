import pickle


fp_read = open('all_pairs_shortest_lengths.p')
shortest_lengths = pickle.load(fp_read)
ofile = open('all_pairs_shortest_lengths.txt','w')
for node in shortest_lengths.keys() :
	print >>ofile,node,len(shortest_lengths[node])
	for key in shortest_lengths[node].keys() :
		print >>ofile,key,shortest_lengths[node][key]

ofile.close()

