import numpy
import heapq
import pickle
import copy
import sys

maxDistance = 10000000

nodes = file("LocationData.txt")
edgesFile = file("SegmentData.txt")

# def load_obj(name ):
#     with open( name + '.p', 'rb') as f:
#         return pickle.load(f)
        
# all_pairs_shortest_lengths = load_obj("all_pairs_shortest_lengths")
# print all_pairs_shortest_lengths
# sys.exit(1)

nodesList = []
listCoords = []
while(1):
    line = nodes.readline()
    if( line == "" ):
        break
    lineData = line.split(",")
    listCoords.append( ( float(lineData[1]), float(lineData[2]) )  ) 
    nodesList.append( int( lineData[0] ) )

listCoords.sort(key=lambda tup: (tup[0], tup[1]) )

edgeWeight = {}
edges = {}

while(1):
    line = edgesFile.readline()
    if( line == "" ):
        break
    lineData = line.split(";")
    edgeWeight[ ( int( lineData[0] ) , int( lineData[1] ) ) ] = float( lineData[2] )

    try: 
        edges[ int(lineData[0]) ].append( int(lineData[1]) )
    except: 
        edges[ int(lineData[0]) ] = [ int(lineData[1]) ]
    

dijkstra_lengths = {}
		
def dijkstra( source ):
	global nodesList, edges, edge_weight, dijkstra_lengths
	dijkstra_lengths[ source ] = {}
	heap = []
	tempDis = {}
	for i in xrange(0, len(nodesList) ):
		tempDis[ nodesList[i] ] = maxDistance
	tempDis[ source ] = 0
	heapq.heappush( heap, (0, (source) ) )

	while( len(heap) ):
		top = heapq.heappop( heap )
		
		if( abs(top[0]) > tempDis[ top[1] ] ):
			continue

		if top[1] in edges:
			for key in edges[ top[1] ]:
				weightNode = tempDis[ top[1] ]
				weightKey = tempDis[ key ]
				newWeightKey = weightNode + edgeWeight[ (top[1], key) ]
				
				if( newWeightKey < weightKey):
					tempDis[ key ] = newWeightKey
					heapq.heappush( heap, ( newWeightKey, (key) ) )

	for i in xrange(0, len(nodesList) ):		
		dijkstra_lengths[source][ nodesList[i] ] = tempDis[ nodesList[i] ] 


for i in nodesList:
	dijkstra(i)

fp1 = open('all_pairs_shortest_lengths.p', 'w')
pickle.dump(dijkstra_lengths, fp1)
fp1.close()

