import numpy
import networkx as nx
import os
import pickle

alpha = 0.2 
beta = 10 #in minutes
#Convert all times to minutes

fp_read = open('all_pairs_shortest_lengths.p')
shortest_lengths = pickle.load(fp_read)

print "here"
# ------------Abhishek---------------------------------
def parseTimeFromString(s):
	components = s.split(':')
	return (int(components[0])*60+int(components[1]))/15

def getMeTime(s):
	return parseTimeFromString(s.split(" ")[1])

class node:
    nodeId = 0
    time = 0
    def nodeInit(self):
	    nodeId = 0
	    time = 0
# ------------------------------------------------------

# ------ Loading dictionary for time to trip query -------

def load_obj(name ):
    with open('obj/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)
        
sourceTimeDestination = load_obj("TrajData")

# ---------------------------------------------------------
ifile = open('beijing_adj_fromhistory_new.txt')

lines = ifile.readlines()

l1 = lines[0].split(',')
num_nodes = int(l1[0])
num_edges = int(l1[1])

nodes_to_edges_dic = {}
nodes_list = []
adj_list = {}
for i in range(1,num_nodes+1):
	node_line = lines[i].split(',')
	nodes_list.append(int(node_line[0]))
	nodes_to_edges_dic[int(node_line[0])] = []
	for j in range(3, len(node_line)):
		nodes_to_edges_dic[int(node_line[0])].append(node_line[j])
	adj_list[int(node_line[0])] = []

edges_to_nodes_dic = {}
edges_list = []
full_edges_list = []
edge_weight = {}

G_dist = nx.Graph()
G_dist.add_nodes_from(nodes_list)

for i in range(num_nodes+1,len(lines)):
	edge_line = lines[i].split(',')	
	edges_list.append(int(edge_line[0]))
	edges_to_nodes_dic[int(edge_line[0])] = (int(edge_line[1]),int(edge_line[2]),float(edge_line[3]))
	full_edges_list.append( (int(edge_line[1]),int(edge_line[2])) )
	G_dist.add_edge(int(edge_line[1]),int(edge_line[2]),weight = float(edge_line[3]) )
	edge_weight[(int(edge_line[1]),int(edge_line[2]))] = float(edge_line[3])
	edge_weight[(int(edge_line[2]),int(edge_line[1]))] = float(edge_line[3]) #for undirected only
	adj_list[int(edge_line[1])].append(int(edge_line[2]))
	adj_list[int(edge_line[2])].append(int(edge_line[1])) #for undirected only


class request:
	city = 'beijing'
	def __init__(self,source,destination,req_time):
		self.source = source
		self.destination = destination
		self.start_time = req_time # For now request time is equated with pickup time

def get_all_trips(source, start_time, window):
	global sourceTimeDestination
	tripList = []
	for i in xrange(0, window/15 + 1):
		if source in sourceTimeDestination:
			if (start_time + i) in sourceTimeDestination[source]:
				tripList += sourceTimeDestination[source][start_time + i]
	return tripList

def get_historical_score_timed(S,D,path,start_time): # TIME VERSION
	Te = 0
	V0 = S
	Tsd = shortest_time(S,D,start_time)
	scores = []
	total_score = 0
	for V in path:
		score = 0
		Te = Te + time_estimate(V0,V,Te + start_time)
		dest_list   = get_all_trips(V,start_time + Te - Beta, Beta) #undefined right now
		for W in dest_list:
			Tsv = Te
			Tvw = shortest_time(V,W,start_time + Te) #undefined right now
			Twd = shortest_time(W,D,start_time + Te + Tvw) 
			if((Tsv + Tvw + Twd) < ((1+alpha)*Tsd)):
				score +=1
			else:
				Tvd = shortest_time(V,D,start_time + Te) #undefined right now
				Tdw = shortest_time(D,W,start_time + Te + Tvd)
				if((Tsv + Tvd + Tdw) < ((1+alpha)*Tsd)):
					score +=1
		scores.append(score)
		total_score += score

	return (total_score,scores)


def get_historical_score(S,D,path,start_time): # DISTANCE VERSION
	global sourceTimeDestination
	Te = 0
	V0 = S
	Tsd = nx.shortest_path_length(G_dist,S,D,"weight")
	scores = []
	total_score = 0
	for V in path:
		score = 0
		try:
			Te = Te + edge_weight[(V0,V)]
		except:
			Te = Te + 0
		dest_list = get_all_trips(V,start_time/15, 60) #undefined right now
		for W in dest_list:
			Tsv = Te
			try:
				Tvw = shortest_lengths[V][W]
			except:
				Tvw = nx.shortest_path_length(G_dist, V, W, "weight") 
			try:
				Twd = shortest_lengths[W][D]
			except:
				Twd = nx.shortest_path_length(G_dist, W, D, "weight")
			if((Tsv + Tvw + Twd) < ((1+alpha)*Tsd)):
				score +=1
			else:
				try:
					Tvd = shortest_lengths[V][D]
				except:
					Tvd = nx.shortest_path_length(G_dist, V, D, "weight") 
				try:
					Tdw = shortest_lengths[D][W]
				except:
					Tdw = nx.shortest_path_length(G_dist, D, W, "weight")
				if((Tsv + Tvd + Tdw) < ((1+alpha)*Tsd)):
					score +=1
		scores.append(score)
		total_score += score
	return (total_score,scores)


def return_comp_paths(s,d,W): #First value returned is list of candidate paths, second value is shortest path
	paths_stack = [([s],0)]
	found_paths = []
	shortest_path = []
	length_min = 100000 
	while(len(paths_stack) >0):
		path_pair = paths_stack.pop()
		path = path_pair[0]
		wt = path_pair[1]
		n = path[-1]
		for m in adj_list[n]:
			if(m in path):
				continue
			h = 0
			try:
				h = shortest_lengths[m][d]
			except:
				h=0
			if(edge_weight[(m,n)] == 0):
				continue
			if((edge_weight[(m,n)] + wt + h) <= W):
				new_path_pair = (path + [m],edge_weight[(m,n)] + wt)
				paths_stack.append(new_path_pair)
				if(m==d):
					found_paths.append(path + [m])
					if(length_min > (edge_weight[(m,n)] + wt)):
						length_min = edge_weight[(m,n)] + wt
						shortest_path = path + [m]
	return found_paths,shortest_path

def get_adv_points(req_ob): #Difference between highest score and shortest path score
	S = req_ob.source
	D = req_ob.destination
	start_time = req_ob.start_time
	print "CheckPoint 1"
	try:
		spl = shortest_lengths[S][D]
	except:
		spl = nx.shortest_path_length(G_dist,S,D,"weight")
	print "CheckPoint 2"
	(cand_paths, sp) = return_comp_paths(S,D,(1+alpha)*spl)
	(Ns, Ns_array) = get_historical_score(S,D,sp,start_time)
	max_score = 0
	for path in cand_paths:
		(n,n_arr) = get_historical_score(S,D,path,start_time)
		max_score = max(max_score,n)
	try:
		return (float(max_score - Ns) / Ns)
	except:
		return -(float(max_score - Ns))

score = 0
numberTrips = 0
for id in sourceTimeDestination.keys():
	for time in sourceTimeDestination[id].keys():
		for node in sourceTimeDestination[id][time]:
			objectReq = request() 
			objectReq.source = id
			objectReq.destination = node
			objectReq.start_time = time
			print get_adv_points(objectReq) 
			break
		break
	break

path = './TrajData/'
listing = os.listdir(path)

counter = 0


#############TO RUN ADV POINTS OVER ALL TRAJS############################
# for infile in listing:
# 	file1 =  open(path+infile, 'r')
# 	if(infile[0:4] != "beij"):
# 		continue
# 	while 1:
# 		counter += 1
# 		file1.readline()
# 		line1 = file1.readline()
# 		# if(counter%100 != 0):
# 		# 	continue
# 		line2 = line1.split('][')
# 		source = line2[0]
# 		destination = line2[len(line2)-1]
# 		source = source[1:len(source)].split(",")
# 		destination = destination[0:len(destination)-2].split(",")
# 		if(line1 == ""):
# 			break
# 		sourceNode = node()
# 		destinationNode = node()
# 		sourceNode.nodeId =  source[0]
# 		sourceNode.time =  getMeTime(source[3])
# 		destinationNode.nodeId =  destination[0]
# 		destinationNode.time =  getMeTime(destination[3])	
# 		req_ob  = request(int(sourceNode.nodeId), int(destinationNode.nodeId), int(sourceNode.time))
# 		print get_adv_points(req_ob)
# 		break
####END OF######TO RUN ADV POINTS OVER ALL TRAJS#############################################

#######################To STORE SHORTEST DISTANCES###########################################
# active_nodes = []
# for node in nodes_list:
# 	try:
# 		if(len(sourceTimeDestination[node].keys())>0):
# 			active_nodes.append(node)
# 		for key in sourceTimeDestination[node].keys():
# 			active_nodes+= sourceTimeDestination[node][key]
# 	except:
# 		continue

# active_nodes = list(set(active_nodes))
# print (len(active_nodes))


# fp = open('all_pairs_shortest_lengths.p','w')
# shortest_paths = {}
# shortest_lengths = {}
# for m in active_nodes:
# 	shortest_lengths[m] = {}
# 	shortest_paths[m] = {}
# i = 0
# for node in active_nodes:
# 	sl = nx.single_source_dijkstra_path_length(G_dist,node,cutoff=None,weight="weight")
# 	for m in active_nodes:
# 		shortest_lengths[node][m] = sl[m]
# 	i = i+1
# 	if(i%100 == 0):
# 		print i


# pickle.dump(shortest_lengths,fp)

# fp.close()
#######################END OF #######To STORE SHORTEST DISTANCES###################################






