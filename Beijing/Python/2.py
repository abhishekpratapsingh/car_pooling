# from math import radians, cos, sin, asin, sqrt
# import numpy 
# import pickle
# import datetime 

# def load_obj(name ):
#     with open( name + '.p', 'rb') as f:
#         return pickle.load(f)
        
# all_pairs_shortest_lengths = load_obj("all_pairs_shortest_lengths")
# traj = load_obj("traj")
# # print all_pairs_shortest_lengths
# countNodes = 0
# countInv = 0

# for tr in traj:
#     dest = tr[-1]    
#     distToDest = []
#     for node in tr:
#         distToDest.append( all_pairs_shortest_lengths[node][dest] )
#     countNodes += len(tr)
#     for i in xrange(1, len(tr)): 
#         if( distToDest[i] > distToDest[i-1] ):
#             countInv += 1
#     print distToDest
#     break 

ifile = open('beijing_adj_fromhistory_new.txt')

lines = ifile.readlines()

l1 = lines[0].split(',')
num_nodes = int(l1[0])
num_edges = int(l1[1])

nodes_to_edges_dic = {}
nodes_list = []
adj_list = {}
for i in range(1,num_nodes+1):
    node_line = lines[i].split(',')
    nodes_list.append(int(node_line[0]))
    nodes_to_edges_dic[int(node_line[0])] = []
    for j in range(3, len(node_line)):
        nodes_to_edges_dic[int(node_line[0])].append(node_line[j])
    adj_list[int(node_line[0])] = []

edges_to_nodes_dic = {}
edges_list = []
full_edges_list = []
edge_weight = {}

G_dist = nx.Graph()
G_dist.add_nodes_from(nodes_list)

for i in range(num_nodes+1,len(lines)):
    edge_line = lines[i].split(',') 
    edges_list.append(int(edge_line[0]))
    edges_to_nodes_dic[int(edge_line[0])] = (int(edge_line[1]),int(edge_line[2]),float(edge_line[3]))
    full_edges_list.append( (int(edge_line[1]),int(edge_line[2])) )
    G_dist.add_edge(int(edge_line[1]),int(edge_line[2]),weight = float(edge_line[3]) )
    edge_weight[(int(edge_line[1]),int(edge_line[2]))] = float(edge_line[3])
    edge_weight[(int(edge_line[2]),int(edge_line[1]))] = float(edge_line[3]) #for undirected only
    adj_list[int(edge_line[1])].append(int(edge_line[2]))
    adj_list[int(edge_line[2])].append(int(edge_line[1])) #for undirected only

edge = ""