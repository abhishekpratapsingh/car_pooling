import os
import pickle

path = './TrajData/'
listing = os.listdir(path)

def parseTimeFromString(s):
	components = s.split(':')
	return (int(components[0])*60 + int(components[1]))/15

def getMeTime(s):
	return parseTimeFromString(s.split(" ")[1])

class node:
    nodeId = 0
    time = 0
    def nodeInit(self):
	    nodeId = 0
	    time = 0

sourceTimeDestination = {} 
for infile in listing:
	file1 =  open(path+infile, 'r')
	if(infile[0:4] != "beij"):
		continue
	counter = 0
	while 1:
		line1 = file1.readline()
		line2 = file1.readline().split('][')
		source = line2[0]
		destination = line2[len(line2)-1]
		source = source[1:len(source)].split(",")
		destination = destination[0:len(destination)-2].split(",")
		# print source,destination
		if(line1 == ""):
			break
		sourceNode = node()
		destinationNode = node()
		sourceNode.nodeId =  int(source[0])
		sourceNode.time =  getMeTime(source[3])
		destinationNode.nodeId =  int(destination[0])
		destinationNode.time =  getMeTime(destination[3])

		if sourceNode.nodeId in sourceTimeDestination:
			if sourceNode.time in sourceTimeDestination[sourceNode.nodeId]:
				sourceTimeDestination[sourceNode.nodeId][sourceNode.time]\
				.append(destinationNode.nodeId)
			else:
				sourceTimeDestination[sourceNode.nodeId][sourceNode.time]\
				= [destinationNode.nodeId]
		else:
			sourceTimeDestination[sourceNode.nodeId]  = {}
			sourceTimeDestination[sourceNode.nodeId][sourceNode.time]\
				= [destinationNode.nodeId]
		
		counter += 1
	print counter
print len(sourceTimeDestination.keys())
def save_obj(obj, name ):
    with open('obj/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

save_obj(sourceTimeDestination, "TrajData")

# def load_obj(name ):
#     with open('obj/' + name + '.pkl', 'rb') as f:
#         return pickle.load(f)
count = 0
# for key in sourceTimeDestination.keys():
# 	for key2 in sourceTimeDestination[key]:
# 		count += len(sourceTimeDestination[key][key2])
# print count

a = [] 

a.append(1);
a += [1,2,3]
print a
